﻿using Fr.EQL.AI110.DLD_GC.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.DataAccess
{
    public class ProduitDao : DAO
    {
        #region READ
        //GetAll
        public List<Produit> GetAll()
        {

            List<Produit> result = new List<Produit>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM produit";
            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Produit prod = ProdDataReader(dr);

                    result.Add(prod);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de la lecture des categories : " + exc.Message);
            }
            finally
            {
                // on ferme la connexion dans un finally pour
                // garantir qu'elle sera bien fermée quoiqu'il arrive
                cnx.Close();
            }
            return result;
        }
        public List<ProduitDetails> GetAllDetails()
        {
            List<ProduitDetails> result = new List<ProduitDetails>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT 
                                    p.id p_id, p.cat3_id p_cat3_id, p.conservation_id p_cons_id, p.name p_name,
                                    c3.id c3_id, c3.cat2_id c3_cat2_id, c3.name c3_name,
                                    c2.id c2_id, c2.cat_id c2_cat_id, c2.name c2_name,
                                    c.id c_id, c.name c_name,
                                    cons.id cons_id, cons.name cons_name,
                                    me.product_id me_prod_id, me.unit_id me_unit_id,
                                    u.id u_id, u.name u_name
                                FROM    
                                    produit p
                                    LEFT JOIN cat3 c3 ON p.cat3_id = c3.id
                                    LEFT JOIN cat2 c2 ON c3.cat2_id = c2.id
                                    LEFT JOIN cat c ON c2.cat_id = c.id
                                    LEFT JOIN conservation cons ON p.conservation_id = cons.id
                                    LEFT JOIN ca_mesurer me ON p.id = me.product_id
                                    LEFT JOIN unite u ON u.id = me.unit_id
                                GROUP BY
                                    p_id";
            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    ProduitDetails prod = ProdDetailsDataReader(dr);

                    result.Add(prod);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de la lecture des categories : " + exc.Message);
            }
            finally
            {
                // on ferme la connexion dans un finally pour
                // garantir qu'elle sera bien fermée quoiqu'il arrive
                cnx.Close();
            }
            return result;
        }

        //GetById
        public ProduitDetails GetById(int id)
        {
            ProduitDetails pro = null;

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT 
                                    p.id p_id, p.cat3_id p_cat3_id, p.conservation_id p_cons_id, p.name p_name,
                                    c3.id c3_id, c3.cat2_id c3_cat2_id, c3.name c3_name,
                                    c2.id c2_id, c2.cat_id c2_cat_id, c2.name c2_name,
                                    c.id c_id, c.name c_name,
                                    cons.id cons_id, cons.name cons_name,
                                    me.product_id me_prod_id, me.unit_id me_unit_id,
                                    u.id u_id, u.name u_name
                                FROM
                                    produit p
                                    JOIN cat3 c3 ON p.cat3_id = c3.id
                                    JOIN cat2 c2 ON c3.cat2_id = c2.id
                                    JOIN cat c ON c2.cat_id = c.id
                                    JOIN conservation cons ON p.conservation_id = cons.id
                                    JOIN ca_mesurer me ON me.product_id = p.id
                                    JOIN unite u ON u.id = me.unit_id
                                WHERE 
                                    p.id = @id
                                GROUP BY
                                    p_id";

            cmd.Parameters.Add(new MySqlParameter("id", id));

            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    pro = ProdDetailsDataReader(dr);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de récupération d'un don : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }

            return pro;
        }

        //GetByParentId
        public List<ProduitDetails> GetProduitByCat3Id(int id)
        {
            List<ProduitDetails> result = new List<ProduitDetails>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT 
                                    p.id p_id, p.cat3_id p_cat3_id, p.conservation_id p_cons_id, p.name p_name,
                                    c3.id c3_id, c3.cat2_id c3_cat2_id, c3.name c3_name
                                FROM
                                    produit p
                                    JOIN cat3 c3 ON p.cat3_id = c3.id
                                WHERE 
                                    c3.id = @id";

            cmd.Parameters.Add(new MySqlParameter("id", id));

            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    ProduitDetails prod = ProdbyCat3DataReader(dr);

                    result.Add(prod);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de récupération d'un don : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }

            return result;
        }


        #endregion

        #region FACTORISATIONS
        private Produit ProdDataReader(DbDataReader dr)
        {
            Produit pro = new Produit();

            if (!dr.IsDBNull(dr.GetOrdinal("id")))
            {
                pro.Id = dr.GetInt32(dr.GetOrdinal("id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("cat3_id")))
            {
                pro.Cat3Id = dr.GetInt32(dr.GetOrdinal("cat3_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("conservation_id")))
            {
                pro.ConservationId = dr.GetInt32(dr.GetOrdinal("conservation_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("name")))
            {
                pro.Name = dr.GetString(dr.GetOrdinal("name"));
            }
            return pro;
        }
        private ProduitDetails ProdbyCat3DataReader(DbDataReader dr)
        {
            ProduitDetails pro = new ProduitDetails();

            if (!dr.IsDBNull(dr.GetOrdinal("p_id")))
            {
                pro.Id = dr.GetInt32(dr.GetOrdinal("p_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("p_cat3_id")))
            {
                pro.Cat3Id = dr.GetInt32(dr.GetOrdinal("p_cat3_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("p_cons_id")))
            {
                pro.ConservationId = dr.GetInt32(dr.GetOrdinal("p_cons_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("c3_id")))
            {
                pro.Cat3Id = dr.GetInt32(dr.GetOrdinal("c3_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("c3_cat2_id")))
            {
                pro.Cat2Id = dr.GetInt32(dr.GetOrdinal("c3_cat2_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("p_name")))
            {
                pro.Name = dr.GetString(dr.GetOrdinal("p_name"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("c3_name")))
            {
                pro.Cat3Name = dr.GetString(dr.GetOrdinal("c3_name"));
            }
            return pro;
        }
        private ProduitDetails ProdDetailsDataReader(DbDataReader dr)
        {
            ProduitDetails pro = new ProduitDetails();

            if (!dr.IsDBNull(dr.GetOrdinal("p_id")))
            {
                pro.Id = dr.GetInt32(dr.GetOrdinal("p_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("p_cat3_id")))
            {
                pro.Cat3Id = dr.GetInt32(dr.GetOrdinal("p_cat3_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("p_cons_id")))
            {
                pro.ConservationId = dr.GetInt32(dr.GetOrdinal("p_cons_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("c3_id")))
            {
                pro.Cat3Id = dr.GetInt32(dr.GetOrdinal("c3_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("c3_cat2_id")))
            {
                pro.Cat2Id = dr.GetInt32(dr.GetOrdinal("c3_cat2_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("c2_id")))
            {
                pro.Cat2Id = dr.GetInt32(dr.GetOrdinal("c2_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("c2_cat_id")))
            {
                pro.Cat2Id = dr.GetInt32(dr.GetOrdinal("c2_cat_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("c_id")))
            {
                pro.CatId = dr.GetInt32(dr.GetOrdinal("c_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("cons_id")))
            {
                pro.ConservationId = dr.GetInt32(dr.GetOrdinal("cons_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("me_prod_id")))
            {
                pro.Id = dr.GetInt32(dr.GetOrdinal("me_prod_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("me_unit_id")))
            {
                pro.UniteId = dr.GetInt32(dr.GetOrdinal("me_unit_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("u_id")))
            {
                pro.UniteId = dr.GetInt32(dr.GetOrdinal("u_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("p_name")))
            {
                pro.Name = dr.GetString(dr.GetOrdinal("p_name"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("c3_name")))
            {
                pro.Cat3Name = dr.GetString(dr.GetOrdinal("c3_name"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("c2_name")))
            {
                pro.Cat2Name = dr.GetString(dr.GetOrdinal("c2_name"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("c_name")))
            {
                pro.CatName = dr.GetString(dr.GetOrdinal("c_name"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("cons_name")))
            {
                pro.ConservationName = dr.GetString(dr.GetOrdinal("cons_name"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("u_name")))
            {
                pro.UniteName = dr.GetString(dr.GetOrdinal("u_name"));
            }

            return pro;
        }
        
        #endregion
    }
}