﻿using Fr.EQL.AI110.DLD_GC.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.DataAccess
{
    public class CategorieDao : DAO
    {
        #region READ
        //GetAll
        public List<Categorie> GetAllCategories()
        {

            List<Categorie> result = new List<Categorie>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM cat";
            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Categorie cat = CatDataReader(dr);

                    result.Add(cat);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de la lecture des categories : " + exc.Message);
            }
            finally
            {
                // on ferme la connexion dans un finally pour
                // garantir qu'elle sera bien fermée quoiqu'il arrive
                cnx.Close();
            }
            return result;

        }
        public List<Categorie2> GetAllCategories2()
        {

            List<Categorie2> result = new List<Categorie2>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM cat2";
            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Categorie2 cat = Cat2DataReader(dr);

                    result.Add(cat);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de la lecture des categories : " + exc.Message);
            }
            finally
            {
                // on ferme la connexion dans un finally pour
                // garantir qu'elle sera bien fermée quoiqu'il arrive
                cnx.Close();
            }
            return result;

        }
        public List<Categorie3> GetAllCategories3()
        {

            List<Categorie3> result = new List<Categorie3>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM cat3";
            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Categorie3 cat = Cat3DataReader(dr);

                    result.Add(cat);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de la lecture des categories : " + exc.Message);
            }
            finally
            {
                // on ferme la connexion dans un finally pour
                // garantir qu'elle sera bien fermée quoiqu'il arrive
                cnx.Close();
            }
            return result;

        }

        //GetById
        public Categorie GetCatById(int id)
        {
            Categorie cat = null;

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM cat WHERE id = @id";

            cmd.Parameters.Add(new MySqlParameter("id", id));

            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    cat = CatDataReader(dr);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de récupération d'une catégorie : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }

            return cat;
        }
        public Categorie2 GetCat2ById(int id)
        {
            Categorie2 cat = null;

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM cat2 WHERE id = @id";

            cmd.Parameters.Add(new MySqlParameter("id", id));

            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    cat = Cat2DataReader(dr);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de récupération d'une catégorie : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }

            return cat;
        }
        public Categorie3 GetCat3ById(int id)
        {
            Categorie3 cat = null;

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM cat3 WHERE id = @id";

            cmd.Parameters.Add(new MySqlParameter("id", id));

            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    cat = Cat3DataReader(dr);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de récupération d'une catégorie : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }

            return cat;
        }

        //GetByParentId
        public List<Categorie2> GetCat2ByCatId (int id)
        {
            List<Categorie2> result = new List<Categorie2>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT 
                                    c2.id c2_id, c2.cat_id c2_cat_id, c2.name c2_name,
                                    c.id c_id
                                FROM
	                                cat2 c2 
	                                JOIN cat c ON c2.cat_id=c.id
                                WHERE 
                                    c.id=@id";

            cmd.Parameters.Add(new MySqlParameter("id", id));
            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Categorie2 cat = Cat2byCatDataReader(dr);

                    result.Add(cat);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de la lecture des categories : " + exc.Message);
            }
            finally
            {
                // on ferme la connexion dans un finally pour
                // garantir qu'elle sera bien fermée quoiqu'il arrive
                cnx.Close();
            }
            return result;
        }
        public List<Categorie3> GetCat3ByCat2Id(int id)
        {
            List<Categorie3> result = new List<Categorie3>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT 
                                    c3.id c3_id, c3.cat2_id c3_cat2_id, c3.name c3_name,
                                    c2.id c2_id
                                FROM
	                                cat3 c3 
	                                JOIN cat2 c2 ON c3.cat2_id=c2.id
                                WHERE 
                                    c2.id=@id";

            cmd.Parameters.Add(new MySqlParameter("id", id));
            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Categorie3 cat = Cat3byCat2DataReader(dr);

                    result.Add(cat);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de la lecture des categories : " + exc.Message);
            }
            finally
            {
                // on ferme la connexion dans un finally pour
                // garantir qu'elle sera bien fermée quoiqu'il arrive
                cnx.Close();
            }
            return result;
        }

        #endregion

        #region FACTORISATIONS
        private Categorie CatDataReader(DbDataReader dr)
        {
            Categorie cat = new Categorie();

            if (!dr.IsDBNull(dr.GetOrdinal("id")))
            {
                cat.Id = dr.GetInt32(dr.GetOrdinal("id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("name")))
            {
                cat.Name = dr.GetString(dr.GetOrdinal("name"));
            }
            return cat;
        }
        private Categorie2 Cat2DataReader(DbDataReader dr)
        {
            Categorie2 cat = new Categorie2();

            if (!dr.IsDBNull(dr.GetOrdinal("id")))
            {
                cat.Id = dr.GetInt32(dr.GetOrdinal("id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("cat_id")))
            {
                cat.CatId = dr.GetInt32(dr.GetOrdinal("cat_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("name")))
            {
                cat.Name = dr.GetString(dr.GetOrdinal("name"));
            }
            return cat;
        }
        private Categorie3 Cat3DataReader(DbDataReader dr)
        {
            Categorie3 cat = new Categorie3();

            if (!dr.IsDBNull(dr.GetOrdinal("id")))
            {
                cat.Id = dr.GetInt32(dr.GetOrdinal("id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("cat2_id")))
            {
                cat.Cat2Id = dr.GetInt32(dr.GetOrdinal("cat2_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("name")))
            {
                cat.Name = dr.GetString(dr.GetOrdinal("name"));
            }
            return cat;
        }

        private Categorie2 Cat2byCatDataReader(DbDataReader dr)
        {
            Categorie2 cat = new Categorie2();

            if (!dr.IsDBNull(dr.GetOrdinal("c2_id")))
            {
                cat.Id = dr.GetInt32(dr.GetOrdinal("c2_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("c2_cat_id")))
            {
                cat.CatId = dr.GetInt32(dr.GetOrdinal("c2_cat_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("c_id")))
            {
                cat.CatId = dr.GetInt32(dr.GetOrdinal("c_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("c2_name")))
            {
                cat.Name = dr.GetString(dr.GetOrdinal("c2_name"));
            }
            return cat;
        }
        private Categorie3 Cat3byCat2DataReader(DbDataReader dr)
        {
            Categorie3 cat = new Categorie3();

            if (!dr.IsDBNull(dr.GetOrdinal("c3_id")))
            {
                cat.Id = dr.GetInt32(dr.GetOrdinal("c3_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("c3_cat2_id")))
            {
                cat.Cat2Id = dr.GetInt32(dr.GetOrdinal("c3_cat2_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("c2_id")))
            {
                cat.Cat2Id = dr.GetInt32(dr.GetOrdinal("c2_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("c3_name")))
            {
                cat.Name = dr.GetString(dr.GetOrdinal("c3_name"));
            }
            return cat;
        }
        #endregion
    }
}
