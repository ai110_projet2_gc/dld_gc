﻿using Fr.EQL.AI110.DLD_GC.Entities;
using MySql.Data.MySqlClient;
using System.Data.Common;

namespace Fr.EQL.AI110.DLD_GC.DataAccess
{
    public class UniteDao : DAO
    {
        public List<Unite> GetAllUnites()
        {

            List<Unite> result = new List<Unite>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM unite";
            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Unite unite = DataReaderToEntity(dr);

                    result.Add(unite);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de la lecture des categories : " + exc.Message);
            }
            finally
            {
                // on ferme la connexion dans un finally pour
                // garantir qu'elle sera bien fermée quoiqu'il arrive
                cnx.Close();
            }
            return result;
        }

        private Unite DataReaderToEntity(DbDataReader dr)
        {
            Unite unite = new Unite();

            if (!dr.IsDBNull(dr.GetOrdinal("id")))
            {
                unite.Id = dr.GetInt32(dr.GetOrdinal("id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("name")))
            {
                unite.Name = dr.GetString(dr.GetOrdinal("name"));
            }
            return unite;
        }
    }
}