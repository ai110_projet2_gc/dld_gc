﻿using Fr.EQL.AI110.DLD_GC.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.DataAccess
{
    public class StockageDao : DAO
    {
        public List<StockageDetails> GetAllStockage()
        {
            List<StockageDetails> result = new List<StockageDetails>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM stockage";

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    StockageDetails stoDetails = DataReaderToEntity(dr);
                    result.Add(stoDetails);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la lecture des lieu de stockage " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }
        public List<Relais> GetAllRelais()
        {
            List<Relais> result = new List<Relais>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM relais";

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Relais relais = DataReaderToRelais(dr);
                    result.Add(relais);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la lecture des lieu de stockage " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;

            return result;
        }
        private StockageDetails DataReaderToEntity(DbDataReader dr)
        {
            StockageDetails stockage = new StockageDetails();
            if (!dr.IsDBNull(dr.GetOrdinal("id")))
            {
                stockage.Id = dr.GetInt32(dr.GetOrdinal("id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("accessibilite_id")))
            {
                stockage.AccessibiliteId = dr.GetInt32(dr.GetOrdinal("accessibilite_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("conservation_id")))
            {
                stockage.ConservationId = dr.GetInt32(dr.GetOrdinal("conservation_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("relais_id")))
            {
                stockage.RelaisId = dr.GetInt32(dr.GetOrdinal("relais_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("create_space")))
            {
                stockage.CreateSpace = dr.GetDateTime(dr.GetOrdinal("create_space"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("remove_space")))
            {
                stockage.RemoveSpace = dr.GetDateTime(dr.GetOrdinal("remove_space"));
            }
            return stockage;
        }
        private Relais DataReaderToRelais(DbDataReader dr)
        {
            Relais relais = new Relais();
            if (!dr.IsDBNull(dr.GetOrdinal("id")))
            {
                relais.RelaisId = dr.GetInt32(dr.GetOrdinal("id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("localisation_id")))
            {
                relais.LocalisationId = dr.GetInt32(dr.GetOrdinal("localisation_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("partenaire_id")))
            {
                relais.PartenaireId = dr.GetInt32(dr.GetOrdinal("partenaire_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("name")))
            {
                relais.RelaisName = dr.GetString(dr.GetOrdinal("name"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("street_nb")))
            {
                relais.StreetNumber = dr.GetInt32(dr.GetOrdinal("street_nb"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("street_name")))
            {
                relais.StreetName = dr.GetString(dr.GetOrdinal("street_name"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("comp_adr")))
            {
                relais.CompAdr = dr.GetString(dr.GetOrdinal("comp_adr"));
            }
            return relais;
        }
    }
}
