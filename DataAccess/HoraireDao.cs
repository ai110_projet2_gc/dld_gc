﻿using Fr.EQL.AI110.DLD_GC.Entities;
using MySql.Data.MySqlClient;
using System.Data.Common;

namespace Fr.EQL.AI110.DLD_GC.DataAccess
{
    public class HoraireDao : DAO
    {
        public void Insert(Horaire horaire)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"INSERT INTO Horaire
                            (jour_id, don_id, morning_start, morning_end, afternoon_start, afternoon_end)
                            VALUES
                            (@jour_id, @don_id, @morning_start, @morning_end, @afternoon_start, @afternoon_end)";

            cmd.Parameters.Add(new MySqlParameter("jour_id", horaire.JourId));
            cmd.Parameters.Add(new MySqlParameter("don_id", horaire.DonId));
            cmd.Parameters.Add(new MySqlParameter("morning_start", horaire.MorningStart));
            cmd.Parameters.Add(new MySqlParameter("morning_end", horaire.MorningEnd));
            cmd.Parameters.Add(new MySqlParameter("afternoon_start", horaire.AfternoonStart));
            cmd.Parameters.Add(new MySqlParameter("afternoon_end", horaire.AfternoonEnd));
         
            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la création du Horaire " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
        }

    }
}