-- 2 : On choisi comment les afficher
SELECT  
	CASE
		WHEN d.donor_id=a.id 
			THEN CONCAT(DATE(a.create_date), ' : ', UPPER(a.lastname), ' ', a.firstname, ' (Donateur)')
		ELSE CONCAT(DATE(a.create_date), ' : ', UPPER(a.lastname), ' ', a.firstname, ' (Bénéficiaire)')
	END 'ADHÉRENTS', -- on affiche qui est qui 
    CONCAT('000', d.id, ': ', UPPER(c.name), ' -> ', c2.name, ' -> ', c.name, ' -> ', p.name) DONS
-- 1 : On choisi quelles tables on souhaite exploiter
FROM 
	adherent a
    JOIN don d 		ON a.id=d.beneficiary_id OR a.id=d.donor_id -- donor_id et beneficiary_id retournent des valeurs distinctes
    JOIN produit p 	ON d.produit_id=p.id
    JOIN cat3 c3 	ON p.cat3_id=c3.id
    JOIN cat2 c2 	ON c3.cat2_id=c2.id
    JOIN cat c 		ON c2.cat_id=c.id
-- 3 : On ajoute des filtres dans la requêtes
WHERE 
	a.lastname LIKE '%%'
ORDER BY a.lastname ASC;

-- Thibault :)
	