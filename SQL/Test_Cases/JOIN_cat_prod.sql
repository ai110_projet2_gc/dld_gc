SELECT
	c.name 'Catégorie', 
	c2.name 'Sous-catégorie', 
    c3.name 'Type', 
    p.name 'Produit'
FROM 
	produit p
	JOIN cat3 c3 ON p.cat3_id = c3.id
    JOIN cat2 c2 ON c3.cat2_id = c2.id
    JOIN cat c ON c2.cat_id = c.id
WHERE p.name LIKE '%%'
ORDER BY p.name DESC
;