SELECT 
	a.firstname Prénom,
    a.lastname Nom,
    d.qty Quantité,
    u.name Unité,
    p.name Produit
FROM 
    adherent a
    JOIN don d ON a.id=d.donor_id
    JOIN produit p ON p.id=d.produit_id
    JOIN unite u ON d.unite_id=u.id;