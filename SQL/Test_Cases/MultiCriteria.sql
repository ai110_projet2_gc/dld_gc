SELECT d.*,
                                loc.city don_city, loc.postcode don_postcode,
                                uni.name unite_name, 
                                cons_don.name cons_don_name,

                                donor.id donor_id,donor.firstname donor_firstname,donor.lastname donor_lastname,
                                donor.email donor_email, donor.tel donor_tel, donor.street_nb donor_street_nb, donor.comp_adr donor_comp_adr,
                                donor_loc.city donor_city, donor_loc.postcode donor_postcode,

                                beneficiary.id bene_id, beneficiary.firstname bene_firstname, beneficiary.lastname bene_lastname,
                                beneficiary.email bene_email, beneficiary.tel bene_tel, beneficiary.street_nb bene_street_nb, beneficiary.comp_adr bene_comp_adr,
                                bene_loc.city bene_city, bene_loc.postcode bene_postcode,

                                p.name p_name,
                                c.name cat_name,c2.name cat2_name,c3.name cat3_name,

                                
                                
                                rel.street_nb rel_street_nb, rel.street_name rel_street_name, rel.comp_adr rel_comp_adr,
                                loc_relais.city relais_city, loc_relais.postcode relais_postcode,

                                part.street_nb part_street_nb, part.street_name part_street_name, part.comp_adr part_comp_adr,
                                loc_part.city part_city, loc_part.postcode part_postcode,

                                accs_stockage.name accs_stockage_name, cons_stockage.name cons_stockage_name,

                                horaire_don.morning_start, horaire_don.morning_end, horaire_don.afternoon_start, horaire_don.afternoon_end,
                                jour_horaire_don.name jour_horaire_don,

                                horaire_stockage.morning_start, horaire_stockage.morning_end, horaire_stockage.afternoon_start, horaire_stockage.afternoon_end,
                                jour_horaire_stockage.name jour_horaire_stockage
                                

                                    FROM don d
                                        JOIN adherent donor ON d.donor_id=donor.id
                                        LEFT JOIN localisation donor_loc ON donor_loc.id=donor.localisation_id
                                        JOIN adherent beneficiary ON d.beneficiary_id=beneficiary.id
                                        LEFT JOIN localisation bene_loc ON bene_loc.id=beneficiary.localisation_id
                                        JOIN produit p ON d.produit_id=p.id
								        LEFT JOIN unite uni ON d.unite_id=uni.id
                                        LEFT JOIN stockage stock ON d.stockage_id=stock.id
                                        LEFT JOIN conservation cons_stockage ON stock.conservation_id=cons_stockage.id
                                        LEFT JOIN localisation loc ON d.localisation_id=loc.id
                                        LEFT JOIN cat3 c3 ON p.cat3_id=c3.id
                                        LEFT JOIN cat2 c2 ON c3.cat2_id=c2.id
                                        LEFT JOIN cat c ON c2.cat_id=c.id
                                        LEFT JOIN relais rel ON stock.relais_id=rel.id
                                        LEFT JOIN localisation loc_relais ON loc_relais.id=rel.localisation_id
                                        LEFT JOIN partenaire part ON part.id = rel.partenaire_id
                                        LEFT JOIN localisation loc_part ON loc_part.id=part.localisation_id
                                        LEFT JOIN accessibilite accs_stockage ON accs_stockage.id = stock.accessibilite_id
                                        LEFT JOIN conservation cons_don ON cons_don.id = d.conservation_id
                                        LEFT JOIN horaire horaire_don ON horaire_don.id = d.id
                                        LEFT JOIN horaire horaire_stockage ON horaire_stockage.id = stock.id
                                        LEFT JOIN jour jour_horaire_don ON jour_horaire_don.id = horaire_don.jour_id
                                        LEFT JOIN jour jour_horaire_stockage ON jour_horaire_stockage.id = horaire_stockage.jour_id
