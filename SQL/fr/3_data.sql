-- MySQL dump 10.13  Distrib 8.0.27, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: projet2_dld
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accessibilite`
--

DROP TABLE IF EXISTS `accessibilite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `accessibilite` (
  `Id_accessibilite` int NOT NULL AUTO_INCREMENT,
  `Nom_accessibilite` varchar(254) DEFAULT NULL,
  PRIMARY KEY (`Id_accessibilite`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accessibilite`
--

LOCK TABLES `accessibilite` WRITE;
/*!40000 ALTER TABLE `accessibilite` DISABLE KEYS */;
INSERT INTO `accessibilite` VALUES (1,'commerçant'),(2,'pick-up box'),(3,'donateur');
/*!40000 ALTER TABLE `accessibilite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `achat_credits`
--

DROP TABLE IF EXISTS `achat_credits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `achat_credits` (
  `Id_adherent` int NOT NULL,
  `Id_carnet` int NOT NULL,
  `Date` datetime DEFAULT NULL,
  `Quantite` int DEFAULT NULL,
  PRIMARY KEY (`Id_adherent`,`Id_carnet`),
  KEY `FK_Association_33B` (`Id_carnet`),
  CONSTRAINT `FK_Association_33A` FOREIGN KEY (`Id_adherent`) REFERENCES `adherent` (`Id_adherent`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Association_33B` FOREIGN KEY (`Id_carnet`) REFERENCES `carnet_credits` (`Id_carnet`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `achat_credits`
--

LOCK TABLES `achat_credits` WRITE;
/*!40000 ALTER TABLE `achat_credits` DISABLE KEYS */;
/*!40000 ALTER TABLE `achat_credits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `adherent`
--

DROP TABLE IF EXISTS `adherent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `adherent` (
  `Id_adherent` int NOT NULL AUTO_INCREMENT,
  `Id_ville` int NOT NULL,
  `Nom` varchar(254) DEFAULT NULL,
  `Prenom` varchar(254) DEFAULT NULL,
  `Date_de_naissance` datetime DEFAULT NULL,
  `Email` varchar(254) DEFAULT NULL,
  `Mot_de_passe` varchar(254) DEFAULT NULL,
  `Numero_voie` int DEFAULT NULL,
  `Libelle_voie` varchar(254) DEFAULT NULL,
  `Complement_adresse` varchar(254) DEFAULT NULL,
  `Telephone` varchar(254) DEFAULT NULL,
  `Date_creation_compte` datetime DEFAULT NULL,
  `Date_confirmation_inscription` datetime DEFAULT NULL,
  `Date_desactivation` datetime DEFAULT NULL,
  `Code_valid_mail` int DEFAULT NULL,
  PRIMARY KEY (`Id_adherent`),
  KEY `FK_Association_3` (`Id_ville`),
  CONSTRAINT `FK_Association_3` FOREIGN KEY (`Id_ville`) REFERENCES `localisation` (`Id_ville`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adherent`
--

LOCK TABLES `adherent` WRITE;
/*!40000 ALTER TABLE `adherent` DISABLE KEYS */;
/*!40000 ALTER TABLE `adherent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `carnet_credits`
--

DROP TABLE IF EXISTS `carnet_credits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `carnet_credits` (
  `Id_carnet` int NOT NULL AUTO_INCREMENT,
  `Nom` varchar(254) DEFAULT NULL,
  `Nombre_credits` int DEFAULT NULL,
  `Prix` float DEFAULT NULL,
  PRIMARY KEY (`Id_carnet`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carnet_credits`
--

LOCK TABLES `carnet_credits` WRITE;
/*!40000 ALTER TABLE `carnet_credits` DISABLE KEYS */;
INSERT INTO `carnet_credits` VALUES (1,'unité',1,1),(2,'carnet de 5',5,4),(3,'carnet de 10',10,7);
/*!40000 ALTER TABLE `carnet_credits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categorie` (
  `Id_categorie` int NOT NULL AUTO_INCREMENT,
  `Nom_catg` varchar(254) DEFAULT NULL,
  PRIMARY KEY (`Id_categorie`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorie`
--

LOCK TABLES `categorie` WRITE;
/*!40000 ALTER TABLE `categorie` DISABLE KEYS */;
INSERT INTO `categorie` VALUES (1,'Fruits et Légumes'),(2,'Viandes et Poissons'),(3,'Pains et Pâtisseries'),(4,'Frais'),(5,'Surgelés'),(6,'Boissons'),(7,'Epicerie salée'),(8,'Epicerie sucrée');
/*!40000 ALTER TABLE `categorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dimensions`
--

DROP TABLE IF EXISTS `dimensions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dimensions` (
  `Id_dimensions` int NOT NULL AUTO_INCREMENT,
  `Id_stockage` int NOT NULL,
  `Nom` varchar(254) DEFAULT NULL,
  `Hauteur` int DEFAULT NULL,
  `Largeur` int DEFAULT NULL,
  `Profondeur` int DEFAULT NULL,
  PRIMARY KEY (`Id_dimensions`),
  KEY `FK_Association_23` (`Id_stockage`),
  CONSTRAINT `FK_Association_23` FOREIGN KEY (`Id_stockage`) REFERENCES `espace_stockage` (`Id_stockage`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dimensions`
--

LOCK TABLES `dimensions` WRITE;
/*!40000 ALTER TABLE `dimensions` DISABLE KEYS */;
/*!40000 ALTER TABLE `dimensions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `distance`
--

DROP TABLE IF EXISTS `distance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `distance` (
  `Id_ville` int NOT NULL,
  `Loc_Id_ville` int NOT NULL,
  `Distance_moyenne` int DEFAULT NULL,
  PRIMARY KEY (`Id_ville`,`Loc_Id_ville`),
  KEY `FK_Association_33` (`Loc_Id_ville`),
  CONSTRAINT `FK_Association_32` FOREIGN KEY (`Id_ville`) REFERENCES `localisation` (`Id_ville`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Association_33` FOREIGN KEY (`Loc_Id_ville`) REFERENCES `localisation` (`Id_ville`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `distance`
--

LOCK TABLES `distance` WRITE;
/*!40000 ALTER TABLE `distance` DISABLE KEYS */;
/*!40000 ALTER TABLE `distance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `don`
--

DROP TABLE IF EXISTS `don`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `don` (
  `Id_don` int NOT NULL AUTO_INCREMENT,
  `Id_adherent` int DEFAULT NULL,
  `Id_produit` int NOT NULL,
  `Adh_Id_adherent` int NOT NULL,
  `Id_stockage` int DEFAULT NULL,
  `Id_unite_mesure` int NOT NULL,
  `Id_type_conservation` int NOT NULL,
  `Id_ville` int DEFAULT NULL,
  `Quantite_produit` float DEFAULT NULL,
  `DLC` datetime DEFAULT NULL,
  `DDM` datetime DEFAULT NULL,
  `Date_proposition` datetime DEFAULT NULL,
  `Date_reservation` datetime DEFAULT NULL,
  `Date_annulation` datetime DEFAULT NULL,
  `Photo` varchar(254) DEFAULT NULL,
  `Numero_voie_alt` int DEFAULT NULL,
  `Libelle_voie_alt` varchar(254) DEFAULT NULL,
  `Complement_adresse_alt` int DEFAULT NULL,
  `Date_disponibilite_don` datetime DEFAULT NULL,
  `Date_finalisation_echange` datetime DEFAULT NULL,
  PRIMARY KEY (`Id_don`),
  KEY `FK_Association_11` (`Id_stockage`),
  KEY `FK_Association_19` (`Id_type_conservation`),
  KEY `FK_Association_31` (`Id_unite_mesure`),
  KEY `FK_Association_5` (`Id_ville`),
  KEY `FK_Association_7` (`Id_produit`),
  KEY `FK_Beneficiaire` (`Id_adherent`),
  KEY `FK_Donateur` (`Adh_Id_adherent`),
  CONSTRAINT `FK_Association_11` FOREIGN KEY (`Id_stockage`) REFERENCES `espace_stockage` (`Id_stockage`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Association_19` FOREIGN KEY (`Id_type_conservation`) REFERENCES `type_conservation` (`Id_type_conservation`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Association_31` FOREIGN KEY (`Id_unite_mesure`) REFERENCES `unite_mesure` (`Id_unite_mesure`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Association_5` FOREIGN KEY (`Id_ville`) REFERENCES `localisation` (`Id_ville`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Association_7` FOREIGN KEY (`Id_produit`) REFERENCES `produit` (`Id_produit`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Beneficiaire` FOREIGN KEY (`Id_adherent`) REFERENCES `adherent` (`Id_adherent`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Donateur` FOREIGN KEY (`Adh_Id_adherent`) REFERENCES `adherent` (`Id_adherent`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `don`
--

LOCK TABLES `don` WRITE;
/*!40000 ALTER TABLE `don` DISABLE KEYS */;
/*!40000 ALTER TABLE `don` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `espace_stockage`
--

DROP TABLE IF EXISTS `espace_stockage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `espace_stockage` (
  `Id_stockage` int NOT NULL AUTO_INCREMENT,
  `Id_accessibilite` int NOT NULL,
  `Id_type_conservation` int NOT NULL,
  `Id_relais` int NOT NULL,
  `Date_ajout_espace` datetime DEFAULT NULL,
  `Date_retrait_espace` datetime DEFAULT NULL,
  PRIMARY KEY (`Id_stockage`),
  KEY `FK_Association_13` (`Id_relais`),
  KEY `FK_Association_18` (`Id_type_conservation`),
  KEY `FK_Association_25` (`Id_accessibilite`),
  CONSTRAINT `FK_Association_13` FOREIGN KEY (`Id_relais`) REFERENCES `point_relais` (`Id_relais`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Association_18` FOREIGN KEY (`Id_type_conservation`) REFERENCES `type_conservation` (`Id_type_conservation`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Association_25` FOREIGN KEY (`Id_accessibilite`) REFERENCES `accessibilite` (`Id_accessibilite`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `espace_stockage`
--

LOCK TABLES `espace_stockage` WRITE;
/*!40000 ALTER TABLE `espace_stockage` DISABLE KEYS */;
/*!40000 ALTER TABLE `espace_stockage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `horaires`
--

DROP TABLE IF EXISTS `horaires`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `horaires` (
  `Id_horaires` int NOT NULL AUTO_INCREMENT,
  `Id_jours_semaine` int DEFAULT NULL,
  `Id_don` int NOT NULL,
  `Id_stockage` int NOT NULL,
  `Debut_matin` datetime DEFAULT NULL,
  `Fin_matin` datetime DEFAULT NULL,
  `Debut_aprem` datetime DEFAULT NULL,
  `Fin_apres` datetime DEFAULT NULL,
  PRIMARY KEY (`Id_horaires`),
  KEY `FK_Association_21` (`Id_jours_semaine`),
  KEY `FK_Association_24` (`Id_stockage`),
  KEY `FK_Association_26` (`Id_don`),
  CONSTRAINT `FK_Association_21` FOREIGN KEY (`Id_jours_semaine`) REFERENCES `jours_semaine` (`Id_jours_semaine`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Association_24` FOREIGN KEY (`Id_stockage`) REFERENCES `espace_stockage` (`Id_stockage`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Association_26` FOREIGN KEY (`Id_don`) REFERENCES `don` (`Id_don`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `horaires`
--

LOCK TABLES `horaires` WRITE;
/*!40000 ALTER TABLE `horaires` DISABLE KEYS */;
/*!40000 ALTER TABLE `horaires` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `indispo`
--

DROP TABLE IF EXISTS `indispo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `indispo` (
  `Id_don` int NOT NULL,
  `Id_jours_semaine` int NOT NULL,
  PRIMARY KEY (`Id_don`,`Id_jours_semaine`),
  KEY `FK_indispoB` (`Id_jours_semaine`),
  CONSTRAINT `FK_indispoA` FOREIGN KEY (`Id_don`) REFERENCES `don` (`Id_don`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_indispoB` FOREIGN KEY (`Id_jours_semaine`) REFERENCES `jours_semaine` (`Id_jours_semaine`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `indispo`
--

LOCK TABLES `indispo` WRITE;
/*!40000 ALTER TABLE `indispo` DISABLE KEYS */;
/*!40000 ALTER TABLE `indispo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jours_semaine`
--

DROP TABLE IF EXISTS `jours_semaine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jours_semaine` (
  `Id_jours_semaine` int NOT NULL AUTO_INCREMENT,
  `Nom_jour` varchar(254) DEFAULT NULL,
  PRIMARY KEY (`Id_jours_semaine`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jours_semaine`
--

LOCK TABLES `jours_semaine` WRITE;
/*!40000 ALTER TABLE `jours_semaine` DISABLE KEYS */;
/*!40000 ALTER TABLE `jours_semaine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `localisation`
--

DROP TABLE IF EXISTS `localisation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `localisation` (
  `Id_ville` int NOT NULL AUTO_INCREMENT,
  `Ville_commune` varchar(254) NOT NULL,
  `Code_postal` varchar(254) NOT NULL,
  PRIMARY KEY (`Id_ville`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `localisation`
--

LOCK TABLES `localisation` WRITE;
/*!40000 ALTER TABLE `localisation` DISABLE KEYS */;
/*!40000 ALTER TABLE `localisation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mesurer`
--

DROP TABLE IF EXISTS `mesurer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mesurer` (
  `Id_produit` int NOT NULL,
  `Id_unite_mesure` int NOT NULL,
  PRIMARY KEY (`Id_produit`,`Id_unite_mesure`),
  KEY `FK_mesurerB` (`Id_unite_mesure`),
  CONSTRAINT `FK_mesurerA` FOREIGN KEY (`Id_produit`) REFERENCES `produit` (`Id_produit`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_mesurerB` FOREIGN KEY (`Id_unite_mesure`) REFERENCES `unite_mesure` (`Id_unite_mesure`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mesurer`
--

LOCK TABLES `mesurer` WRITE;
/*!40000 ALTER TABLE `mesurer` DISABLE KEYS */;
/*!40000 ALTER TABLE `mesurer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partenaire`
--

DROP TABLE IF EXISTS `partenaire`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `partenaire` (
  `Id_partenaire` int NOT NULL AUTO_INCREMENT,
  `Id_ville` int NOT NULL,
  `Nom` varchar(254) DEFAULT NULL,
  `Email` varchar(254) DEFAULT NULL,
  `Mot_de_passe` varchar(254) DEFAULT NULL,
  `Telephone` varchar(254) DEFAULT NULL,
  `Numero_voie` int DEFAULT NULL,
  `Libelle_voie` varchar(254) DEFAULT NULL,
  `Complement_adresse` varchar(254) DEFAULT NULL,
  `Date_enregistrement` datetime DEFAULT NULL,
  `Date_validation` datetime DEFAULT NULL,
  `Date_desactivation` datetime DEFAULT NULL,
  `Code_valid_mail` int DEFAULT NULL,
  PRIMARY KEY (`Id_partenaire`),
  KEY `FK_Association_4` (`Id_ville`),
  CONSTRAINT `FK_Association_4` FOREIGN KEY (`Id_ville`) REFERENCES `localisation` (`Id_ville`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partenaire`
--

LOCK TABLES `partenaire` WRITE;
/*!40000 ALTER TABLE `partenaire` DISABLE KEYS */;
/*!40000 ALTER TABLE `partenaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `periode_indisponibilite`
--

DROP TABLE IF EXISTS `periode_indisponibilite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `periode_indisponibilite` (
  `Id_indisponibilite` int NOT NULL AUTO_INCREMENT,
  `Id_adherent` int NOT NULL,
  `Id_stockage` int NOT NULL,
  `Debut_periode` datetime DEFAULT NULL,
  `Fin_periode` datetime DEFAULT NULL,
  `Date_enregistrement_periode` datetime DEFAULT NULL,
  `Date_annulation_periode` datetime DEFAULT NULL,
  PRIMARY KEY (`Id_indisponibilite`),
  KEY `FK_Association_27` (`Id_adherent`),
  KEY `FK_Association_28` (`Id_stockage`),
  CONSTRAINT `FK_Association_27` FOREIGN KEY (`Id_adherent`) REFERENCES `adherent` (`Id_adherent`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Association_28` FOREIGN KEY (`Id_stockage`) REFERENCES `espace_stockage` (`Id_stockage`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `periode_indisponibilite`
--

LOCK TABLES `periode_indisponibilite` WRITE;
/*!40000 ALTER TABLE `periode_indisponibilite` DISABLE KEYS */;
/*!40000 ALTER TABLE `periode_indisponibilite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `point_relais`
--

DROP TABLE IF EXISTS `point_relais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `point_relais` (
  `Id_relais` int NOT NULL AUTO_INCREMENT,
  `Id_ville` int DEFAULT NULL,
  `Id_partenaire` int NOT NULL,
  `Nom` varchar(254) DEFAULT NULL,
  `Numero_voie` int DEFAULT NULL,
  `Libelle_voie` varchar(254) DEFAULT NULL,
  `Complement_adresse` varchar(254) DEFAULT NULL,
  PRIMARY KEY (`Id_relais`),
  KEY `FK_Association_12` (`Id_partenaire`),
  KEY `FK_Association_6` (`Id_ville`),
  CONSTRAINT `FK_Association_12` FOREIGN KEY (`Id_partenaire`) REFERENCES `partenaire` (`Id_partenaire`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Association_6` FOREIGN KEY (`Id_ville`) REFERENCES `localisation` (`Id_ville`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `point_relais`
--

LOCK TABLES `point_relais` WRITE;
/*!40000 ALTER TABLE `point_relais` DISABLE KEYS */;
/*!40000 ALTER TABLE `point_relais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produit`
--

DROP TABLE IF EXISTS `produit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `produit` (
  `Id_produit` int NOT NULL AUTO_INCREMENT,
  `Id_sous_catg_2` int DEFAULT NULL,
  `Id_type_conservation` int NOT NULL,
  `Nom` varchar(254) DEFAULT NULL,
  PRIMARY KEY (`Id_produit`),
  KEY `FK_Association_20` (`Id_type_conservation`),
  KEY `FK_Association_30` (`Id_sous_catg_2`),
  CONSTRAINT `FK_Association_20` FOREIGN KEY (`Id_type_conservation`) REFERENCES `type_conservation` (`Id_type_conservation`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Association_30` FOREIGN KEY (`Id_sous_catg_2`) REFERENCES `sous_categorie_2` (`Id_sous_catg_2`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produit`
--

LOCK TABLES `produit` WRITE;
/*!40000 ALTER TABLE `produit` DISABLE KEYS */;
INSERT INTO `produit` VALUES (7,23,4,'Orange'),(8,23,4,'Citron'),(9,23,4,'Pamplemousse');
/*!40000 ALTER TABLE `produit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sous_categorie`
--

DROP TABLE IF EXISTS `sous_categorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sous_categorie` (
  `Id_sous_catg` int NOT NULL AUTO_INCREMENT,
  `Id_categorie` int NOT NULL,
  `Nom_sous_catg` varchar(254) DEFAULT NULL,
  PRIMARY KEY (`Id_sous_catg`),
  KEY `FK_Association_17` (`Id_categorie`),
  CONSTRAINT `FK_Association_17` FOREIGN KEY (`Id_categorie`) REFERENCES `categorie` (`Id_categorie`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sous_categorie`
--

LOCK TABLES `sous_categorie` WRITE;
/*!40000 ALTER TABLE `sous_categorie` DISABLE KEYS */;
INSERT INTO `sous_categorie` VALUES (33,1,'Fruits'),(34,1,'Légumes'),(35,1,'Jus de fruits et Légumes frais'),(36,1,'Fruits et Légumes secs');
/*!40000 ALTER TABLE `sous_categorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sous_categorie_2`
--

DROP TABLE IF EXISTS `sous_categorie_2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sous_categorie_2` (
  `Id_sous_catg_2` int NOT NULL AUTO_INCREMENT,
  `Id_sous_catg` int NOT NULL,
  `Nom_sous_catg_2` varchar(254) DEFAULT NULL,
  PRIMARY KEY (`Id_sous_catg_2`),
  KEY `FK_Association_29` (`Id_sous_catg`),
  CONSTRAINT `FK_Association_29` FOREIGN KEY (`Id_sous_catg`) REFERENCES `sous_categorie` (`Id_sous_catg`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sous_categorie_2`
--

LOCK TABLES `sous_categorie_2` WRITE;
/*!40000 ALTER TABLE `sous_categorie_2` DISABLE KEYS */;
INSERT INTO `sous_categorie_2` VALUES (23,33,'Agrumes'),(24,33,'Pommes, Poires et Raisins'),(25,33,'Bananes et Kiwis'),(26,33,'Prunes, Abricots, Pêches'),(27,33,'Fruits rouges'),(28,33,'Melons et Pastèques'),(29,33,'Fruits exotiques');
/*!40000 ALTER TABLE `sous_categorie_2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_conservation`
--

DROP TABLE IF EXISTS `type_conservation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type_conservation` (
  `Id_type_conservation` int NOT NULL AUTO_INCREMENT,
  `Nom_type_conservation` varchar(254) DEFAULT NULL,
  PRIMARY KEY (`Id_type_conservation`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_conservation`
--

LOCK TABLES `type_conservation` WRITE;
/*!40000 ALTER TABLE `type_conservation` DISABLE KEYS */;
INSERT INTO `type_conservation` VALUES (1,'refrigéré'),(2,'congélé'),(3,'surgélé'),(4,'temperature ambiante');
/*!40000 ALTER TABLE `type_conservation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unite_mesure`
--

DROP TABLE IF EXISTS `unite_mesure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `unite_mesure` (
  `Id_unite_mesure` int NOT NULL AUTO_INCREMENT,
  `Nom` varchar(254) NOT NULL,
  PRIMARY KEY (`Id_unite_mesure`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unite_mesure`
--

LOCK TABLES `unite_mesure` WRITE;
/*!40000 ALTER TABLE `unite_mesure` DISABLE KEYS */;
INSERT INTO `unite_mesure` VALUES (1,'g'),(2,'kg'),(3,'ml'),(4,'cl'),(5,'l'),(6,'pièce'),(7,'boîte'),(8,'botte'),(9,'cagette');
/*!40000 ALTER TABLE `unite_mesure` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-10 13:48:01
