/*==============================================================*/
/* Nom de SGBD :  MySQL 5.0                                     */
/* Date de création :  10/12/2021 12:11:26                      */
/*==============================================================*/


drop table if exists Accessibilite;

drop table if exists Achat_credits;

drop table if exists Adherent;

drop table if exists Carnet_credits;

drop table if exists Categorie;

drop table if exists Dimensions;

drop table if exists Distance;

drop table if exists Don;

drop table if exists Espace_stockage;

drop table if exists Horaires;

drop table if exists Jours_semaine;

drop table if exists Localisation;

drop table if exists Partenaire;

drop table if exists Periode_indisponibilite;

drop table if exists Point_relais;

drop table if exists Produit;

drop table if exists Sous_categorie;

drop table if exists Sous_categorie_2;

drop table if exists Type_conservation;

drop table if exists Unite_mesure;

drop table if exists indispo;

drop table if exists mesurer;

/*==============================================================*/
/* Table : Accessibilite                                        */
/*==============================================================*/
create table Accessibilite
(
   Id_accessibilite     int not null auto_increment,
   Nom_accessibilite    varchar(254),
   primary key (Id_accessibilite)
);

/*==============================================================*/
/* Table : Achat_credits                                        */
/*==============================================================*/
create table Achat_credits
(
   Id_adherent          int not null,
   Id_carnet            int not null,
   Date                 datetime,
   Quantite             int,
   primary key (Id_adherent, Id_carnet)
);

/*==============================================================*/
/* Table : Adherent                                             */
/*==============================================================*/
create table Adherent
(
   Id_adherent          int not null auto_increment,
   Id_ville             int not null,
   Nom                  varchar(254),
   Prenom               varchar(254),
   Date_de_naissance    datetime,
   Email                varchar(254),
   Mot_de_passe         varchar(254),
   Numero_voie          int,
   Libelle_voie         varchar(254),
   Complement_adresse   varchar(254),
   Telephone            varchar(254),
   Date_creation_compte datetime,
   Date_confirmation_inscription datetime,
   Date_desactivation   datetime,
   Code_valid_mail      int,
   primary key (Id_adherent)
);

/*==============================================================*/
/* Table : Carnet_credits                                       */
/*==============================================================*/
create table Carnet_credits
(
   Id_carnet            int not null auto_increment,
   Nom                  varchar(254),
   Nombre_credits       int,
   Prix                 float,
   primary key (Id_carnet)
);

/*==============================================================*/
/* Table : Categorie                                            */
/*==============================================================*/
create table Categorie
(
   Id_categorie         int not null auto_increment,
   Nom_catg             varchar(254),
   primary key (Id_categorie)
);

/*==============================================================*/
/* Table : Dimensions                                           */
/*==============================================================*/
create table Dimensions
(
   Id_dimensions        int not null auto_increment,
   Id_stockage          int not null,
   Nom                  varchar(254),
   Hauteur              int,
   Largeur              int,
   Profondeur           int,
   primary key (Id_dimensions)
);

/*==============================================================*/
/* Table : Distance                                             */
/*==============================================================*/
create table Distance
(
   Id_ville             int not null,
   Loc_Id_ville         int not null,
   Distance_moyenne     int,
   primary key (Id_ville, Loc_Id_ville)
);

/*==============================================================*/
/* Table : Don                                                  */
/*==============================================================*/
create table Don
(
   Id_don               int not null auto_increment,
   Id_adherent          int,
   Id_produit           int not null,
   Adh_Id_adherent      int not null,
   Id_stockage          int,
   Id_unite_mesure      int not null,
   Id_type_conservation int not null,
   Id_ville             int,
   Quantite_produit     float,
   DLC                  datetime,
   DDM                  datetime,
   Date_proposition     datetime,
   Date_reservation     datetime,
   Date_annulation      datetime,
   Photo                varchar(254),
   Numero_voie_alt      int,
   Libelle_voie_alt     varchar(254),
   Complement_adresse_alt int,
   Date_disponibilite_don datetime,
   Date_finalisation_echange datetime,
   primary key (Id_don)
);

/*==============================================================*/
/* Table : Espace_stockage                                      */
/*==============================================================*/
create table Espace_stockage
(
   Id_stockage          int not null auto_increment,
   Id_accessibilite     int not null,
   Id_type_conservation int not null,
   Id_relais            int not null,
   Date_ajout_espace    datetime,
   Date_retrait_espace  datetime,
   primary key (Id_stockage)
);

/*==============================================================*/
/* Table : Horaires                                             */
/*==============================================================*/
create table Horaires
(
   Id_horaires          int not null auto_increment,
   Id_jours_semaine     int,
   Id_don               int not null,
   Id_stockage          int not null,
   Debut_matin          datetime,
   Fin_matin            datetime,
   Debut_aprem          datetime,
   Fin_apres            datetime,
   primary key (Id_horaires)
);

/*==============================================================*/
/* Table : Jours_semaine                                        */
/*==============================================================*/
create table Jours_semaine
(
   Id_jours_semaine     int not null auto_increment,
   Nom_jour             varchar(254),
   primary key (Id_jours_semaine)
);

/*==============================================================*/
/* Table : Localisation                                         */
/*==============================================================*/
create table Localisation
(
   Id_ville             int not null auto_increment,
   Ville_commune        varchar(254) not null,
   Code_postal          varchar(254) not null,
   primary key (Id_ville)
);

/*==============================================================*/
/* Table : Partenaire                                           */
/*==============================================================*/
create table Partenaire
(
   Id_partenaire        int not null auto_increment,
   Id_ville             int not null,
   Nom                  varchar(254),
   Email                varchar(254),
   Mot_de_passe         varchar(254),
   Telephone            varchar(254),
   Numero_voie          int,
   Libelle_voie         varchar(254),
   Complement_adresse   varchar(254),
   Date_enregistrement  datetime,
   Date_validation      datetime,
   Date_desactivation   datetime,
   Code_valid_mail      int,
   primary key (Id_partenaire)
);

/*==============================================================*/
/* Table : Periode_indisponibilite                              */
/*==============================================================*/
create table Periode_indisponibilite
(
   Id_indisponibilite   int not null auto_increment,
   Id_adherent          int not null,
   Id_stockage          int not null,
   Debut_periode        datetime,
   Fin_periode          datetime,
   Date_enregistrement_periode datetime,
   Date_annulation_periode datetime,
   primary key (Id_indisponibilite)
);

/*==============================================================*/
/* Table : Point_relais                                         */
/*==============================================================*/
create table Point_relais
(
   Id_relais            int not null auto_increment,
   Id_ville             int,
   Id_partenaire        int not null,
   Nom                  varchar(254),
   Numero_voie          int,
   Libelle_voie         varchar(254),
   Complement_adresse   varchar(254),
   primary key (Id_relais)
);

/*==============================================================*/
/* Table : Produit                                              */
/*==============================================================*/
create table Produit
(
   Id_produit           int not null auto_increment,
   Id_sous_catg_2       int,
   Id_type_conservation int not null,
   Nom                  varchar(254),
   primary key (Id_produit)
);

/*==============================================================*/
/* Table : Sous_categorie                                       */
/*==============================================================*/
create table Sous_categorie
(
   Id_sous_catg         int not null auto_increment,
   Id_categorie         int not null,
   Nom_sous_catg        varchar(254),
   primary key (Id_sous_catg)
);

/*==============================================================*/
/* Table : Sous_categorie_2                                     */
/*==============================================================*/
create table Sous_categorie_2
(
   Id_sous_catg_2       int not null auto_increment,
   Id_sous_catg         int not null,
   Nom_sous_catg_2      varchar(254),
   primary key (Id_sous_catg_2)
);

/*==============================================================*/
/* Table : Type_conservation                                    */
/*==============================================================*/
create table Type_conservation
(
   Id_type_conservation int not null auto_increment,
   Nom_type_conservation varchar(254),
   primary key (Id_type_conservation)
);

/*==============================================================*/
/* Table : Unite_mesure                                         */
/*==============================================================*/
create table Unite_mesure
(
   Id_unite_mesure      int not null auto_increment,
   Nom                  varchar(254) not null,
   primary key (Id_unite_mesure)
);

/*==============================================================*/
/* Table : indispo                                              */
/*==============================================================*/
create table indispo
(
   Id_don               int not null,
   Id_jours_semaine     int not null,
   primary key (Id_don, Id_jours_semaine)
);

/*==============================================================*/
/* Table : mesurer                                              */
/*==============================================================*/
create table mesurer
(
   Id_produit           int not null,
   Id_unite_mesure      int not null,
   primary key (Id_produit, Id_unite_mesure)
);

alter table Achat_credits add constraint FK_Association_Adherent foreign key (Id_adherent)
      references Adherent (Id_adherent) on delete restrict on update restrict;

alter table Achat_credits add constraint FK_Association_Carnet foreign key (Id_carnet)
      references Carnet_credits (Id_carnet) on delete restrict on update restrict;

alter table Adherent add constraint FK_Association_3 foreign key (Id_ville)
      references Localisation (Id_ville) on delete restrict on update restrict;

alter table Dimensions add constraint FK_Association_23 foreign key (Id_stockage)
      references Espace_stockage (Id_stockage) on delete restrict on update restrict;

alter table Distance add constraint FK_Association_32 foreign key (Id_ville)
      references Localisation (Id_ville) on delete restrict on update restrict;

alter table Distance add constraint FK_Association_33 foreign key (Loc_Id_ville)
      references Localisation (Id_ville) on delete restrict on update restrict;

alter table Don add constraint FK_Association_11 foreign key (Id_stockage)
      references Espace_stockage (Id_stockage) on delete restrict on update restrict;

alter table Don add constraint FK_Association_19 foreign key (Id_type_conservation)
      references Type_conservation (Id_type_conservation) on delete restrict on update restrict;

alter table Don add constraint FK_Association_31 foreign key (Id_unite_mesure)
      references Unite_mesure (Id_unite_mesure) on delete restrict on update restrict;

alter table Don add constraint FK_Association_5 foreign key (Id_ville)
      references Localisation (Id_ville) on delete restrict on update restrict;

alter table Don add constraint FK_Association_7 foreign key (Id_produit)
      references Produit (Id_produit) on delete restrict on update restrict;

alter table Don add constraint FK_Beneficiaire foreign key (Id_adherent)
      references Adherent (Id_adherent) on delete restrict on update restrict;

alter table Don add constraint FK_Donateur foreign key (Adh_Id_adherent)
      references Adherent (Id_adherent) on delete restrict on update restrict;

alter table Espace_stockage add constraint FK_Association_13 foreign key (Id_relais)
      references Point_relais (Id_relais) on delete restrict on update restrict;

alter table Espace_stockage add constraint FK_Association_18 foreign key (Id_type_conservation)
      references Type_conservation (Id_type_conservation) on delete restrict on update restrict;

alter table Espace_stockage add constraint FK_Association_25 foreign key (Id_accessibilite)
      references Accessibilite (Id_accessibilite) on delete restrict on update restrict;

alter table Horaires add constraint FK_Association_21 foreign key (Id_jours_semaine)
      references Jours_semaine (Id_jours_semaine) on delete restrict on update restrict;

alter table Horaires add constraint FK_Association_24 foreign key (Id_stockage)
      references Espace_stockage (Id_stockage) on delete restrict on update restrict;

alter table Horaires add constraint FK_Association_26 foreign key (Id_don)
      references Don (Id_don) on delete restrict on update restrict;

alter table Partenaire add constraint FK_Association_4 foreign key (Id_ville)
      references Localisation (Id_ville) on delete restrict on update restrict;

alter table Periode_indisponibilite add constraint FK_Association_27 foreign key (Id_adherent)
      references Adherent (Id_adherent) on delete restrict on update restrict;

alter table Periode_indisponibilite add constraint FK_Association_28 foreign key (Id_stockage)
      references Espace_stockage (Id_stockage) on delete restrict on update restrict;

alter table Point_relais add constraint FK_Association_12 foreign key (Id_partenaire)
      references Partenaire (Id_partenaire) on delete restrict on update restrict;

alter table Point_relais add constraint FK_Association_6 foreign key (Id_ville)
      references Localisation (Id_ville) on delete restrict on update restrict;

alter table Produit add constraint FK_Association_20 foreign key (Id_type_conservation)
      references Type_conservation (Id_type_conservation) on delete restrict on update restrict;

alter table Produit add constraint FK_Association_30 foreign key (Id_sous_catg_2)
      references Sous_categorie_2 (Id_sous_catg_2) on delete restrict on update restrict;

alter table Sous_categorie add constraint FK_Association_17 foreign key (Id_categorie)
      references Categorie (Id_categorie) on delete restrict on update restrict;

alter table Sous_categorie_2 add constraint FK_Association_29 foreign key (Id_sous_catg)
      references Sous_categorie (Id_sous_catg) on delete restrict on update restrict;

alter table indispo add constraint FK_indispo_don foreign key (Id_don)
      references Don (Id_don) on delete restrict on update restrict;

alter table indispo add constraint FK_indispo_jours foreign key (Id_jours_semaine)
      references Jours_semaine (Id_jours_semaine) on delete restrict on update restrict;

alter table mesurer add constraint FK_mesurer_produit foreign key (Id_produit)
      references Produit (Id_produit) on delete restrict on update restrict;

alter table mesurer add constraint FK_mesurer_unite foreign key (Id_unite_mesure)
      references Unite_mesure (Id_unite_mesure) on delete restrict on update restrict;

