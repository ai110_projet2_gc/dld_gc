﻿using Fr.EQL.AI110.DLD_GC.Entities;

namespace Fr.EQL.AI110.DLD_GC.WebApp.Models
{
    public class DonDetailsViewModel
    {
        public int? SelectedAdherentId { get; set; }
        public List<AdherentDetails>? Adherents { get; set; }
        public DonDetails? DonDetails { get; set; }
    }
}