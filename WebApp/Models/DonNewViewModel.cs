﻿using Fr.EQL.AI110.DLD_GC.Entities;

namespace Fr.EQL.AI110.DLD_GC.WebApp.Models
{
    public class DonNewViewModel 
    {
        public List<Categorie>? Categories { get; set; }
        public List<Categorie2>? Categories2 { get; set; }
        public List<Categorie3>? Categories3 { get; set; }
        public List<DonDetails>? Dons { get; set; }
        
    }
}
