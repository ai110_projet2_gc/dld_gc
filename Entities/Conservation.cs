﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.Entities
{
    public class Conservation
    {
        public int? Id { get; set; }
        public string? Name { get; set; }

        public Conservation()
        {
        }

        public Conservation(int? id, string? name)
        {
            Id = id;
            Name = name;
        }

    }
}
