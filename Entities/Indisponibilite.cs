﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.Entities
{
    public class Indisponibilite
    {
        //Règles de gestion

        //les régions balisées par des # sont des 'directives' (pas compilées)
        #region ATTRIBUTES  
        public int? Id { get; set; }
        public DateTime? IndispStart { get; set; }
        public DateTime? IndispEnd { get; set; }
        public DateTime? IndispRegister { get; set; }
        public DateTime? IndispCancel { get; set; }

        #endregion

        #region CONSTRUCTORS
        public Indisponibilite()
        {
        }

        public Indisponibilite(int? id, DateTime? indispStart, DateTime? indispEnd, 
            DateTime? indispRegister, DateTime? indispCancel)
        {
            Id = id;
            IndispStart = indispStart;
            IndispEnd = indispEnd;
            IndispRegister = indispRegister;
            IndispCancel = indispCancel;
        }


        #endregion
    }
}
