﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.Entities
{
    public class Partenaire
    {
        //Règles de gestion

        //les régions balisées par des # sont des 'directives' (pas compilées)
        #region ATTRIBUTES  
        public int? Id { get; set; }
        public int? LocalisationId { get; set; }
        public string? Name { get; set; }
        public string? Email { get; set; }
        public string? Pw { get; set; }
        public string? Tel { get; set; }
        public int? StreetNb { get; set; }
        public string? StreetName { get; set; }
        public string? CompAdr { get; set; }
        public DateTime? RegisterDate { get; set; }
        public DateTime? ValidateDate { get; set; }
        public DateTime? DeactivateDate { get; set; }
        public int? ActivationCode { get; set; }

        #endregion

        #region CONSTRUCTORS
        public Partenaire()
        {
        }

        public Partenaire(int? id, int? localisationId, string? name, string? email, 
            string? pw, string? tel, int? streetNb, string? streetName, string? compAdr, 
            DateTime? registerDate, DateTime? validateDate, DateTime? deactivateDate, 
            int? activationCode)
        {
            Id = id;
            LocalisationId = localisationId;
            Name = name;
            Email = email;
            Pw = pw;
            Tel = tel;
            StreetNb = streetNb;
            StreetName = streetName;
            CompAdr = compAdr;
            RegisterDate = registerDate;
            ValidateDate = validateDate;
            DeactivateDate = deactivateDate;
            ActivationCode = activationCode;
        }

        #endregion
    }
}
