<?xml version="1.0" encoding="UTF-8"?>
<?PowerDesigner AppLocale="UTF16" ID="{61F5E580-D504-4255-973B-9E27691A29F0}" Label="" LastModificationDate="1639134397" Name="MOO P2 G3" Objects="174" Symbols="52" Target="Analyse" TargetLink="Reference" Type="{18112060-1A4B-11D1-83D9-444553540000}" signature="CLD_OBJECT_MODEL" version="15.1.0.2850"?>
<!-- Veuillez ne pas modifier ce fichier -->

<Model xmlns:a="attribute" xmlns:c="collection" xmlns:o="object">

<o:RootObject Id="o1">
<c:Children>
<o:Model Id="o2">
<a:ObjectID>61F5E580-D504-4255-973B-9E27691A29F0</a:ObjectID>
<a:Name>MOO P2 G3</a:Name>
<a:Code>MOO_P2_G3</a:Code>
<a:CreationDate>1638740123</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639054935</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:PackageOptionsText>[FolderOptions]

[FolderOptions\Class Diagram Objects]
GenerationCheckModel=Yes
GenerationPath=
GenerationOptions=
GenerationTasks=
GenerationTargets=
GenerationSelections=

[FolderOptions\CheckModel]

[FolderOptions\CheckModel\Package]

[FolderOptions\CheckModel\Package\Circular inheritance]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Package\Circular dependency]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Package\ShortcutUniqCode]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Package\ChildShortcut]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe]

[FolderOptions\CheckModel\Classe\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Empty classifier]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Persistent class]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Association Identifier]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Visibility]
CheckSeverity=Yes
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Constructor return type]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Constructor modifier]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Method implementation]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Role name assignment]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Role name unicity]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\BeanInfo]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\JavaBean]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Inheritance on Enum Type (Java)]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\BeanClassDefinition]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\BeanClassBusinessImpl]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\BeanClassHomeImpl]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\BeanClassEjbCreate]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\BeanClassEjbPostCreate]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\BeanClassEjbFind]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\BeanClassEjbHome]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\BeanClassEjbSelect]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\PKClassDefinition]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\PKClassAttributes]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\PKClassExistence]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Mapping]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\MappingSFMap]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\CsfrWrongBound]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\ClssInvalidGenMode]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface]

[FolderOptions\CheckModel\Interface\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\Empty classifier]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\Visibility]
CheckSeverity=Yes
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\Interface constructor]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\Association navigability]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\HomeCreateMethods]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\HomeFindMethods]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\HomeMethods]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\ObjectBusinessMethods]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\CsfrWrongBound]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Attribut]

[FolderOptions\CheckModel\Classe.Attribut\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Attribut\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Attribut\CheckNumParam]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Attribut\Datatype assignment]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Attribut\Extend final class]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Attribut\DomainDivergence]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Identifiant]

[FolderOptions\CheckModel\Classe.Identifiant\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Identifiant\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Identifiant\EmptyColl - ATTR]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Identifiant\CheckIncludeColl - Clss]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Attribut]

[FolderOptions\CheckModel\Interface.Attribut\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Attribut\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Attribut\CheckNumParam]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Attribut\Datatype assignment]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Attribut\Extend final class]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Attribut\DomainDivergence]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Attribut\Event parameter datatype]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Opération]

[FolderOptions\CheckModel\Classe.Opération\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Opération\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Opération\Return type assignment]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Opération\Parameter datatype]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Opération\Abstract operation&#39;s body]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Opération\Abstract operation]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Opération\Operation signature]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Opération\Overriding operation]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Opération\Enum Abstract Methods]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Port]

[FolderOptions\CheckModel\Classe.Port\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Port\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Port\PortIsolated]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe.Partie]

[FolderOptions\CheckModel\Classe.Partie\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Partie\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Partie\PartLink]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe.Partie\PartComposition]
CheckSeverity=Yes
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Connecteur d&#39;assemblage]

[FolderOptions\CheckModel\Classe.Connecteur d&#39;assemblage\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Connecteur d&#39;assemblage\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Connecteur d&#39;assemblage\AscnNull]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe.Connecteur d&#39;assemblage\AscnIntf]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Interface.Opération]

[FolderOptions\CheckModel\Interface.Opération\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Opération\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Opération\Return type assignment]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Opération\Parameter datatype]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Association]

[FolderOptions\CheckModel\Association\Generic links]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Association\Bound links]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Généralisation]

[FolderOptions\CheckModel\Généralisation\Redundant Generalizations]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Généralisation\Multiple inheritance (Java)]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Généralisation\Final datatype with initial value]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Généralisation\Non-Persistent Specifying Attribute]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Généralisation\Generic links]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Généralisation\Bound links]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Réalisation]

[FolderOptions\CheckModel\Réalisation\Redundant Realizations]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Réalisation\Generic links]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Réalisation\Bound links]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Domaine]

[FolderOptions\CheckModel\Domaine\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Domaine\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Domaine\CheckNumParam]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Acteur]

[FolderOptions\CheckModel\Acteur\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Acteur\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Acteur\Single]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Cas d&#39;utilisation]

[FolderOptions\CheckModel\Cas d&#39;utilisation\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Cas d&#39;utilisation\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Cas d&#39;utilisation\Single]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Objet]

[FolderOptions\CheckModel\Objet\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Objet\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Objet\Single]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Lien entre objets]

[FolderOptions\CheckModel\Lien entre objets\Redundant Instance links]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Référence d&#39;interaction]

[FolderOptions\CheckModel\Référence d&#39;interaction\IRefDiagram]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Référence d&#39;interaction\IRefLifelines]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Référence d&#39;interaction\IRefInpMsg]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Référence d&#39;interaction\IRefOutMsg]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Message]

[FolderOptions\CheckModel\Message\MessageNoNumber]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Message\MessageManyLinks]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Message\Actor-Message]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Activité]

[FolderOptions\CheckModel\Activité\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Activité\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Activité\CheckActvTrns]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Activité\CheckNoStart]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Activité\CheckActvReuse]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Noeud d&#39;objet]

[FolderOptions\CheckModel\Noeud d&#39;objet\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Noeud d&#39;objet\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Noeud d&#39;objet\CheckObndDttp]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Décision]

[FolderOptions\CheckModel\Décision\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Décision\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Décision\CheckDcsnCompl]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Synchronisation]

[FolderOptions\CheckModel\Synchronisation\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Synchronisation\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Synchronisation\CheckSyncCompl]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Unité d&#39;organisation]

[FolderOptions\CheckModel\Unité d&#39;organisation\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Unité d&#39;organisation\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Unité d&#39;organisation\CheckPrntOrgnLoop]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Début]

[FolderOptions\CheckModel\Début\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Début\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Début\CheckStrtTrns]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Fin]

[FolderOptions\CheckModel\Fin\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Fin\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Fin\CheckStrtTrns]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Transition]

[FolderOptions\CheckModel\Transition\CheckTrnsSrc]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Transition\CheckTrnsCond]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Transition\TrnsDuplSTAT]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Flux]

[FolderOptions\CheckModel\Flux\CheckFlowSrc]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Flux\CheckFlowNoCond]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Flux\CheckFlowCond]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Flux\FlowDuplOOMACTV]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Evénement]

[FolderOptions\CheckModel\Evénement\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Evénement\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Evénement\EvntUnused]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat]

[FolderOptions\CheckModel\Etat\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat\StatTrns]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat\StatNoStart]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat\ActnOrder]
CheckSeverity=Yes
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Etat.Action]

[FolderOptions\CheckModel\Etat.Action\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat.Action\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat.Action\ActnEvent]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat.Action\ActnDupl]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Point de jonction]

[FolderOptions\CheckModel\Point de jonction\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Point de jonction\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Point de jonction\JnPtCompl]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant]

[FolderOptions\CheckModel\Composant\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant\Single]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant\EJBClassifiers]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Composant\Method Soap Message redefinition]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant.Port]

[FolderOptions\CheckModel\Composant.Port\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant.Port\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant.Port\PortIsolated]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Composant.Connecteur d&#39;assemblage]

[FolderOptions\CheckModel\Composant.Connecteur d&#39;assemblage\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant.Connecteur d&#39;assemblage\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant.Connecteur d&#39;assemblage\AscnNull]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Composant.Connecteur d&#39;assemblage\AscnIntf]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Noeud]

[FolderOptions\CheckModel\Noeud\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Noeud\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Noeud\Empty Node]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Instance de composant]

[FolderOptions\CheckModel\Instance de composant\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Instance de composant\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Instance de composant\Component Instance with null Component]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Instance de composant\Duplicate Component Instance]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Instance de composant\Isolated Component Instance]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Source de données]

[FolderOptions\CheckModel\Source de données\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Source de données\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Source de données\EmptyColl - MODLSRC]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Source de données\Data Source Target Consistency]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Activité.Paramètre d&#39;entrée]

[FolderOptions\CheckModel\Activité.Paramètre d&#39;entrée\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Activité.Paramètre d&#39;entrée\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Activité.Paramètre de sortie]

[FolderOptions\CheckModel\Activité.Paramètre de sortie\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Activité.Paramètre de sortie\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Réplication]

[FolderOptions\CheckModel\Réplication\PartialReplication]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Règle de gestion]

[FolderOptions\CheckModel\Règle de gestion\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Règle de gestion\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Règle de gestion\EmptyColl - OBJCOL]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Objet étendu]

[FolderOptions\CheckModel\Objet étendu\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Objet étendu\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Lien étendu]

[FolderOptions\CheckModel\Lien étendu\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Lien étendu\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Fichier]

[FolderOptions\CheckModel\Fichier\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Fichier\CheckPathExists]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes</a:PackageOptionsText>
<a:ModelOptionsText>[ModelOptions]

[ModelOptions\Cld]
CaseSensitive=No
DisplayName=Yes
EnableTrans=No
EnableRequirements=No
ShowClss=No
DeftAttr=int
DeftMthd=
DeftParm=int
DeftCont=
DomnDttp=Yes
DomnChck=No
DomnRule=No
SupportDelay=No
PreviewEditable=Yes
AutoRealize=No
DttpFullName=Yes
DeftClssAttrVisi=private
VBNetPreprocessingSymbols=
CSharpPreprocessingSymbols=

[ModelOptions\Cld\NamingOptionsTemplates]

[ModelOptions\Cld\ClssNamingOptions]

[ModelOptions\Cld\ClssNamingOptions\CLDPCKG]

[ModelOptions\Cld\ClssNamingOptions\CLDPCKG\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDPCKG\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDDOMN]

[ModelOptions\Cld\ClssNamingOptions\CLDDOMN\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDDOMN\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDCLASS]

[ModelOptions\Cld\ClssNamingOptions\CLDCLASS\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDCLASS\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDINTF]

[ModelOptions\Cld\ClssNamingOptions\CLDINTF\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDINTF\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\UCDACTR]

[ModelOptions\Cld\ClssNamingOptions\UCDACTR\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\UCDACTR\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\UCDUCAS]

[ModelOptions\Cld\ClssNamingOptions\UCDUCAS\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\UCDUCAS\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\SQDOBJT]

[ModelOptions\Cld\ClssNamingOptions\SQDOBJT\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\SQDOBJT\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\SQDMSSG]

[ModelOptions\Cld\ClssNamingOptions\SQDMSSG\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\SQDMSSG\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CPDCOMP]

[ModelOptions\Cld\ClssNamingOptions\CPDCOMP\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CPDCOMP\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDATTR]

[ModelOptions\Cld\ClssNamingOptions\CLDATTR\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDATTR\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDMETHOD]

[ModelOptions\Cld\ClssNamingOptions\CLDMETHOD\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDMETHOD\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDPARM]

[ModelOptions\Cld\ClssNamingOptions\CLDPARM\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDPARM\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMPORT]

[ModelOptions\Cld\ClssNamingOptions\OOMPORT\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMPORT\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMPART]

[ModelOptions\Cld\ClssNamingOptions\OOMPART\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMPART\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDASSC]

[ModelOptions\Cld\ClssNamingOptions\CLDASSC\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDASSC\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\UCDASSC]

[ModelOptions\Cld\ClssNamingOptions\UCDASSC\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\UCDASSC\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\GNRLLINK]

[ModelOptions\Cld\ClssNamingOptions\GNRLLINK\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\GNRLLINK\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\RQLINK]

[ModelOptions\Cld\ClssNamingOptions\RQLINK\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\RQLINK\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\RLZSLINK]

[ModelOptions\Cld\ClssNamingOptions\RLZSLINK\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\RLZSLINK\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DEPDLINK]

[ModelOptions\Cld\ClssNamingOptions\DEPDLINK\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DEPDLINK\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMACTV]

[ModelOptions\Cld\ClssNamingOptions\OOMACTV\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMACTV\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\ACDOBST]

[ModelOptions\Cld\ClssNamingOptions\ACDOBST\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\ACDOBST\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\STAT]

[ModelOptions\Cld\ClssNamingOptions\STAT\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\STAT\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DPDNODE]

[ModelOptions\Cld\ClssNamingOptions\DPDNODE\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DPDNODE\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DPDCMPI]

[ModelOptions\Cld\ClssNamingOptions\DPDCMPI\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DPDCMPI\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DPDASSC]

[ModelOptions\Cld\ClssNamingOptions\DPDASSC\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DPDASSC\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMVAR]

[ModelOptions\Cld\ClssNamingOptions\OOMVAR\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMVAR\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\FILO]

[ModelOptions\Cld\ClssNamingOptions\FILO\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=&quot;\/:*?&lt;&gt;|&quot;
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\FILO\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_. &quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\FRMEOBJ]

[ModelOptions\Cld\ClssNamingOptions\FRMEOBJ\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\FRMEOBJ\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\FRMELNK]

[ModelOptions\Cld\ClssNamingOptions\FRMELNK\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\FRMELNK\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DefaultClass]

[ModelOptions\Cld\ClssNamingOptions\DefaultClass\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DefaultClass\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Generate]

[ModelOptions\Generate\Cdm]
CheckModel=Yes
SaveLinks=Yes
NameToCode=No
Notation=2
PreserveMode=Yes
EnableTransformations=No

[ModelOptions\Generate\Pdm]
CheckModel=Yes
SaveLinks=Yes
ORMapping=No
NameToCode=No
BuildTrgr=No
TablePrefix=
RefrUpdRule=Restrict
RefrDelRule=Restrict
IndxPKName=%TABLE%_PK
IndxAKName=%TABLE%_AK
IndxFKName=%REFR%_FK
IndxThreshold=
ColnFKName=%.3:PARENT%_%COLUMN%
ColnFKNameUse=No
PreserveMode=Yes
EnableTransformations=No

[ModelOptions\Generate\Xsm]
CheckModel=Yes
SaveLinks=Yes
ORMapping=No
NameToCode=No</a:ModelOptionsText>
<c:GeneratedModels>
<o:Shortcut Id="o3">
<a:ObjectID>4166996C-5E65-46D8-9B13-898D9CC93AA0</a:ObjectID>
<a:Name>Diagramme de classes UML_1</a:Name>
<a:Code>DIAGRAMME_DE_CLASSES_UML_1</a:Code>
<a:CreationDate>1638911573</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638911573</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:TargetStereotype/>
<a:TargetID>F1BC6D57-D86F-46E6-97A4-CFFCF8DB61EF</a:TargetID>
<a:TargetClassID>CDE44E21-9669-11D1-9914-006097355D9B</a:TargetClassID>
</o:Shortcut>
<o:Shortcut Id="o4">
<a:ObjectID>926F33B3-47A7-4820-8D79-2F1C2056E1D4</a:ObjectID>
<a:Name>Diagramme de classes UML_1</a:Name>
<a:Code>DIAGRAMME_DE_CLASSES_UML_1</a:Code>
<a:CreationDate>1638911799</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638911799</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:TargetStereotype/>
<a:TargetID>7E520AB3-ACCC-476D-A518-D2CF71A4D4B8</a:TargetID>
<a:TargetClassID>1E597170-9350-11D1-AB3C-0020AF71E433</a:TargetClassID>
</o:Shortcut>
<o:Shortcut Id="o5">
<a:ObjectID>33E09CA3-206B-4DC4-93AD-ED980B26C9E6</a:ObjectID>
<a:Name>Diagramme de classes UML_1</a:Name>
<a:Code>DIAGRAMME_DE_CLASSES_UML_1</a:Code>
<a:CreationDate>1638911919</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638911919</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:TargetStereotype/>
<a:TargetID>81342C3A-A408-47C1-9BAD-E4671554446A</a:TargetID>
<a:TargetClassID>CDE44E21-9669-11D1-9914-006097355D9B</a:TargetClassID>
</o:Shortcut>
<o:Shortcut Id="o6">
<a:ObjectID>9F8992D5-3FD9-4E5D-8EE0-4A7E356920D1</a:ObjectID>
<a:Name>MOO P2 G3</a:Name>
<a:Code>MOO_P2_G3</a:Code>
<a:CreationDate>1638997563</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638997563</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:TargetStereotype/>
<a:TargetID>269A5707-9D0F-4F80-B73A-9922C5BB909B</a:TargetID>
<a:TargetClassID>CDE44E21-9669-11D1-9914-006097355D9B</a:TargetClassID>
</o:Shortcut>
<o:Shortcut Id="o7">
<a:ObjectID>D955F2B6-F867-447F-AE3E-0552C829CB87</a:ObjectID>
<a:Name>MOO P2 G3</a:Name>
<a:Code>MOO_P2_G3</a:Code>
<a:CreationDate>1639008868</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639008868</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:TargetStereotype/>
<a:TargetID>54029FDD-A46C-4D46-BDA9-221F52CB7B38</a:TargetID>
<a:TargetClassID>CDE44E21-9669-11D1-9914-006097355D9B</a:TargetClassID>
</o:Shortcut>
<o:Shortcut Id="o8">
<a:ObjectID>CD33E149-ED98-4D7E-BB48-405E2BCC54E5</a:ObjectID>
<a:Name>MOO AI110 P2 G3</a:Name>
<a:Code>MOO_AI110_P2_G3</a:Code>
<a:CreationDate>1639036706</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1639036706</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:TargetStereotype/>
<a:TargetID>5125C573-749F-4020-AC42-DB75BCF46951</a:TargetID>
<a:TargetClassID>CDE44E21-9669-11D1-9914-006097355D9B</a:TargetClassID>
</o:Shortcut>
<o:Shortcut Id="o9">
<a:ObjectID>82600B7A-4263-4BFE-AE11-9207DC3773E0</a:ObjectID>
<a:Name>MOO P2 G3</a:Name>
<a:Code>MOO_P2_G3</a:Code>
<a:CreationDate>1639128684</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639128684</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:TargetStereotype/>
<a:TargetID>025B6B68-C69E-45EC-8AD0-FC73F33DB3D5</a:TargetID>
<a:TargetClassID>CDE44E21-9669-11D1-9914-006097355D9B</a:TargetClassID>
</o:Shortcut>
</c:GeneratedModels>
<c:ObjectLanguage>
<o:Shortcut Id="o10">
<a:ObjectID>F3A23D74-8FC6-42FD-8583-04DEBAFA633B</a:ObjectID>
<a:Name>Analyse</a:Name>
<a:Code>Analysis</a:Code>
<a:CreationDate>1638740123</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740123</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:TargetStereotype/>
<a:TargetID>87317290-AF03-469F-BC1E-99593F18A4AB</a:TargetID>
<a:TargetClassID>1811206C-1A4B-11D1-83D9-444553540000</a:TargetClassID>
</o:Shortcut>
</c:ObjectLanguage>
<c:ClassDiagrams>
<o:ClassDiagram Id="o11">
<a:ObjectID>0767223B-3C6C-4537-A423-82472A3C5725</a:ObjectID>
<a:Name>DiagrammeClasses_1</a:Name>
<a:Code>DiagrammeClasses_1</a:Code>
<a:CreationDate>1638740123</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639054935</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:DisplayPreferences>[DisplayPreferences]

[DisplayPreferences\CLD]

[DisplayPreferences\General]
Adjust to text=Yes
Snap Grid=No
Constrain Labels=Yes
Display Grid=No
Show Page Delimiter=No
Grid size=800
Graphic unit=2
Window color=255 255 255
Background image=
Background mode=8
Watermark image=
Watermark mode=8
Show watermark on screen=No
Gradient mode=0
Gradient end color=255 255 255
Show Swimlane=No
SwimlaneVert=Yes
TreeVert=No
CompDark=0

[DisplayPreferences\Object]
Mode=0
Trunc Length=80
Word Length=80
Word Text=!&quot;&quot;#$%&amp;&#39;()*+,-./:;&lt;=&gt;?@[\]^_`{|}~
Shortcut IntIcon=Yes
Shortcut IntLoct=Yes
Shortcut IntFullPath=No
Shortcut IntLastPackage=Yes
Shortcut ExtIcon=Yes
Shortcut ExtLoct=No
Shortcut ExtFullPath=No
Shortcut ExtLastPackage=Yes
Shortcut ExtIncludeModl=Yes
EObjShowStrn=Yes
ExtendedObject.Comment=No
ExtendedObject.IconPicture=No
ExtendedObject_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom de l&amp;#39;objet&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
ELnkShowStrn=Yes
ELnkShowName=Yes
ExtendedLink_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
FileObject.Stereotype=No
FileObject.DisplayName=Yes
FileObject.LocationOrName=No
FileObject.IconPicture=No
FileObject.IconMode=Yes
FileObject_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;ExclusiveChoice Name=&quot;Choix exclusif&quot; Mandatory=&quot;Yes&quot; Display=&quot;HorizontalRadios&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Emplacement&quot; Attribute=&quot;LocationOrName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/ExclusiveChoice&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
PckgShowStrn=Yes
Package.Comment=No
Package.IconPicture=No
Package_SymbolLayout=
Display Model Version=Yes
Class.IconPicture=No
Class_SymbolLayout=
Interface.IconPicture=No
Interface_SymbolLayout=
Port.IconPicture=No
Port_SymbolLayout=
ClssShowSttr=Yes
Class.Comment=No
ClssShowCntr=Yes
ClssShowAttr=Yes
ClssAttrTrun=No
ClssAttrMax=3
ClssShowMthd=Yes
ClssMthdTrun=No
ClssMthdMax=3
ClssShowInnr=Yes
IntfShowSttr=Yes
Interface.Comment=No
IntfShowAttr=Yes
IntfAttrTrun=No
IntfAttrMax=3
IntfShowMthd=Yes
IntfMthdTrun=No
IntfMthdMax=3
IntfShowCntr=Yes
IntfShowInnr=Yes
PortShowName=Yes
PortShowType=No
PortShowMult=No
AttrShowVisi=Yes
AttrVisiFmt=1
AttrShowStrn=Yes
AttrShowDttp=Yes
AttrShowDomn=No
AttrShowInit=Yes
MthdShowVisi=Yes
MthdVisiFmt=1
MthdShowStrn=Yes
MthdShowRttp=Yes
MthdShowParm=Yes
AsscShowName=No
AsscShowCntr=Yes
AsscShowRole=Yes
AsscShowOrdr=Yes
AsscShowMult=Yes
AsscMultStr=Yes
AsscShowStrn=No
GnrlShowName=No
GnrlShowStrn=Yes
GnrlShowCntr=Yes
RlzsShowName=No
RlzsShowStrn=Yes
RlzsShowCntr=Yes
DepdShowName=No
DepdShowStrn=Yes
DepdShowCntr=Yes
RqlkShowName=No
RqlkShowStrn=Yes
RqlkShowCntr=Yes

[DisplayPreferences\Symbol]

[DisplayPreferences\Symbol\FRMEOBJ]
STRNFont=Arial,8,N
STRNFont color=0, 0, 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0, 0, 0
LABLFont=Arial,8,N
LABLFont color=0, 0, 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=255 255 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=64
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FRMELNK]
CENTERFont=Arial,8,N
CENTERFont color=0, 0, 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FILO]
OBJSTRNFont=Arial,8,N
OBJSTRNFont color=0, 0, 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0, 0, 0
LCNMFont=Arial,8,N
LCNMFont color=0, 0, 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=2400
Height=2400
Brush color=255 255 255
Fill Color=No
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=-1

[DisplayPreferences\Symbol\CLDPCKG]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 178 178 178
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDCLASS]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
CNTRFont=Arial,8,N
CNTRFont color=0 0 0
AttributesFont=Arial,8,N
AttributesFont color=0 0 0
ClassPrimaryAttributeFont=Arial,8,U
ClassPrimaryAttributeFont color=0 0 0
OperationsFont=Arial,8,N
OperationsFont color=0 0 0
InnerClassifiersFont=Arial,8,N
InnerClassifiersFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=233 202 131
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDINTF]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
CNTRFont=Arial,8,N
CNTRFont color=0 0 0
AttributesFont=Arial,8,N
AttributesFont color=0 0 0
OperationsFont=Arial,8,N
OperationsFont color=0 0 0
InnerClassifiersFont=Arial,8,N
InnerClassifiersFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=205 156 156
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMPORT]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=No
Keep aspect=No
Keep center=No
Keep size=No
Width=825
Height=800
Brush color=250 241 211
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 64 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDASSC]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
MULAFont=Arial,8,N
MULAFont color=0 0 0
Line style=2
Pen=1 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\INNERLINK]
Line style=2
Pen=1 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDACLK]
Line style=2
Pen=2 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\GNRLLINK]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
Line style=2
Pen=1 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\RLZSLINK]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
Line style=2
Pen=3 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\RQLINK]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
Line style=2
Pen=1 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\DEPDLINK]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
Line style=2
Pen=2 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\USRDEPD]
OBJXSTRFont=Arial,8,N
OBJXSTRFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\Free Symbol]
Free TextFont=Arial,8,N
Free TextFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 64 0
Shadow color=192 192 192
Shadow=0</a:DisplayPreferences>
<a:PaperSize>(8268, 11693)</a:PaperSize>
<a:PageMargins>((315,354), (433,354))</a:PageMargins>
<a:PageOrientation>1</a:PageOrientation>
<a:PaperSource>261</a:PaperSource>
<c:Symbols>
<o:AssociationSymbol Id="o12">
<a:CreationDate>1639054827</a:CreationDate>
<a:ModificationDate>1639054916</a:ModificationDate>
<a:Rect>((-98400,23913), (-77550,26261))</a:Rect>
<a:ListOfPoints>((-77550,25087),(-98400,25087))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o13"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o14"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o15"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationClassLinkSymbol Id="o16">
<a:CreationDate>1639054935</a:CreationDate>
<a:ModificationDate>1639055311</a:ModificationDate>
<a:Rect>((-87975,17087), (-87971,25087))</a:Rect>
<a:ListOfPoints>((-87975,25087),(-87971,25087),(-87971,17087))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<c:SourceSymbol>
<o:AssociationSymbol Ref="o12"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o17"/>
</c:DestinationSymbol>
<c:Object>
<o:AssociationClassLink Ref="o18"/>
</c:Object>
</o:AssociationClassLinkSymbol>
<o:TextSymbol Id="o19">
<a:Text>jours indispos</a:Text>
<a:CreationDate>1638975525</a:CreationDate>
<a:ModificationDate>1638975673</a:ModificationDate>
<a:Rect>((-42837,-17778), (-38038,-14179))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:LineColor>0</a:LineColor>
<a:DashStyle>7</a:DashStyle>
<a:FillColor>0</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:TextSymbol>
<o:AssociationSymbol Id="o20">
<a:CreationDate>1638741664</a:CreationDate>
<a:ModificationDate>1638981731</a:ModificationDate>
<a:DestinationTextOffset>(1934, -1622)</a:DestinationTextOffset>
<a:Rect>((-63113,3281), (-56536,15863))</a:Rect>
<a:ListOfPoints>((-56536,3281),(-56536,15863),(-63113,15863))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o21"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o13"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o22"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o23">
<a:CreationDate>1638741668</a:CreationDate>
<a:ModificationDate>1638983584</a:ModificationDate>
<a:Rect>((-73988,1071), (-54451,14938))</a:Rect>
<a:ListOfPoints>((-54451,2245),(-73988,2245),(-73988,14938))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o21"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o13"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o24"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o25">
<a:CreationDate>1638741833</a:CreationDate>
<a:ModificationDate>1638975176</a:ModificationDate>
<a:Rect>((-65480,19237), (-52810,27069))</a:Rect>
<a:ListOfPoints>((-65480,25895),(-58678,25895),(-58678,20411),(-52810,20411))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o13"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o26"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o27"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o28">
<a:CreationDate>1638742137</a:CreationDate>
<a:ModificationDate>1638975176</a:ModificationDate>
<a:Rect>((-46500,20680), (-24984,30562))</a:Rect>
<a:ListOfPoints>((-24984,30562),(-35852,30562),(-35852,20680),(-46500,20680))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o29"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o26"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o30"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o31">
<a:CreationDate>1638742140</a:CreationDate>
<a:ModificationDate>1639055075</a:ModificationDate>
<a:Rect>((-51735,-2826), (-47911,18103))</a:Rect>
<a:ListOfPoints>((-49898,-2826),(-49898,18103))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o21"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o26"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o32"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o33">
<a:CreationDate>1638742143</a:CreationDate>
<a:ModificationDate>1639037055</a:ModificationDate>
<a:SourceTextOffset>(-1022, 37)</a:SourceTextOffset>
<a:Rect>((-47363,12731), (-35944,18501))</a:Rect>
<a:ListOfPoints>((-35944,12731),(-35944,17327),(-47363,17327))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o34"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o26"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o35"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o36">
<a:CreationDate>1638742322</a:CreationDate>
<a:ModificationDate>1638996937</a:ModificationDate>
<a:Rect>((-52957,-17397), (-49133,-4872))</a:Rect>
<a:ListOfPoints>((-50970,-4872),(-50970,-17397))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o21"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o37"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o38"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o39">
<a:CreationDate>1638743009</a:CreationDate>
<a:ModificationDate>1638996947</a:ModificationDate>
<a:Rect>((-70869,-18941), (-50314,-8739))</a:Rect>
<a:ListOfPoints>((-50314,-17768),(-70869,-17768),(-70869,-8739))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o37"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o40"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o41"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o42">
<a:CreationDate>1638743243</a:CreationDate>
<a:ModificationDate>1638997508</a:ModificationDate>
<a:Rect>((-44198,-264), (-12741,2084))</a:Rect>
<a:ListOfPoints>((-44198,910),(-12741,910))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o21"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o43"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o44"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o45">
<a:CreationDate>1638743318</a:CreationDate>
<a:ModificationDate>1638982497</a:ModificationDate>
<a:DestinationTextOffset>(857, 1306)</a:DestinationTextOffset>
<a:Rect>((-35533,12359), (-22868,24634))</a:Rect>
<a:ListOfPoints>((-24892,24634),(-24892,12359),(-35533,12359))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o29"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o34"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o46"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o47">
<a:CreationDate>1638743325</a:CreationDate>
<a:ModificationDate>1638997492</a:ModificationDate>
<a:Rect>((-29260,1556), (-12254,10721))</a:Rect>
<a:ListOfPoints>((-29260,9547),(-12291,9547),(-12291,1556))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o34"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o43"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o48"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o49">
<a:CreationDate>1638808519</a:CreationDate>
<a:ModificationDate>1638983161</a:ModificationDate>
<a:Rect>((-76874,-28773), (-66016,-20345))</a:Rect>
<a:ListOfPoints>((-76874,-20345),(-76874,-27600),(-66016,-27600))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o50"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o51"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o52"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o53">
<a:CreationDate>1638825676</a:CreationDate>
<a:ModificationDate>1638996886</a:ModificationDate>
<a:Rect>((-34002,-30250), (-2902,-473))</a:Rect>
<a:ListOfPoints>((-4776,-473),(-4776,-30250),(-34002,-30250))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o43"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o54"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o55"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o56">
<a:CreationDate>1638825682</a:CreationDate>
<a:ModificationDate>1638996892</a:ModificationDate>
<a:Rect>((-48170,-29157), (-44272,-8584))</a:Rect>
<a:ListOfPoints>((-46146,-8584),(-46146,-29157))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o21"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o54"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o57"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o58">
<a:CreationDate>1638825703</a:CreationDate>
<a:ModificationDate>1638996886</a:ModificationDate>
<a:Rect>((-51483,-31351), (-45041,-19959))</a:Rect>
<a:ListOfPoints>((-51483,-19959),(-51483,-30178),(-45041,-30178))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o37"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o54"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o59"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o60">
<a:CreationDate>1638864109</a:CreationDate>
<a:ModificationDate>1638996861</a:ModificationDate>
<a:SourceTextOffset>(-486, 652)</a:SourceTextOffset>
<a:DestinationTextOffset>(-1125, 305)</a:DestinationTextOffset>
<a:Rect>((-34651,-12562), (-22147,-5207))</a:Rect>
<a:ListOfPoints>((-22147,-6447),(-32514,-6447),(-32514,-12562))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o61"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o62"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o63"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o64">
<a:CreationDate>1638908549</a:CreationDate>
<a:ModificationDate>1638996876</a:ModificationDate>
<a:Rect>((-12556,-12134), (-8732,609))</a:Rect>
<a:ListOfPoints>((-10719,609),(-10719,-12134))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o43"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o65"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o66"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o67">
<a:CreationDate>1638908742</a:CreationDate>
<a:ModificationDate>1638997474</a:ModificationDate>
<a:SourceTextOffset>(115, -644)</a:SourceTextOffset>
<a:DestinationTextOffset>(204, 644)</a:DestinationTextOffset>
<a:Rect>((-25986,-2593), (-7017,-131))</a:Rect>
<a:ListOfPoints>((-7017,-1363),(-25986,-1363))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o43"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o61"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o68"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o69">
<a:CreationDate>1638909822</a:CreationDate>
<a:ModificationDate>1638997486</a:ModificationDate>
<a:Rect>((-5956,54), (-2132,13540))</a:Rect>
<a:ListOfPoints>((-4119,54),(-4119,13540))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o43"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o70"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o71"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o72">
<a:CreationDate>1638910164</a:CreationDate>
<a:ModificationDate>1638997499</a:ModificationDate>
<a:Rect>((-44013,-3982), (-26761,-1634))</a:Rect>
<a:ListOfPoints>((-44013,-2809),(-26761,-2809))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o21"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o61"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o73"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o74">
<a:CreationDate>1638910565</a:CreationDate>
<a:ModificationDate>1638997246</a:ModificationDate>
<a:Rect>((-71396,28219), (-12245,35892))</a:Rect>
<a:ListOfPoints>((-69372,28219),(-69372,35892),(-12245,35892))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o13"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o75"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o76"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o77">
<a:CreationDate>1638910608</a:CreationDate>
<a:ModificationDate>1638997246</a:ModificationDate>
<a:Rect>((-10159,-1001), (-6261,30373))</a:Rect>
<a:ListOfPoints>((-8135,-1001),(-8135,30373))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o43"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o75"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o78"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o79">
<a:CreationDate>1638910899</a:CreationDate>
<a:ModificationDate>1638983161</a:ModificationDate>
<a:Rect>((-67178,-37312), (-63782,-28574))</a:Rect>
<a:ListOfPoints>((-66234,-28574),(-66234,-36139),(-63782,-36139))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o51"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o80"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o81"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o82">
<a:CreationDate>1638910945</a:CreationDate>
<a:ModificationDate>1638996504</a:ModificationDate>
<a:Rect>((-57313,-37004), (-53415,-19911))</a:Rect>
<a:ListOfPoints>((-55289,-19911),(-55289,-37004))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o37"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o80"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o83"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o84">
<a:CreationDate>1638911098</a:CreationDate>
<a:ModificationDate>1639008805</a:ModificationDate>
<a:Rect>((-67428,-9532), (-56102,-7184))</a:Rect>
<a:ListOfPoints>((-56102,-8359),(-67428,-8359))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o21"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o40"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o85"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o86">
<a:CreationDate>1638974971</a:CreationDate>
<a:ModificationDate>1638975176</a:ModificationDate>
<a:Rect>((-56000,21712), (-46101,25612))</a:Rect>
<a:ListOfPoints>((-54163,21787),(-54163,25612),(-46138,25612),(-46138,21712))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o26"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o26"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o87"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationClassLinkSymbol Id="o88">
<a:CreationDate>1638975025</a:CreationDate>
<a:ModificationDate>1638975176</a:ModificationDate>
<a:Rect>((-50413,25612), (-50150,30188))</a:Rect>
<a:ListOfPoints>((-50150,25612),(-50413,25612),(-50413,30188))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<c:SourceSymbol>
<o:AssociationSymbol Ref="o86"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o89"/>
</c:DestinationSymbol>
<c:Object>
<o:AssociationClassLink Ref="o90"/>
</c:Object>
</o:AssociationClassLinkSymbol>
<o:AssociationSymbol Id="o91">
<a:CreationDate>1638975472</a:CreationDate>
<a:ModificationDate>1638982979</a:ModificationDate>
<a:Rect>((-43093,-14752), (-36313,-4874))</a:Rect>
<a:ListOfPoints>((-43056,-4874),(-43056,-13579),(-36313,-13579))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o21"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o62"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o92"/>
</c:Object>
</o:AssociationSymbol>
<o:ClassSymbol Id="o13">
<a:CreationDate>1638740163</a:CreationDate>
<a:ModificationDate>1639055302</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-78195,13301), (-62387,29783))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>14807295</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o93"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o29">
<a:CreationDate>1638740569</a:CreationDate>
<a:ModificationDate>1638997312</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-31460,17708), (-18666,32242))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>13565902</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o94"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o21">
<a:CreationDate>1638740576</a:CreationDate>
<a:ModificationDate>1638997066</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-57108,-9302), (-42382,6206))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>16777156</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o95"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o50">
<a:CreationDate>1638740577</a:CreationDate>
<a:ModificationDate>1638978411</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-81921,-22703), (-72447,-17909))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>12632256</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o96"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o37">
<a:CreationDate>1638740578</a:CreationDate>
<a:ModificationDate>1638976872</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-57016,-20462), (-48392,-15668))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>12632256</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o97"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o26">
<a:CreationDate>1638740586</a:CreationDate>
<a:ModificationDate>1638975176</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-55232,16303), (-44522,22071))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>12632256</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o98"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o43">
<a:CreationDate>1638740587</a:CreationDate>
<a:ModificationDate>1638997304</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-14113,-2087), (-2399,3681))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>13565902</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o99"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o34">
<a:CreationDate>1638740588</a:CreationDate>
<a:ModificationDate>1638997143</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-41967,7320), (-29173,15036))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>13565902</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o100"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o40">
<a:CreationDate>1638742936</a:CreationDate>
<a:ModificationDate>1638975176</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-76558,-11008), (-65539,-6214))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>12632256</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o101"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o61">
<a:CreationDate>1638744585</a:CreationDate>
<a:ModificationDate>1638997153</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-29722,-8466), (-20326,-750))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>13565902</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o102"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o51">
<a:CreationDate>1638808232</a:CreationDate>
<a:ModificationDate>1638983161</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-71251,-30212), (-60619,-25418))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>12632256</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o103"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o54">
<a:CreationDate>1638823998</a:CreationDate>
<a:ModificationDate>1638996886</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-46508,-31744), (-32864,-26950))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>12632256</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o104"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o62">
<a:CreationDate>1638863929</a:CreationDate>
<a:ModificationDate>1638975566</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-37963,-14707), (-26635,-9913))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>12632256</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o105"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o65">
<a:CreationDate>1638908402</a:CreationDate>
<a:ModificationDate>1638982882</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-16199,-15279), (-6031,-8635))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>128</a:LineColor>
<a:FillColor>12632256</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:ManuallyResized>1</a:ManuallyResized>
<c:Object>
<o:Class Ref="o106"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o70">
<a:CreationDate>1638909731</a:CreationDate>
<a:ModificationDate>1638982943</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-7236,11010), (4246,15804))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>12632256</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o107"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o75">
<a:CreationDate>1638910288</a:CreationDate>
<a:ModificationDate>1638997246</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-16813,30323), (-1545,38039))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>12909823</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o108"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o80">
<a:CreationDate>1638910740</a:CreationDate>
<a:ModificationDate>1638978415</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-64141,-38519), (-52582,-33725))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>12632256</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o109"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o89">
<a:CreationDate>1638974985</a:CreationDate>
<a:ModificationDate>1638975176</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-55729,28278), (-45097,32099))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>12632256</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o110"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o14">
<a:CreationDate>1639054555</a:CreationDate>
<a:ModificationDate>1639054916</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-103828,22428), (-93274,29170))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>12632256</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o111"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o17">
<a:CreationDate>1639054935</a:CreationDate>
<a:ModificationDate>1639055311</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-91782,14691), (-84162,19485))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>14807295</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o112"/>
</c:Object>
</o:ClassSymbol>
</c:Symbols>
</o:ClassDiagram>
</c:ClassDiagrams>
<c:DefaultDiagram>
<o:ClassDiagram Ref="o11"/>
</c:DefaultDiagram>
<c:Classes>
<o:Class Id="o93">
<a:ObjectID>051DE203-E2EF-4488-9CB4-0759AE2EF285</a:ObjectID>
<a:Name>Adherent</a:Name>
<a:Code>Adherent</a:Code>
<a:CreationDate>1638740163</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639055017</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o113">
<a:ObjectID>32241662-60CB-4B3D-BD44-5C40311B0AF1</a:ObjectID>
<a:Name>Id_adherent</a:Name>
<a:Code>Id_adherent</a:Code>
<a:CreationDate>1638740240</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740398</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o114">
<a:ObjectID>2B264B88-32DD-411C-8644-12B5CCBE8CF5</a:ObjectID>
<a:Name>Nom</a:Name>
<a:Code>Nom</a:Code>
<a:CreationDate>1638740240</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740545</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o115">
<a:ObjectID>C0C87F93-9F8A-418D-89C8-9CB5E0E5FAF8</a:ObjectID>
<a:Name>Prenom</a:Name>
<a:Code>Prenom</a:Code>
<a:CreationDate>1638740240</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740545</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o116">
<a:ObjectID>16569E37-39AE-453A-A645-90530EC07D2D</a:ObjectID>
<a:Name>Date_de_naissance</a:Name>
<a:Code>Date_de_naissance</a:Code>
<a:CreationDate>1638740240</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740545</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o117">
<a:ObjectID>63D3584E-11D7-4D96-AFF8-654A563DB025</a:ObjectID>
<a:Name>Email</a:Name>
<a:Code>Email</a:Code>
<a:CreationDate>1638740240</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740545</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o118">
<a:ObjectID>786F9CB8-4455-40E0-A368-9A3B5C6432F2</a:ObjectID>
<a:Name>Mot_de_passe</a:Name>
<a:Code>Mot_de_passe</a:Code>
<a:CreationDate>1638740427</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740545</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o119">
<a:ObjectID>C69F77E3-7C46-4F5C-A3AD-877A144F44E1</a:ObjectID>
<a:Name>Numero_voie</a:Name>
<a:Code>Numero_voie</a:Code>
<a:CreationDate>1638740427</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740488</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o120">
<a:ObjectID>C8A1A350-01B1-4649-A0EB-BD4960E35A39</a:ObjectID>
<a:Name>Libelle_voie</a:Name>
<a:Code>Libelle_voie</a:Code>
<a:CreationDate>1638740427</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740545</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o121">
<a:ObjectID>B0E769D6-D04A-4E35-BFFF-270476AB4D0B</a:ObjectID>
<a:Name>Complement_adresse</a:Name>
<a:Code>Complement_adresse</a:Code>
<a:CreationDate>1638740427</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740545</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o122">
<a:ObjectID>F4E0321A-806B-4788-B624-469B1DC6B052</a:ObjectID>
<a:Name>Telephone</a:Name>
<a:Code>Telephone</a:Code>
<a:CreationDate>1638740427</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740545</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o123">
<a:ObjectID>04F38831-0083-463A-A82B-9EEC25F5A226</a:ObjectID>
<a:Name>Date_creation_compte</a:Name>
<a:Code>Date_creation_compte</a:Code>
<a:CreationDate>1638740427</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740545</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o124">
<a:ObjectID>A3D6D639-902B-4417-AC98-56BC4F80FF98</a:ObjectID>
<a:Name>Date_confirmation_inscription</a:Name>
<a:Code>Date_confirmation_inscription</a:Code>
<a:CreationDate>1638740427</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740545</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o125">
<a:ObjectID>1D9237E7-FDA0-420A-BC96-495EBFB3A54A</a:ObjectID>
<a:Name>Date_desactivation</a:Name>
<a:Code>Date_desactivation</a:Code>
<a:CreationDate>1638740427</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740545</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o126">
<a:ObjectID>5CB7CC92-8709-40C9-96CA-C077B24922F9</a:ObjectID>
<a:Name>Code_valid_mail</a:Name>
<a:Code>Code_valid_mail</a:Code>
<a:CreationDate>1638976342</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1638996608</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o127">
<a:ObjectID>6EAB09BA-B4F1-4DBB-9E43-DD2D9F7DAAD5</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638740395</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740398</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o113"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o127"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o94">
<a:ObjectID>BFCA5EE9-BEF6-45EE-B569-7A3D2FE7A83B</a:ObjectID>
<a:Name>Partenaire</a:Name>
<a:Code>Partenaire</a:Code>
<a:CreationDate>1638740569</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638976340</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o128">
<a:ObjectID>8087639E-5293-4430-A2A1-19F45EF1FF65</a:ObjectID>
<a:Name>Id_partenaire</a:Name>
<a:Code>Id_partenaire</a:Code>
<a:CreationDate>1638740709</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740778</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o129">
<a:ObjectID>B4BBE71F-736D-43E5-8222-DB1556F7ACD5</a:ObjectID>
<a:Name>Nom</a:Name>
<a:Code>Nom</a:Code>
<a:CreationDate>1638740709</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740858</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o130">
<a:ObjectID>31AD6E96-A3EF-460B-B6D6-FB14A0DE7E43</a:ObjectID>
<a:Name>Email</a:Name>
<a:Code>Email</a:Code>
<a:CreationDate>1638740709</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740858</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o131">
<a:ObjectID>CB70A8B0-2DBF-4EBD-AF6F-5356323E08A0</a:ObjectID>
<a:Name>Mot_de_passe</a:Name>
<a:Code>Mot_de_passe</a:Code>
<a:CreationDate>1638740709</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740858</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o132">
<a:ObjectID>173478AE-0A97-4B42-8C80-A1E0279A9CCD</a:ObjectID>
<a:Name>Telephone</a:Name>
<a:Code>Telephone</a:Code>
<a:CreationDate>1638740709</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740858</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o133">
<a:ObjectID>203B3FA5-50FA-4B5A-8F16-CC67E893EC7F</a:ObjectID>
<a:Name>Numero_voie</a:Name>
<a:Code>Numero_voie</a:Code>
<a:CreationDate>1638740709</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740762</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o134">
<a:ObjectID>C02A2B1C-6B5F-42A4-8A1A-92809852BF15</a:ObjectID>
<a:Name>Libelle_voie</a:Name>
<a:Code>Libelle_voie</a:Code>
<a:CreationDate>1638740709</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740858</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o135">
<a:ObjectID>2DF6BDCA-B6A0-411C-941C-D5A5FEFBD4D8</a:ObjectID>
<a:Name>Complement_adresse</a:Name>
<a:Code>Complement_adresse</a:Code>
<a:CreationDate>1638740709</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740858</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o136">
<a:ObjectID>A3E62F21-8B20-453A-9BB1-42024DF8F331</a:ObjectID>
<a:Name>Date_enregistrement</a:Name>
<a:Code>Date_enregistrement</a:Code>
<a:CreationDate>1638740709</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740858</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o137">
<a:ObjectID>431D2A9E-0F9B-4846-8A4D-6B394F9A9450</a:ObjectID>
<a:Name>Date_validation</a:Name>
<a:Code>Date_validation</a:Code>
<a:CreationDate>1638740709</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740858</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o138">
<a:ObjectID>F1CE01F2-715F-4DA0-8BDB-CA0EBAB63088</a:ObjectID>
<a:Name>Date_desactivation</a:Name>
<a:Code>Date_desactivation</a:Code>
<a:CreationDate>1638740709</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740858</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o139">
<a:ObjectID>099EDFF5-B3B1-4D4D-AB6F-2A0186BA9116</a:ObjectID>
<a:Name>Code_valid_mail</a:Name>
<a:Code>Code_valid_mail</a:Code>
<a:CreationDate>1638976328</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1638996663</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o140">
<a:ObjectID>38CCCB57-1D5E-4502-BCE2-8C9C70F12955</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638740767</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740778</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o128"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o140"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o95">
<a:ObjectID>EA45CA83-8DF3-4C3D-948E-A7F99BF73464</a:ObjectID>
<a:Name>Don</a:Name>
<a:Code>Don</a:Code>
<a:CreationDate>1638740576</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639054470</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o141">
<a:ObjectID>A232F769-24B4-4CE9-9EB6-3C8FE7846C93</a:ObjectID>
<a:Name>Id_don</a:Name>
<a:Code>Id_don</a:Code>
<a:CreationDate>1638740868</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740929</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o142">
<a:ObjectID>FB8C3BFC-D398-4150-8664-F603FB6875B7</a:ObjectID>
<a:Name>Quantite_produit</a:Name>
<a:Code>Quantite_produit</a:Code>
<a:CreationDate>1638740868</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638909214</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>float</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o143">
<a:ObjectID>8878778A-FC99-4708-9B42-7ACD64B4455F</a:ObjectID>
<a:Name>DLC</a:Name>
<a:Code>DLC</a:Code>
<a:CreationDate>1638740868</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740995</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o144">
<a:ObjectID>7748BDC2-889C-4786-A710-93BBE7AA5DBB</a:ObjectID>
<a:Name>DDM</a:Name>
<a:Code>DDM</a:Code>
<a:CreationDate>1638740868</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740995</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o145">
<a:ObjectID>7FABA44C-1A27-4FEB-B3FF-0E2D8171CADA</a:ObjectID>
<a:Name>Date_proposition</a:Name>
<a:Code>Date_proposition</a:Code>
<a:CreationDate>1638740868</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740995</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o146">
<a:ObjectID>666013B2-24C7-407F-B5FC-DB1917F221F7</a:ObjectID>
<a:Name>Date_reservation</a:Name>
<a:Code>Date_reservation</a:Code>
<a:CreationDate>1638740868</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740995</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o147">
<a:ObjectID>3016CD27-BD0B-42CD-9A8F-64502A61FBFC</a:ObjectID>
<a:Name>Date_annulation</a:Name>
<a:Code>Date_annulation</a:Code>
<a:CreationDate>1638976791</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1638996966</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o148">
<a:ObjectID>9C295FFB-FC86-4775-BE07-42CA81332725</a:ObjectID>
<a:Name>Photo</a:Name>
<a:Code>Photo</a:Code>
<a:CreationDate>1638740868</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740995</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o149">
<a:ObjectID>EE295C6A-7E0C-4DED-B927-C35056FD7743</a:ObjectID>
<a:Name>Numero_voie_alt</a:Name>
<a:Code>Numero_voie_alt</a:Code>
<a:CreationDate>1639035855</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1639054470</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o150">
<a:ObjectID>DE2D0AB3-FDDD-453B-BEC4-65F434B331F5</a:ObjectID>
<a:Name>Libelle_voie_alt</a:Name>
<a:Code>Libelle_voie_alt</a:Code>
<a:CreationDate>1639008736</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639054470</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o151">
<a:ObjectID>077E41FD-A5F4-4118-B9E4-ED447353A817</a:ObjectID>
<a:Name>Complement_adresse_alt</a:Name>
<a:Code>Complement_adresse_alt</a:Code>
<a:CreationDate>1639054394</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639054470</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o152">
<a:ObjectID>B9E04C13-5C5A-4DD8-AA2F-B1312D766DFD</a:ObjectID>
<a:Name>Date_disponibilite_don</a:Name>
<a:Code>Date_disponibilite_don</a:Code>
<a:CreationDate>1638997337</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639037256</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o153">
<a:ObjectID>AD83AE7B-4732-43E9-BB34-C029ED0454A3</a:ObjectID>
<a:Name>Date_finalisation_echange</a:Name>
<a:Code>Date_finalisation_echange</a:Code>
<a:CreationDate>1638740868</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740995</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o154">
<a:ObjectID>6FD9C532-EFE8-42F1-AA8A-EDDDCDC6936F</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638740926</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740929</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o141"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o154"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o96">
<a:ObjectID>BA4A362B-2275-4BF7-99F9-06B691D9B8BE</a:ObjectID>
<a:Name>Categorie</a:Name>
<a:Code>Categorie</a:Code>
<a:CreationDate>1638740577</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638910870</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o155">
<a:ObjectID>4926F696-2CFB-4EAE-8287-D7937274B59E</a:ObjectID>
<a:Name>Id_categorie</a:Name>
<a:Code>Id_categorie</a:Code>
<a:CreationDate>1638741134</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638741197</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o156">
<a:ObjectID>942C12EE-27AF-4C94-B3D8-E091C428A2BD</a:ObjectID>
<a:Name>Nom_catg</a:Name>
<a:Code>Nom_catg</a:Code>
<a:CreationDate>1638795491</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1638808440</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o157">
<a:ObjectID>E9FCF145-6BCC-449E-9B76-ABA713FD8C77</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638741192</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638741197</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o155"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o157"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o97">
<a:ObjectID>F9EF8A6B-ADDB-4736-BAFE-E1ABF9B6FDCE</a:ObjectID>
<a:Name>Produit</a:Name>
<a:Code>Produit</a:Code>
<a:CreationDate>1638740578</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639036774</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o158">
<a:ObjectID>2FF43FA2-929C-4CC8-963D-AC63C356EA53</a:ObjectID>
<a:Name>Id_produit</a:Name>
<a:Code>Id_produit</a:Code>
<a:CreationDate>1638741028</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638741053</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o159">
<a:ObjectID>53B35A33-6889-44B7-8317-35E389302EC8</a:ObjectID>
<a:Name>Nom</a:Name>
<a:Code>Nom</a:Code>
<a:CreationDate>1638741028</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638741046</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o160">
<a:ObjectID>F8512356-8CEF-44C6-8283-3F4BCE9EF66C</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638741049</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638741053</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o158"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o160"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o98">
<a:ObjectID>F80C9ACE-3B69-4F28-91A4-C5CC74FB4D1F</a:ObjectID>
<a:Name>Localisation</a:Name>
<a:Code>Localisation</a:Code>
<a:CreationDate>1638740586</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638974955</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o161">
<a:ObjectID>45DF0AB8-A478-4050-A87C-88919B3A5A23</a:ObjectID>
<a:Name>Id ville</a:Name>
<a:Code>Id_ville</a:Code>
<a:CreationDate>1638974886</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1638974941</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o162">
<a:ObjectID>13129F51-731F-4617-8EC4-8427ADF02A7B</a:ObjectID>
<a:Name>Ville_commune</a:Name>
<a:Code>Ville_commune</a:Code>
<a:CreationDate>1638740621</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638974899</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o163">
<a:ObjectID>3E413A67-BF0D-4966-A4C3-C670C2B8F6AF</a:ObjectID>
<a:Name>Code_postal</a:Name>
<a:Code>Code_postal</a:Code>
<a:CreationDate>1638740621</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638974906</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o164">
<a:ObjectID>A2D1872D-64F1-42EE-AEF4-ECE54B0C9C82</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638742015</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638974941</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o161"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o164"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o99">
<a:ObjectID>DAC77B44-AF51-4CD6-87D5-CBCFD1E6AD45</a:ObjectID>
<a:Name>Espace_stockage</a:Name>
<a:Code>Espace_stockage</a:Code>
<a:CreationDate>1638740587</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638997452</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o165">
<a:ObjectID>E413D056-8E25-4EF8-ABE7-1EF1233E5E40</a:ObjectID>
<a:Name>Id_stockage</a:Name>
<a:Code>Id_stockage</a:Code>
<a:CreationDate>1638741368</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638741435</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o166">
<a:ObjectID>84A11848-1432-4F42-845D-76C62146C11B</a:ObjectID>
<a:Name>Date_ajout_espace</a:Name>
<a:Code>Date_ajout_espace</a:Code>
<a:CreationDate>1638976415</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1638996754</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o167">
<a:ObjectID>375900DB-CF2A-4D78-898B-80DC924E4029</a:ObjectID>
<a:Name>Date_retrait_espace</a:Name>
<a:Code>Date_retrait_espace</a:Code>
<a:CreationDate>1638976415</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1638996575</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o168">
<a:ObjectID>263F68D9-DC7D-40A3-BFD3-16A3C73EA921</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638741432</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638741435</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o165"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o168"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o100">
<a:ObjectID>9B337BBA-49B7-4290-BBE7-4FDC8FFB3870</a:ObjectID>
<a:Name>Point_relais</a:Name>
<a:Code>Point_relais</a:Code>
<a:CreationDate>1638740588</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638975582</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o169">
<a:ObjectID>2ED8C839-D27B-4CC7-A70D-0FF3830BD390</a:ObjectID>
<a:Name>Id_relais</a:Name>
<a:Code>Id_relais</a:Code>
<a:CreationDate>1638741314</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638741349</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o170">
<a:ObjectID>AC36B39C-2121-4D0B-965F-EEFEB58A902B</a:ObjectID>
<a:Name>Nom</a:Name>
<a:Code>Nom</a:Code>
<a:CreationDate>1638741314</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638741459</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o171">
<a:ObjectID>31894487-74AC-440E-8D99-4BE00CACF27C</a:ObjectID>
<a:Name>Numero_voie</a:Name>
<a:Code>Numero_voie</a:Code>
<a:CreationDate>1638741314</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638741345</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o172">
<a:ObjectID>D30CEDE3-1308-40AB-B591-945AF4DA0784</a:ObjectID>
<a:Name>Libelle_voie</a:Name>
<a:Code>Libelle_voie</a:Code>
<a:CreationDate>1638741314</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638741459</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o173">
<a:ObjectID>1E8AD026-5F63-48AF-8867-F1F150EC0AFD</a:ObjectID>
<a:Name>Complement_adresse</a:Name>
<a:Code>Complement_adresse</a:Code>
<a:CreationDate>1638741314</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638741459</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o174">
<a:ObjectID>53AAF8E6-0B79-4571-A52C-4997C822B2D2</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638741345</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638741349</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o169"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o174"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o101">
<a:ObjectID>23CB78FD-586F-4BE2-8EB0-89B345F91FC7</a:ObjectID>
<a:Name>Unite_mesure</a:Name>
<a:Code>Unite_mesure</a:Code>
<a:CreationDate>1638742936</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639036774</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o175">
<a:ObjectID>A6137118-0707-43C4-9C55-519137641503</a:ObjectID>
<a:Name>Id_unite_mesure</a:Name>
<a:Code>Id_unite_mesure</a:Code>
<a:CreationDate>1638909340</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638909363</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o176">
<a:ObjectID>EDECF33B-6377-4C2C-9496-7B45651279D9</a:ObjectID>
<a:Name>Nom</a:Name>
<a:Code>Nom</a:Code>
<a:CreationDate>1638742960</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638909369</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o177">
<a:ObjectID>D1594FEC-E1ED-4D2E-949D-8D781669B0F3</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638742980</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638909369</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o175"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o177"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o102">
<a:ObjectID>37BCD573-070B-4708-A657-4D165BF7E1C3</a:ObjectID>
<a:Name>Horaires</a:Name>
<a:Code>Horaires</a:Code>
<a:CreationDate>1638744585</a:CreationDate>
<a:Creator>savsv</a:Creator>
<a:ModificationDate>1638910182</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o178">
<a:ObjectID>5842A7B9-0A9F-46BF-9A42-1BAD1B1E6F80</a:ObjectID>
<a:Name>Id_horaires</a:Name>
<a:Code>Id_horaires</a:Code>
<a:CreationDate>1638864031</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1638864094</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o179">
<a:ObjectID>2AED5220-C3FA-4CA9-B6A9-4675915848E7</a:ObjectID>
<a:Name>Debut_matin</a:Name>
<a:Code>Debut_matin</a:Code>
<a:CreationDate>1638744628</a:CreationDate>
<a:Creator>savsv</a:Creator>
<a:ModificationDate>1638827261</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o180">
<a:ObjectID>8FA3578F-4C40-4261-BCB0-8C77F0170F0A</a:ObjectID>
<a:Name>Fin_matin</a:Name>
<a:Code>Fin_matin</a:Code>
<a:CreationDate>1638744628</a:CreationDate>
<a:Creator>savsv</a:Creator>
<a:ModificationDate>1638827261</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o181">
<a:ObjectID>83DCAA04-D1E4-444A-95B8-B2A40A64733C</a:ObjectID>
<a:Name>Debut_aprem</a:Name>
<a:Code>Debut_aprem</a:Code>
<a:CreationDate>1638744628</a:CreationDate>
<a:Creator>savsv</a:Creator>
<a:ModificationDate>1638827261</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o182">
<a:ObjectID>44F20C18-7AE8-45D1-9444-76B2C766DEC4</a:ObjectID>
<a:Name>Fin_apres</a:Name>
<a:Code>Fin_apres</a:Code>
<a:CreationDate>1638744628</a:CreationDate>
<a:Creator>savsv</a:Creator>
<a:ModificationDate>1638827261</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o183">
<a:ObjectID>585D98CE-7D5C-466B-AB44-E0FBE2BEE23A</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638744659</a:CreationDate>
<a:Creator>savsv</a:Creator>
<a:ModificationDate>1638864094</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o178"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o183"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o103">
<a:ObjectID>C8E8307B-7DCE-47FF-A890-65A0F0095D3C</a:ObjectID>
<a:Name>Sous-categorie</a:Name>
<a:Code>Sous_categorie</a:Code>
<a:CreationDate>1638808232</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1638910919</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o184">
<a:ObjectID>734D97F9-154F-435C-85F2-7B0243DDEC4C</a:ObjectID>
<a:Name>Id_sous_catg</a:Name>
<a:Code>Id_sous_catg</a:Code>
<a:CreationDate>1638808237</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1638808415</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o185">
<a:ObjectID>6E675793-7006-4B96-9FA5-480A673D9B72</a:ObjectID>
<a:Name>Nom_sous_catg</a:Name>
<a:Code>Nom_sous_catg</a:Code>
<a:CreationDate>1638808237</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1638808346</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o186">
<a:ObjectID>3AD4AE8A-E8A0-4FA8-9FAC-6C1E7722360F</a:ObjectID>
<a:Name>Identifiant_2</a:Name>
<a:Code>Identifiant_2</a:Code>
<a:CreationDate>1638808408</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1638808413</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o184"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o186"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o104">
<a:ObjectID>326E88CB-227F-4763-AA29-79C761D04EE6</a:ObjectID>
<a:Name>Type_conservation</a:Name>
<a:Code>Type_conservation</a:Code>
<a:CreationDate>1638823998</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638825801</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o187">
<a:ObjectID>26C35706-4ABC-43CA-9882-308EE0505EAD</a:ObjectID>
<a:Name>Id_type_conservation</a:Name>
<a:Code>Id_type_conservation</a:Code>
<a:CreationDate>1638824190</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638824708</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o188">
<a:ObjectID>B99CAF16-50BF-41E3-B660-50FB1615F0B8</a:ObjectID>
<a:Name>Nom_type_conservation</a:Name>
<a:Code>Nom_type_conservation</a:Code>
<a:CreationDate>1638824361</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638824708</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o189">
<a:ObjectID>8F9A6E21-6536-4293-997F-F2143230A0C0</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638824392</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638824395</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o187"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o189"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o105">
<a:ObjectID>D8886003-9606-44B9-89CB-27556D0BECA0</a:ObjectID>
<a:Name>Jours_semaine</a:Name>
<a:Code>Jours_semaine</a:Code>
<a:CreationDate>1638863929</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639036800</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o190">
<a:ObjectID>3FC8F0F4-D970-436B-8038-FCFF59241E47</a:ObjectID>
<a:Name>Id_jours_semaine</a:Name>
<a:Code>Id_jours_semaine</a:Code>
<a:CreationDate>1638908985</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638909154</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o191">
<a:ObjectID>62EAB544-B4B1-464D-A401-BFE197B74E6E</a:ObjectID>
<a:Name>Nom_jour</a:Name>
<a:Code>Nom_jour</a:Code>
<a:CreationDate>1638908628</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638909141</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o192">
<a:ObjectID>33F31BF8-0122-4AD6-A420-E1CAC4CD695D</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638909151</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638909154</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o190"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o192"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o106">
<a:ObjectID>771DEE7D-906F-41E9-A99D-0154DA94D9C8</a:ObjectID>
<a:Name>Dimensions</a:Name>
<a:Code>Dimensions</a:Code>
<a:CreationDate>1638908402</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638975661</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o193">
<a:ObjectID>A142D9E7-0416-401A-8CCA-799EE11B183A</a:ObjectID>
<a:Name>Id_dimensions</a:Name>
<a:Code>Id_dimensions</a:Code>
<a:CreationDate>1638908411</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638908543</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o194">
<a:ObjectID>1BE2D152-1E9A-417F-A129-6BED426F8EFD</a:ObjectID>
<a:Name>Nom</a:Name>
<a:Code>Nom</a:Code>
<a:CreationDate>1638908411</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638975661</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o195">
<a:ObjectID>BF41AECE-A00E-4730-9FA5-F37D333CEFAC</a:ObjectID>
<a:Name>Hauteur</a:Name>
<a:Code>Hauteur</a:Code>
<a:CreationDate>1638908411</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638975661</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o196">
<a:ObjectID>08D24349-EE23-4BCD-9B26-EE20F578FD01</a:ObjectID>
<a:Name>Largeur</a:Name>
<a:Code>Largeur</a:Code>
<a:CreationDate>1638908411</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638975661</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o197">
<a:ObjectID>D027A082-1E25-41EE-9435-ECB67CA46E3A</a:ObjectID>
<a:Name>Profondeur</a:Name>
<a:Code>Profondeur</a:Code>
<a:CreationDate>1638975631</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1638975661</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o198">
<a:ObjectID>FEB47E9D-EE88-442F-8272-9F2190A805E3</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638908540</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638908543</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o193"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o198"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o107">
<a:ObjectID>AC425B14-24B2-46AF-8F41-49A02DFEB3D3</a:ObjectID>
<a:Name>Accessibilite</a:Name>
<a:Code>Accessibilite</a:Code>
<a:CreationDate>1638909731</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639128625</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o199">
<a:ObjectID>BB97BED1-8D40-4804-B654-675A6429101B</a:ObjectID>
<a:Name>Id_accessibilite</a:Name>
<a:Code>Id_accessibilite</a:Code>
<a:CreationDate>1638909743</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638909769</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o200">
<a:ObjectID>FA8BB504-674E-45CE-BEDD-77298918E978</a:ObjectID>
<a:Name>Nom_accessibilite</a:Name>
<a:Code>Nom_accessibilite</a:Code>
<a:CreationDate>1638909743</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638996724</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o201">
<a:ObjectID>871BCECF-EBA7-4775-854D-E3273AA8E820</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638909766</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638909769</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o199"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o201"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o108">
<a:ObjectID>1D371398-9F85-4C59-AE7F-BA3EF8C9B7B9</a:ObjectID>
<a:Name>Periode_indisponibilite</a:Name>
<a:Code>Periode_indisponibilite</a:Code>
<a:CreationDate>1638910288</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638977097</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o202">
<a:ObjectID>4D1B719D-1A83-40AA-A753-5173D6527F99</a:ObjectID>
<a:Name>Id_indisponibilite</a:Name>
<a:Code>Id_indisponibilite</a:Code>
<a:CreationDate>1638910319</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638910549</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o203">
<a:ObjectID>B6E707BF-88F0-41D4-930E-8F5C8D8899C1</a:ObjectID>
<a:Name>Debut_periode</a:Name>
<a:Code>Debut_periode</a:Code>
<a:CreationDate>1638910319</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638910546</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o204">
<a:ObjectID>366F293F-190D-45B1-B43D-CE8DC44F9E63</a:ObjectID>
<a:Name>Fin_periode</a:Name>
<a:Code>Fin_periode</a:Code>
<a:CreationDate>1638910319</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638910546</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o205">
<a:ObjectID>1E66DDFC-7343-4109-97E7-8F5FEE384C46</a:ObjectID>
<a:Name>Date_enregistrement_periode</a:Name>
<a:Code>Date_enregistrement_periode</a:Code>
<a:CreationDate>1638977065</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1638996712</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o206">
<a:ObjectID>FBEC7582-073A-44E8-9E94-0AECD0852E6E</a:ObjectID>
<a:Name>Date_annulation_periode</a:Name>
<a:Code>Date_annulation_periode</a:Code>
<a:CreationDate>1638977065</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1638996712</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o207">
<a:ObjectID>B0E52339-8973-4DD0-9993-3B0FE4B7E897</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638910546</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638910549</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o202"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o207"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o109">
<a:ObjectID>4EE291AB-D150-4587-8938-85DCB574E533</a:ObjectID>
<a:Name>Sous-categorie-2</a:Name>
<a:Code>Sous_categorie_2</a:Code>
<a:CreationDate>1638910740</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638910981</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o208">
<a:ObjectID>3E7BE4CF-7D2E-4724-90AA-B1BECE5B076A</a:ObjectID>
<a:Name>Id_sous_catg_2</a:Name>
<a:Code>Id_sous_catg_2</a:Code>
<a:CreationDate>1638910759</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638910799</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o209">
<a:ObjectID>E27BC27C-417D-4428-8B19-499495CC230A</a:ObjectID>
<a:Name>Nom_sous_catg_2</a:Name>
<a:Code>Nom_sous_catg_2</a:Code>
<a:CreationDate>1638910777</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638996484</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o210">
<a:ObjectID>7901A955-5433-43D2-B2F9-4FEA167858B5</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638910796</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638910799</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o208"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o210"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o110">
<a:ObjectID>7DCDE51D-5DC7-450D-B149-1AC39C6E3CDF</a:ObjectID>
<a:Name>Distance</a:Name>
<a:Code>Distance</a:Code>
<a:CreationDate>1638974985</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1638996650</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o211">
<a:ObjectID>32FC5D18-163D-43A8-9AAE-00B78523AB55</a:ObjectID>
<a:Name>Distance_moyenne</a:Name>
<a:Code>Distance_moyenne</a:Code>
<a:CreationDate>1638974986</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1638996629</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
</o:Class>
<o:Class Id="o111">
<a:ObjectID>33430FD0-447A-424E-808B-C6B469D93393</a:ObjectID>
<a:Name>Carnet_credits</a:Name>
<a:Code>Carnet_credits</a:Code>
<a:CreationDate>1639054555</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639055017</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o212">
<a:ObjectID>614AC06C-3D46-4F7E-A877-1A9DE7A51888</a:ObjectID>
<a:Name>Id_carnet</a:Name>
<a:Code>Id_carnet</a:Code>
<a:CreationDate>1639054569</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639054809</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o213">
<a:ObjectID>A587C9E0-E0FD-4BE8-BB1F-5CCE0819CB6D</a:ObjectID>
<a:Name>Nom</a:Name>
<a:Code>Nom</a:Code>
<a:CreationDate>1639054728</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639054796</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o214">
<a:ObjectID>156FFF3D-52E9-474E-93F4-73CDB1EC3AF9</a:ObjectID>
<a:Name>Nombre_credits</a:Name>
<a:Code>Nombre_credits</a:Code>
<a:CreationDate>1639054728</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639054796</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o215">
<a:ObjectID>DA8DD187-6393-4411-BD7D-0235C0BA6B05</a:ObjectID>
<a:Name>Prix</a:Name>
<a:Code>Prix</a:Code>
<a:CreationDate>1639054728</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639054803</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:DataType>float</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o216">
<a:ObjectID>1D804748-DC75-444A-B1E6-0481432A713A</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1639054807</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639054809</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o212"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o216"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o112">
<a:ObjectID>9A719461-C4C9-4E3F-9F03-AD47E8D369A7</a:ObjectID>
<a:Name>Achat_credits</a:Name>
<a:Code>Achat_credits</a:Code>
<a:CreationDate>1639054935</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639054996</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o217">
<a:ObjectID>EE4E9739-4BB5-48ED-A597-E1ED5427CAF0</a:ObjectID>
<a:Name>Date</a:Name>
<a:Code>Date</a:Code>
<a:CreationDate>1639054955</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639054996</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o218">
<a:ObjectID>64B22C7A-0D40-4A6A-89D7-A7C46BD9F4A3</a:ObjectID>
<a:Name>Quantite</a:Name>
<a:Code>Quantite</a:Code>
<a:CreationDate>1639054955</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639054996</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
</o:Class>
</c:Classes>
<c:Associations>
<o:Association Id="o22">
<a:ObjectID>4C083C20-D856-451A-AF19-A03D6A4C5128</a:ObjectID>
<a:Name>Beneficiaire</a:Name>
<a:Code>Beneficiaire</a:Code>
<a:CreationDate>1638741664</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638911512</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleBName>Beneficiaire</a:RoleBName>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o93"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o95"/>
</c:Object2>
</o:Association>
<o:Association Id="o24">
<a:ObjectID>8A919B82-21D5-4CC5-AFCA-111CD53F7DC9</a:ObjectID>
<a:Name>Donateur</a:Name>
<a:Code>Donateur</a:Code>
<a:CreationDate>1638741668</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638911491</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleBName>Donateur</a:RoleBName>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o93"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o95"/>
</c:Object2>
</o:Association>
<o:Association Id="o27">
<a:ObjectID>7A90E571-A744-4990-A9BD-7C05257FEE43</a:ObjectID>
<a:Name>Association_3</a:Name>
<a:Code>Association_3</a:Code>
<a:CreationDate>1638741833</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638741962</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o98"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o93"/>
</c:Object2>
</o:Association>
<o:Association Id="o30">
<a:ObjectID>F4ED954D-9699-481D-BF84-6E5BB224EC68</a:ObjectID>
<a:Name>Association_4</a:Name>
<a:Code>Association_4</a:Code>
<a:CreationDate>1638742137</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638742284</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o98"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o94"/>
</c:Object2>
</o:Association>
<o:Association Id="o32">
<a:ObjectID>0490B2CC-29CC-4D8E-B849-C8056EE3D452</a:ObjectID>
<a:Name>Association_5</a:Name>
<a:Code>Association_5</a:Code>
<a:CreationDate>1638742140</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638742196</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o98"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o95"/>
</c:Object2>
</o:Association>
<o:Association Id="o35">
<a:ObjectID>C5208E43-F8E3-48C0-9436-21F444FE07E3</a:ObjectID>
<a:Name>Association_6</a:Name>
<a:Code>Association_6</a:Code>
<a:CreationDate>1638742143</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638975140</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o98"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o100"/>
</c:Object2>
</o:Association>
<o:Association Id="o38">
<a:ObjectID>BECFC468-0EAB-45B3-A057-37CC5F24DBB9</a:ObjectID>
<a:Name>Association_7</a:Name>
<a:Code>Association_7</a:Code>
<a:CreationDate>1638742322</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638742339</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o97"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o95"/>
</c:Object2>
</o:Association>
<o:Association Id="o41">
<a:ObjectID>DAC334C9-1A10-43EE-B323-89A8925D9F4D</a:ObjectID>
<a:Name>mesurer</a:Name>
<a:Code>mesurer</a:Code>
<a:CreationDate>1638743009</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639036774</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..*</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o101"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o97"/>
</c:Object2>
</o:Association>
<o:Association Id="o44">
<a:ObjectID>37D0D6AB-C010-45F6-BBC1-1E39047A3EAD</a:ObjectID>
<a:Name>Association_11</a:Name>
<a:Code>Association_11</a:Code>
<a:CreationDate>1638743243</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638975582</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o99"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o95"/>
</c:Object2>
</o:Association>
<o:Association Id="o46">
<a:ObjectID>595662E2-11DE-46DE-9308-D774392CB34F</a:ObjectID>
<a:Name>Association_12</a:Name>
<a:Code>Association_12</a:Code>
<a:CreationDate>1638743318</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638743398</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>1..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o100"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o94"/>
</c:Object2>
</o:Association>
<o:Association Id="o48">
<a:ObjectID>D61D57F9-BA5A-41E6-AAFE-88CA55D925B4</a:ObjectID>
<a:Name>Association_13</a:Name>
<a:Code>Association_13</a:Code>
<a:CreationDate>1638743325</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638743449</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>1..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o99"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o100"/>
</c:Object2>
</o:Association>
<o:Association Id="o52">
<a:ObjectID>843620A9-7AFD-42A9-A3EC-56D2459CE40B</a:ObjectID>
<a:Name>Association_17</a:Name>
<a:Code>Association_17</a:Code>
<a:CreationDate>1638808519</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1638910870</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>1..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..*</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o103"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o96"/>
</c:Object2>
</o:Association>
<o:Association Id="o55">
<a:ObjectID>0367B4C8-1042-401A-8B7A-500E26B178C2</a:ObjectID>
<a:Name>Association_18</a:Name>
<a:Code>Association_18</a:Code>
<a:CreationDate>1638825676</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638825782</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o104"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o99"/>
</c:Object2>
</o:Association>
<o:Association Id="o57">
<a:ObjectID>882F15CC-A79E-4269-BD09-9F8B823649F4</a:ObjectID>
<a:Name>Association_19</a:Name>
<a:Code>Association_19</a:Code>
<a:CreationDate>1638825682</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638825792</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o104"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o95"/>
</c:Object2>
</o:Association>
<o:Association Id="o59">
<a:ObjectID>5A37824C-77C7-433D-8D5D-34798F708CDD</a:ObjectID>
<a:Name>Association_20</a:Name>
<a:Code>Association_20</a:Code>
<a:CreationDate>1638825703</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638825801</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o104"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o97"/>
</c:Object2>
</o:Association>
<o:Association Id="o63">
<a:ObjectID>737D2748-AF80-4FE9-A8B7-988804ADEE07</a:ObjectID>
<a:Name>Association_21</a:Name>
<a:Code>Association_21</a:Code>
<a:CreationDate>1638864109</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1638975446</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o105"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o102"/>
</c:Object2>
</o:Association>
<o:Association Id="o66">
<a:ObjectID>B2F9FD4F-B4FC-409C-BAA7-E41AB86EB796</a:ObjectID>
<a:Name>Association_23</a:Name>
<a:Code>Association_23</a:Code>
<a:CreationDate>1638908549</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638908583</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>1..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o106"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o99"/>
</c:Object2>
</o:Association>
<o:Association Id="o68">
<a:ObjectID>948271EA-689D-48C8-BD6A-D002B67F1102</a:ObjectID>
<a:Name>Association_24</a:Name>
<a:Code>Association_24</a:Code>
<a:CreationDate>1638908742</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638975102</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:RoleAMultiplicity>1..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o102"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o99"/>
</c:Object2>
</o:Association>
<o:Association Id="o71">
<a:ObjectID>32C2EE09-63FB-40DF-8FCF-EF9B345A65AD</a:ObjectID>
<a:Name>Association_25</a:Name>
<a:Code>Association_25</a:Code>
<a:CreationDate>1638909822</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638909888</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o107"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o99"/>
</c:Object2>
</o:Association>
<o:Association Id="o73">
<a:ObjectID>6F863AC6-7AB5-49AA-ABA1-0034E69A2FAC</a:ObjectID>
<a:Name>Association_26</a:Name>
<a:Code>Association_26</a:Code>
<a:CreationDate>1638910164</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638975096</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:RoleAMultiplicity>1..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o102"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o95"/>
</c:Object2>
</o:Association>
<o:Association Id="o76">
<a:ObjectID>529D007C-3DD0-4397-9169-ED1071B5C7FA</a:ObjectID>
<a:Name>Association_27</a:Name>
<a:Code>Association_27</a:Code>
<a:CreationDate>1638910565</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638911745</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>1..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o108"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o93"/>
</c:Object2>
</o:Association>
<o:Association Id="o78">
<a:ObjectID>1F498080-7411-498F-B81B-EDE7E1354814</a:ObjectID>
<a:Name>Association_28</a:Name>
<a:Code>Association_28</a:Code>
<a:CreationDate>1638910608</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638911752</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>1..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o108"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o99"/>
</c:Object2>
</o:Association>
<o:Association Id="o81">
<a:ObjectID>3F11A031-87C7-4B35-B3A0-666BBDB726D9</a:ObjectID>
<a:Name>Association_29</a:Name>
<a:Code>Association_29</a:Code>
<a:CreationDate>1638910899</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638983221</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:RoleAMultiplicity>1..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..*</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o109"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o103"/>
</c:Object2>
</o:Association>
<o:Association Id="o83">
<a:ObjectID>1463CD02-67E8-41E9-AB9E-627CABE49920</a:ObjectID>
<a:Name>Association_30</a:Name>
<a:Code>Association_30</a:Code>
<a:CreationDate>1638910945</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638910981</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o109"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o97"/>
</c:Object2>
</o:Association>
<o:Association Id="o85">
<a:ObjectID>4ACCDFB1-3431-4CEE-BA87-CB81F65F2332</a:ObjectID>
<a:Name>Association_31</a:Name>
<a:Code>Association_31</a:Code>
<a:CreationDate>1638911098</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638911137</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o101"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o95"/>
</c:Object2>
</o:Association>
<o:Association Id="o87">
<a:ObjectID>4B91835C-4530-4B53-9A50-D9FD44881EA0</a:ObjectID>
<a:Name>Association_32</a:Name>
<a:Code>Association_32</a:Code>
<a:CreationDate>1638974971</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1639134397</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o98"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o98"/>
</c:Object2>
</o:Association>
<o:Association Id="o92">
<a:ObjectID>95F5AC28-A941-49EB-B5E6-E90B93562302</a:ObjectID>
<a:Name>indispo</a:Name>
<a:Code>indispo</a:Code>
<a:CreationDate>1638975472</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1639036800</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o105"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o95"/>
</c:Object2>
</o:Association>
<o:Association Id="o15">
<a:ObjectID>03D54E49-40AA-43A1-96A6-517FFCAD3D67</a:ObjectID>
<a:Name>Association_33</a:Name>
<a:Code>Association_33</a:Code>
<a:CreationDate>1639054827</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639134397</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o111"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o93"/>
</c:Object2>
</o:Association>
</c:Associations>
<c:AssociationClassLinks>
<o:AssociationClassLink Id="o90">
<a:ObjectID>11010681-3962-47A6-AF7C-35A5F35D8055</a:ObjectID>
<a:CreationDate>1638975025</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1638975025</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<c:Object1>
<o:Class Ref="o110"/>
</c:Object1>
<c:Object2>
<o:Association Ref="o87"/>
</c:Object2>
</o:AssociationClassLink>
<o:AssociationClassLink Id="o18">
<a:ObjectID>83B0C0C5-B932-434B-8DBE-22033DB00422</a:ObjectID>
<a:CreationDate>1639054935</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639054935</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<c:Object1>
<o:Class Ref="o112"/>
</c:Object1>
<c:Object2>
<o:Association Ref="o15"/>
</c:Object2>
</o:AssociationClassLink>
</c:AssociationClassLinks>
<c:TargetModels>
<o:TargetModel Id="o219">
<a:ObjectID>72F00AA5-6F18-4A87-B064-B8F16AD79109</a:ObjectID>
<a:Name>Analyse</a:Name>
<a:Code>Analysis</a:Code>
<a:CreationDate>1638740123</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639035814</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:TargetModelURL>file:///%_OBJLANG%/analysis.xol</a:TargetModelURL>
<a:TargetModelID>87317290-AF03-469F-BC1E-99593F18A4AB</a:TargetModelID>
<a:TargetModelClassID>1811206C-1A4B-11D1-83D9-444553540000</a:TargetModelClassID>
<c:SessionShortcuts>
<o:Shortcut Ref="o10"/>
</c:SessionShortcuts>
</o:TargetModel>
<o:TargetModel Id="o220">
<a:ObjectID>AE7D4FAD-633D-4CF0-903C-AC1B3D438B46</a:ObjectID>
<a:Name>Diagramme de classes UML_1</a:Name>
<a:Code>DIAGRAMME_DE_CLASSES_UML_1</a:Code>
<a:ExtractionID>7340147</a:ExtractionID>
<a:CreationDate>1638911573</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638911634</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:TargetModelURL>file:///C|/Users/Thibault/Desktop/Diagramme de classes UML_1.mpd</a:TargetModelURL>
<a:TargetModelID>F1BC6D57-D86F-46E6-97A4-CFFCF8DB61EF</a:TargetModelID>
<a:TargetModelClassID>CDE44E21-9669-11D1-9914-006097355D9B</a:TargetModelClassID>
<c:SessionShortcuts>
<o:Shortcut Ref="o3"/>
</c:SessionShortcuts>
</o:TargetModel>
<o:TargetModel Id="o221">
<a:ObjectID>C67BAA6E-3373-4714-A34B-21ED23456D9A</a:ObjectID>
<a:Name>Diagramme de classes UML_1</a:Name>
<a:Code>DIAGRAMME_DE_CLASSES_UML_1</a:Code>
<a:ExtractionID>79200</a:ExtractionID>
<a:CreationDate>1638911799</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638911877</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:TargetModelURL>file:///C|/Users/Thibault/Desktop/Diagramme de classes UML_1.mcd</a:TargetModelURL>
<a:TargetModelID>7E520AB3-ACCC-476D-A518-D2CF71A4D4B8</a:TargetModelID>
<a:TargetModelClassID>1E597170-9350-11D1-AB3C-0020AF71E433</a:TargetModelClassID>
<c:SessionShortcuts>
<o:Shortcut Ref="o4"/>
</c:SessionShortcuts>
</o:TargetModel>
<o:TargetModel Id="o222">
<a:ObjectID>B0F624A9-FF95-4087-BD0D-E4D7E7EDDDFD</a:ObjectID>
<a:Name>Diagramme de classes UML_1</a:Name>
<a:Code>DIAGRAMME_DE_CLASSES_UML_1</a:Code>
<a:ExtractionID>17</a:ExtractionID>
<a:CreationDate>1638911919</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638911957</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:TargetModelURL>file:///C|/Users/Thibault/Desktop/MPD.mpd</a:TargetModelURL>
<a:TargetModelID>81342C3A-A408-47C1-9BAD-E4671554446A</a:TargetModelID>
<a:TargetModelClassID>CDE44E21-9669-11D1-9914-006097355D9B</a:TargetModelClassID>
<c:SessionShortcuts>
<o:Shortcut Ref="o5"/>
</c:SessionShortcuts>
</o:TargetModel>
<o:TargetModel Id="o223">
<a:ObjectID>FF8C8569-DEFB-4526-A353-C4A4E6551155</a:ObjectID>
<a:Name>MOO P2 G3</a:Name>
<a:Code>MOO_P2_G3</a:Code>
<a:ExtractionID>3538976</a:ExtractionID>
<a:CreationDate>1638997563</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638997848</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:TargetModelURL>file:///C|/Users/Thibault/Desktop/MOO P2 G3.mpd</a:TargetModelURL>
<a:TargetModelID>269A5707-9D0F-4F80-B73A-9922C5BB909B</a:TargetModelID>
<a:TargetModelClassID>CDE44E21-9669-11D1-9914-006097355D9B</a:TargetModelClassID>
<c:SessionShortcuts>
<o:Shortcut Ref="o6"/>
</c:SessionShortcuts>
</o:TargetModel>
<o:TargetModel Id="o224">
<a:ObjectID>000FE581-13CA-4A7D-BD30-F41D1608860D</a:ObjectID>
<a:Name>MOO P2 G3</a:Name>
<a:Code>MOO_P2_G3</a:Code>
<a:ExtractionID>7340111</a:ExtractionID>
<a:CreationDate>1639008868</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639008886</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:TargetModelURL>file:///C|/Users/Thibault/Desktop/MOO P2 G3.mpd</a:TargetModelURL>
<a:TargetModelID>54029FDD-A46C-4D46-BDA9-221F52CB7B38</a:TargetModelID>
<a:TargetModelClassID>CDE44E21-9669-11D1-9914-006097355D9B</a:TargetModelClassID>
<c:SessionShortcuts>
<o:Shortcut Ref="o7"/>
</c:SessionShortcuts>
</o:TargetModel>
<o:TargetModel Id="o225">
<a:ObjectID>E53CD295-DE8E-49A7-B99F-A23648A05E43</a:ObjectID>
<a:Name>MOO AI110 P2 G3</a:Name>
<a:Code>MOO_AI110_P2_G3</a:Code>
<a:CreationDate>1639036706</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1639037443</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:TargetModelURL>file:///D|/Documents/00-afcepf-EQL/AI 110/G3/MOO AI110 P2 G3.mpd</a:TargetModelURL>
<a:TargetModelID>5125C573-749F-4020-AC42-DB75BCF46951</a:TargetModelID>
<a:TargetModelClassID>CDE44E21-9669-11D1-9914-006097355D9B</a:TargetModelClassID>
<c:SessionShortcuts>
<o:Shortcut Ref="o8"/>
</c:SessionShortcuts>
</o:TargetModel>
<o:TargetModel Id="o226">
<a:ObjectID>04047B8B-A10A-4853-A1CB-E55236FB1354</a:ObjectID>
<a:Name>MOO P2 G3</a:Name>
<a:Code>MOO_P2_G3</a:Code>
<a:CreationDate>1639128684</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639129180</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:TargetModelURL>file:///C|/Users/formation/source/repos/DLD_GC/Modeles/MOO P2 G3.mpd</a:TargetModelURL>
<a:TargetModelID>025B6B68-C69E-45EC-8AD0-FC73F33DB3D5</a:TargetModelID>
<a:TargetModelClassID>CDE44E21-9669-11D1-9914-006097355D9B</a:TargetModelClassID>
<c:SessionShortcuts>
<o:Shortcut Ref="o9"/>
</c:SessionShortcuts>
</o:TargetModel>
</c:TargetModels>
</o:Model>
</c:Children>
</o:RootObject>

</Model>