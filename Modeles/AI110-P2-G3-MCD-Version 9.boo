<?xml version="1.0" encoding="UTF-8"?>
<?PowerDesigner AppLocale="UTF16" ID="{61F5E580-D504-4255-973B-9E27691A29F0}" Label="" LastModificationDate="1639747705" Name="MOO P2 G3" Objects="179" Symbols="49" Target="Analyse" TargetLink="Reference" Type="{18112060-1A4B-11D1-83D9-444553540000}" signature="CLD_OBJECT_MODEL" version="15.1.0.2850"?>
<!-- Veuillez ne pas modifier ce fichier -->

<Model xmlns:a="attribute" xmlns:c="collection" xmlns:o="object">

<o:RootObject Id="o1">
<c:Children>
<o:Model Id="o2">
<a:ObjectID>61F5E580-D504-4255-973B-9E27691A29F0</a:ObjectID>
<a:Name>MOO P2 G3</a:Name>
<a:Code>MOO_P2_G3</a:Code>
<a:CreationDate>1638740123</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639746606</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:PackageOptionsText>[FolderOptions]

[FolderOptions\Class Diagram Objects]
GenerationCheckModel=Yes
GenerationPath=
GenerationOptions=
GenerationTasks=
GenerationTargets=
GenerationSelections=

[FolderOptions\CheckModel]

[FolderOptions\CheckModel\Package]

[FolderOptions\CheckModel\Package\Circular inheritance]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Package\Circular dependency]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Package\ShortcutUniqCode]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Package\ChildShortcut]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe]

[FolderOptions\CheckModel\Classe\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Empty classifier]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Persistent class]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Association Identifier]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Visibility]
CheckSeverity=Yes
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Constructor return type]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Constructor modifier]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Method implementation]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Role name assignment]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Role name unicity]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\BeanInfo]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\JavaBean]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Inheritance on Enum Type (Java)]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\BeanClassDefinition]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\BeanClassBusinessImpl]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\BeanClassHomeImpl]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\BeanClassEjbCreate]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\BeanClassEjbPostCreate]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\BeanClassEjbFind]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\BeanClassEjbHome]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\BeanClassEjbSelect]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\PKClassDefinition]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\PKClassAttributes]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\PKClassExistence]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Mapping]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\MappingSFMap]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\CsfrWrongBound]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\ClssInvalidGenMode]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface]

[FolderOptions\CheckModel\Interface\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\Empty classifier]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\Visibility]
CheckSeverity=Yes
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\Interface constructor]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\Association navigability]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\HomeCreateMethods]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\HomeFindMethods]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\HomeMethods]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\ObjectBusinessMethods]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\CsfrWrongBound]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Attribut]

[FolderOptions\CheckModel\Classe.Attribut\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Attribut\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Attribut\CheckNumParam]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Attribut\Datatype assignment]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Attribut\Extend final class]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Attribut\DomainDivergence]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Identifiant]

[FolderOptions\CheckModel\Classe.Identifiant\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Identifiant\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Identifiant\EmptyColl - ATTR]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Identifiant\CheckIncludeColl - Clss]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Attribut]

[FolderOptions\CheckModel\Interface.Attribut\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Attribut\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Attribut\CheckNumParam]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Attribut\Datatype assignment]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Attribut\Extend final class]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Attribut\DomainDivergence]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Attribut\Event parameter datatype]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Opération]

[FolderOptions\CheckModel\Classe.Opération\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Opération\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Opération\Return type assignment]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Opération\Parameter datatype]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Opération\Abstract operation&#39;s body]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Opération\Abstract operation]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Opération\Operation signature]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Opération\Overriding operation]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Opération\Enum Abstract Methods]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Port]

[FolderOptions\CheckModel\Classe.Port\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Port\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Port\PortIsolated]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe.Partie]

[FolderOptions\CheckModel\Classe.Partie\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Partie\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Partie\PartLink]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe.Partie\PartComposition]
CheckSeverity=Yes
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Connecteur d&#39;assemblage]

[FolderOptions\CheckModel\Classe.Connecteur d&#39;assemblage\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Connecteur d&#39;assemblage\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Connecteur d&#39;assemblage\AscnNull]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe.Connecteur d&#39;assemblage\AscnIntf]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Interface.Opération]

[FolderOptions\CheckModel\Interface.Opération\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Opération\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Opération\Return type assignment]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Opération\Parameter datatype]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Association]

[FolderOptions\CheckModel\Association\Generic links]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Association\Bound links]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Généralisation]

[FolderOptions\CheckModel\Généralisation\Redundant Generalizations]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Généralisation\Multiple inheritance (Java)]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Généralisation\Final datatype with initial value]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Généralisation\Non-Persistent Specifying Attribute]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Généralisation\Generic links]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Généralisation\Bound links]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Réalisation]

[FolderOptions\CheckModel\Réalisation\Redundant Realizations]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Réalisation\Generic links]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Réalisation\Bound links]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Domaine]

[FolderOptions\CheckModel\Domaine\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Domaine\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Domaine\CheckNumParam]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Acteur]

[FolderOptions\CheckModel\Acteur\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Acteur\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Acteur\Single]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Cas d&#39;utilisation]

[FolderOptions\CheckModel\Cas d&#39;utilisation\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Cas d&#39;utilisation\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Cas d&#39;utilisation\Single]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Objet]

[FolderOptions\CheckModel\Objet\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Objet\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Objet\Single]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Lien entre objets]

[FolderOptions\CheckModel\Lien entre objets\Redundant Instance links]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Référence d&#39;interaction]

[FolderOptions\CheckModel\Référence d&#39;interaction\IRefDiagram]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Référence d&#39;interaction\IRefLifelines]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Référence d&#39;interaction\IRefInpMsg]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Référence d&#39;interaction\IRefOutMsg]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Message]

[FolderOptions\CheckModel\Message\MessageNoNumber]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Message\MessageManyLinks]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Message\Actor-Message]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Activité]

[FolderOptions\CheckModel\Activité\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Activité\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Activité\CheckActvTrns]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Activité\CheckNoStart]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Activité\CheckActvReuse]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Noeud d&#39;objet]

[FolderOptions\CheckModel\Noeud d&#39;objet\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Noeud d&#39;objet\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Noeud d&#39;objet\CheckObndDttp]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Décision]

[FolderOptions\CheckModel\Décision\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Décision\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Décision\CheckDcsnCompl]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Synchronisation]

[FolderOptions\CheckModel\Synchronisation\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Synchronisation\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Synchronisation\CheckSyncCompl]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Unité d&#39;organisation]

[FolderOptions\CheckModel\Unité d&#39;organisation\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Unité d&#39;organisation\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Unité d&#39;organisation\CheckPrntOrgnLoop]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Début]

[FolderOptions\CheckModel\Début\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Début\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Début\CheckStrtTrns]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Fin]

[FolderOptions\CheckModel\Fin\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Fin\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Fin\CheckStrtTrns]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Transition]

[FolderOptions\CheckModel\Transition\CheckTrnsSrc]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Transition\CheckTrnsCond]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Transition\TrnsDuplSTAT]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Flux]

[FolderOptions\CheckModel\Flux\CheckFlowSrc]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Flux\CheckFlowNoCond]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Flux\CheckFlowCond]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Flux\FlowDuplOOMACTV]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Evénement]

[FolderOptions\CheckModel\Evénement\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Evénement\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Evénement\EvntUnused]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat]

[FolderOptions\CheckModel\Etat\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat\StatTrns]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat\StatNoStart]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat\ActnOrder]
CheckSeverity=Yes
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Etat.Action]

[FolderOptions\CheckModel\Etat.Action\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat.Action\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat.Action\ActnEvent]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat.Action\ActnDupl]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Point de jonction]

[FolderOptions\CheckModel\Point de jonction\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Point de jonction\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Point de jonction\JnPtCompl]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant]

[FolderOptions\CheckModel\Composant\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant\Single]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant\EJBClassifiers]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Composant\Method Soap Message redefinition]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant.Port]

[FolderOptions\CheckModel\Composant.Port\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant.Port\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant.Port\PortIsolated]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Composant.Connecteur d&#39;assemblage]

[FolderOptions\CheckModel\Composant.Connecteur d&#39;assemblage\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant.Connecteur d&#39;assemblage\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant.Connecteur d&#39;assemblage\AscnNull]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Composant.Connecteur d&#39;assemblage\AscnIntf]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Noeud]

[FolderOptions\CheckModel\Noeud\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Noeud\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Noeud\Empty Node]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Instance de composant]

[FolderOptions\CheckModel\Instance de composant\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Instance de composant\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Instance de composant\Component Instance with null Component]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Instance de composant\Duplicate Component Instance]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Instance de composant\Isolated Component Instance]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Source de données]

[FolderOptions\CheckModel\Source de données\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Source de données\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Source de données\EmptyColl - MODLSRC]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Source de données\Data Source Target Consistency]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Activité.Paramètre d&#39;entrée]

[FolderOptions\CheckModel\Activité.Paramètre d&#39;entrée\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Activité.Paramètre d&#39;entrée\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Activité.Paramètre de sortie]

[FolderOptions\CheckModel\Activité.Paramètre de sortie\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Activité.Paramètre de sortie\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Réplication]

[FolderOptions\CheckModel\Réplication\PartialReplication]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Règle de gestion]

[FolderOptions\CheckModel\Règle de gestion\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Règle de gestion\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Règle de gestion\EmptyColl - OBJCOL]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Objet étendu]

[FolderOptions\CheckModel\Objet étendu\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Objet étendu\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Lien étendu]

[FolderOptions\CheckModel\Lien étendu\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Lien étendu\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Fichier]

[FolderOptions\CheckModel\Fichier\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Fichier\CheckPathExists]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes</a:PackageOptionsText>
<a:ModelOptionsText>[ModelOptions]

[ModelOptions\Cld]
CaseSensitive=No
DisplayName=Yes
EnableTrans=No
EnableRequirements=No
ShowClss=No
DeftAttr=int
DeftMthd=
DeftParm=int
DeftCont=
DomnDttp=Yes
DomnChck=No
DomnRule=No
SupportDelay=No
PreviewEditable=Yes
AutoRealize=No
DttpFullName=Yes
DeftClssAttrVisi=private
VBNetPreprocessingSymbols=
CSharpPreprocessingSymbols=

[ModelOptions\Cld\NamingOptionsTemplates]

[ModelOptions\Cld\ClssNamingOptions]

[ModelOptions\Cld\ClssNamingOptions\CLDPCKG]

[ModelOptions\Cld\ClssNamingOptions\CLDPCKG\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDPCKG\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDDOMN]

[ModelOptions\Cld\ClssNamingOptions\CLDDOMN\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDDOMN\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDCLASS]

[ModelOptions\Cld\ClssNamingOptions\CLDCLASS\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDCLASS\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDINTF]

[ModelOptions\Cld\ClssNamingOptions\CLDINTF\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDINTF\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\UCDACTR]

[ModelOptions\Cld\ClssNamingOptions\UCDACTR\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\UCDACTR\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\UCDUCAS]

[ModelOptions\Cld\ClssNamingOptions\UCDUCAS\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\UCDUCAS\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\SQDOBJT]

[ModelOptions\Cld\ClssNamingOptions\SQDOBJT\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\SQDOBJT\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\SQDMSSG]

[ModelOptions\Cld\ClssNamingOptions\SQDMSSG\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\SQDMSSG\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CPDCOMP]

[ModelOptions\Cld\ClssNamingOptions\CPDCOMP\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CPDCOMP\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDATTR]

[ModelOptions\Cld\ClssNamingOptions\CLDATTR\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDATTR\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDMETHOD]

[ModelOptions\Cld\ClssNamingOptions\CLDMETHOD\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDMETHOD\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDPARM]

[ModelOptions\Cld\ClssNamingOptions\CLDPARM\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDPARM\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMPORT]

[ModelOptions\Cld\ClssNamingOptions\OOMPORT\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMPORT\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMPART]

[ModelOptions\Cld\ClssNamingOptions\OOMPART\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMPART\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDASSC]

[ModelOptions\Cld\ClssNamingOptions\CLDASSC\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDASSC\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\UCDASSC]

[ModelOptions\Cld\ClssNamingOptions\UCDASSC\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\UCDASSC\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\GNRLLINK]

[ModelOptions\Cld\ClssNamingOptions\GNRLLINK\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\GNRLLINK\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\RQLINK]

[ModelOptions\Cld\ClssNamingOptions\RQLINK\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\RQLINK\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\RLZSLINK]

[ModelOptions\Cld\ClssNamingOptions\RLZSLINK\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\RLZSLINK\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DEPDLINK]

[ModelOptions\Cld\ClssNamingOptions\DEPDLINK\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DEPDLINK\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMACTV]

[ModelOptions\Cld\ClssNamingOptions\OOMACTV\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMACTV\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\ACDOBST]

[ModelOptions\Cld\ClssNamingOptions\ACDOBST\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\ACDOBST\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\STAT]

[ModelOptions\Cld\ClssNamingOptions\STAT\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\STAT\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DPDNODE]

[ModelOptions\Cld\ClssNamingOptions\DPDNODE\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DPDNODE\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DPDCMPI]

[ModelOptions\Cld\ClssNamingOptions\DPDCMPI\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DPDCMPI\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DPDASSC]

[ModelOptions\Cld\ClssNamingOptions\DPDASSC\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DPDASSC\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMVAR]

[ModelOptions\Cld\ClssNamingOptions\OOMVAR\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMVAR\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\FILO]

[ModelOptions\Cld\ClssNamingOptions\FILO\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=&quot;\/:*?&lt;&gt;|&quot;
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\FILO\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_. &quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\FRMEOBJ]

[ModelOptions\Cld\ClssNamingOptions\FRMEOBJ\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\FRMEOBJ\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\FRMELNK]

[ModelOptions\Cld\ClssNamingOptions\FRMELNK\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\FRMELNK\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DefaultClass]

[ModelOptions\Cld\ClssNamingOptions\DefaultClass\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DefaultClass\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Generate]

[ModelOptions\Generate\Cdm]
CheckModel=Yes
SaveLinks=Yes
NameToCode=No
Notation=2
PreserveMode=Yes
EnableTransformations=No

[ModelOptions\Generate\Pdm]
CheckModel=Yes
SaveLinks=Yes
ORMapping=No
NameToCode=No
BuildTrgr=No
TablePrefix=
RefrUpdRule=Restrict
RefrDelRule=Restrict
IndxPKName=%TABLE%_PK
IndxAKName=%TABLE%_AK
IndxFKName=%REFR%_FK
IndxThreshold=
ColnFKName=%PROLE%_%COLUMN%
ColnFKNameUse=No
PreserveMode=Yes
EnableTransformations=No

[ModelOptions\Generate\Xsm]
CheckModel=Yes
SaveLinks=Yes
ORMapping=No
NameToCode=No</a:ModelOptionsText>
<c:GeneratedModels>
<o:Shortcut Id="o3">
<a:ObjectID>4166996C-5E65-46D8-9B13-898D9CC93AA0</a:ObjectID>
<a:Name>Diagramme de classes UML_1</a:Name>
<a:Code>DIAGRAMME_DE_CLASSES_UML_1</a:Code>
<a:CreationDate>1638911573</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638911573</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:TargetStereotype/>
<a:TargetID>F1BC6D57-D86F-46E6-97A4-CFFCF8DB61EF</a:TargetID>
<a:TargetClassID>CDE44E21-9669-11D1-9914-006097355D9B</a:TargetClassID>
</o:Shortcut>
<o:Shortcut Id="o4">
<a:ObjectID>926F33B3-47A7-4820-8D79-2F1C2056E1D4</a:ObjectID>
<a:Name>Diagramme de classes UML_1</a:Name>
<a:Code>DIAGRAMME_DE_CLASSES_UML_1</a:Code>
<a:CreationDate>1638911799</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638911799</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:TargetStereotype/>
<a:TargetID>7E520AB3-ACCC-476D-A518-D2CF71A4D4B8</a:TargetID>
<a:TargetClassID>1E597170-9350-11D1-AB3C-0020AF71E433</a:TargetClassID>
</o:Shortcut>
<o:Shortcut Id="o5">
<a:ObjectID>33E09CA3-206B-4DC4-93AD-ED980B26C9E6</a:ObjectID>
<a:Name>Diagramme de classes UML_1</a:Name>
<a:Code>DIAGRAMME_DE_CLASSES_UML_1</a:Code>
<a:CreationDate>1638911919</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638911919</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:TargetStereotype/>
<a:TargetID>81342C3A-A408-47C1-9BAD-E4671554446A</a:TargetID>
<a:TargetClassID>CDE44E21-9669-11D1-9914-006097355D9B</a:TargetClassID>
</o:Shortcut>
<o:Shortcut Id="o6">
<a:ObjectID>9F8992D5-3FD9-4E5D-8EE0-4A7E356920D1</a:ObjectID>
<a:Name>MOO P2 G3</a:Name>
<a:Code>MOO_P2_G3</a:Code>
<a:CreationDate>1638997563</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638997563</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:TargetStereotype/>
<a:TargetID>269A5707-9D0F-4F80-B73A-9922C5BB909B</a:TargetID>
<a:TargetClassID>CDE44E21-9669-11D1-9914-006097355D9B</a:TargetClassID>
</o:Shortcut>
<o:Shortcut Id="o7">
<a:ObjectID>D955F2B6-F867-447F-AE3E-0552C829CB87</a:ObjectID>
<a:Name>MOO P2 G3</a:Name>
<a:Code>MOO_P2_G3</a:Code>
<a:CreationDate>1639008868</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639008868</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:TargetStereotype/>
<a:TargetID>54029FDD-A46C-4D46-BDA9-221F52CB7B38</a:TargetID>
<a:TargetClassID>CDE44E21-9669-11D1-9914-006097355D9B</a:TargetClassID>
</o:Shortcut>
<o:Shortcut Id="o8">
<a:ObjectID>CD33E149-ED98-4D7E-BB48-405E2BCC54E5</a:ObjectID>
<a:Name>MOO AI110 P2 G3</a:Name>
<a:Code>MOO_AI110_P2_G3</a:Code>
<a:CreationDate>1639036706</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1639036706</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:TargetStereotype/>
<a:TargetID>5125C573-749F-4020-AC42-DB75BCF46951</a:TargetID>
<a:TargetClassID>CDE44E21-9669-11D1-9914-006097355D9B</a:TargetClassID>
</o:Shortcut>
<o:Shortcut Id="o9">
<a:ObjectID>82600B7A-4263-4BFE-AE11-9207DC3773E0</a:ObjectID>
<a:Name>MOO P2 G3</a:Name>
<a:Code>MOO_P2_G3</a:Code>
<a:CreationDate>1639128684</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639128684</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:TargetStereotype/>
<a:TargetID>025B6B68-C69E-45EC-8AD0-FC73F33DB3D5</a:TargetID>
<a:TargetClassID>CDE44E21-9669-11D1-9914-006097355D9B</a:TargetClassID>
</o:Shortcut>
</c:GeneratedModels>
<c:ObjectLanguage>
<o:Shortcut Id="o10">
<a:ObjectID>F3A23D74-8FC6-42FD-8583-04DEBAFA633B</a:ObjectID>
<a:Name>Analyse</a:Name>
<a:Code>Analysis</a:Code>
<a:CreationDate>1638740123</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740123</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:TargetStereotype/>
<a:TargetID>87317290-AF03-469F-BC1E-99593F18A4AB</a:TargetID>
<a:TargetClassID>1811206C-1A4B-11D1-83D9-444553540000</a:TargetClassID>
</o:Shortcut>
</c:ObjectLanguage>
<c:ClassDiagrams>
<o:ClassDiagram Id="o11">
<a:ObjectID>0767223B-3C6C-4537-A423-82472A3C5725</a:ObjectID>
<a:Name>DiagrammeClasses_1</a:Name>
<a:Code>DiagrammeClasses_1</a:Code>
<a:CreationDate>1638740123</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639746606</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:DisplayPreferences>[DisplayPreferences]

[DisplayPreferences\CLD]

[DisplayPreferences\General]
Adjust to text=Yes
Snap Grid=No
Constrain Labels=Yes
Display Grid=No
Show Page Delimiter=No
Grid size=800
Graphic unit=2
Window color=255 255 255
Background image=
Background mode=8
Watermark image=
Watermark mode=8
Show watermark on screen=No
Gradient mode=0
Gradient end color=255 255 255
Show Swimlane=No
SwimlaneVert=Yes
TreeVert=No
CompDark=0

[DisplayPreferences\Object]
Mode=0
Trunc Length=80
Word Length=80
Word Text=!&quot;&quot;#$%&amp;&#39;()*+,-./:;&lt;=&gt;?@[\]^_`{|}~
Shortcut IntIcon=Yes
Shortcut IntLoct=Yes
Shortcut IntFullPath=No
Shortcut IntLastPackage=Yes
Shortcut ExtIcon=Yes
Shortcut ExtLoct=No
Shortcut ExtFullPath=No
Shortcut ExtLastPackage=Yes
Shortcut ExtIncludeModl=Yes
EObjShowStrn=Yes
ExtendedObject.Comment=No
ExtendedObject.IconPicture=No
ExtendedObject_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom de l&amp;#39;objet&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
ELnkShowStrn=Yes
ELnkShowName=Yes
ExtendedLink_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
FileObject.Stereotype=No
FileObject.DisplayName=Yes
FileObject.LocationOrName=No
FileObject.IconPicture=No
FileObject.IconMode=Yes
FileObject_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;ExclusiveChoice Name=&quot;Choix exclusif&quot; Mandatory=&quot;Yes&quot; Display=&quot;HorizontalRadios&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Emplacement&quot; Attribute=&quot;LocationOrName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/ExclusiveChoice&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
PckgShowStrn=Yes
Package.Comment=No
Package.IconPicture=No
Package_SymbolLayout=
Display Model Version=Yes
Class.IconPicture=No
Class_SymbolLayout=
Interface.IconPicture=No
Interface_SymbolLayout=
Port.IconPicture=No
Port_SymbolLayout=
ClssShowSttr=Yes
Class.Comment=No
ClssShowCntr=Yes
ClssShowAttr=Yes
ClssAttrTrun=No
ClssAttrMax=3
ClssShowMthd=Yes
ClssMthdTrun=No
ClssMthdMax=3
ClssShowInnr=Yes
IntfShowSttr=Yes
Interface.Comment=No
IntfShowAttr=Yes
IntfAttrTrun=No
IntfAttrMax=3
IntfShowMthd=Yes
IntfMthdTrun=No
IntfMthdMax=3
IntfShowCntr=Yes
IntfShowInnr=Yes
PortShowName=Yes
PortShowType=No
PortShowMult=No
AttrShowVisi=Yes
AttrVisiFmt=1
AttrShowStrn=Yes
AttrShowDttp=Yes
AttrShowDomn=No
AttrShowInit=Yes
MthdShowVisi=Yes
MthdVisiFmt=1
MthdShowStrn=Yes
MthdShowRttp=Yes
MthdShowParm=Yes
AsscShowName=No
AsscShowCntr=Yes
AsscShowRole=Yes
AsscShowOrdr=Yes
AsscShowMult=Yes
AsscMultStr=Yes
AsscShowStrn=No
GnrlShowName=No
GnrlShowStrn=Yes
GnrlShowCntr=Yes
RlzsShowName=No
RlzsShowStrn=Yes
RlzsShowCntr=Yes
DepdShowName=No
DepdShowStrn=Yes
DepdShowCntr=Yes
RqlkShowName=No
RqlkShowStrn=Yes
RqlkShowCntr=Yes

[DisplayPreferences\Symbol]

[DisplayPreferences\Symbol\FRMEOBJ]
STRNFont=Arial,8,N
STRNFont color=0, 0, 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0, 0, 0
LABLFont=Arial,8,N
LABLFont color=0, 0, 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=255 255 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=64
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FRMELNK]
CENTERFont=Arial,8,N
CENTERFont color=0, 0, 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FILO]
OBJSTRNFont=Arial,8,N
OBJSTRNFont color=0, 0, 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0, 0, 0
LCNMFont=Arial,8,N
LCNMFont color=0, 0, 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=2400
Height=2400
Brush color=255 255 255
Fill Color=No
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=-1

[DisplayPreferences\Symbol\CLDPCKG]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 178 178 178
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDCLASS]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
CNTRFont=Arial,8,N
CNTRFont color=0 0 0
AttributesFont=Arial,8,N
AttributesFont color=0 0 0
ClassPrimaryAttributeFont=Arial,8,U
ClassPrimaryAttributeFont color=0 0 0
OperationsFont=Arial,8,N
OperationsFont color=0 0 0
InnerClassifiersFont=Arial,8,N
InnerClassifiersFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=233 202 131
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDINTF]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
CNTRFont=Arial,8,N
CNTRFont color=0 0 0
AttributesFont=Arial,8,N
AttributesFont color=0 0 0
OperationsFont=Arial,8,N
OperationsFont color=0 0 0
InnerClassifiersFont=Arial,8,N
InnerClassifiersFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=205 156 156
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMPORT]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=No
Keep aspect=No
Keep center=No
Keep size=No
Width=825
Height=800
Brush color=250 241 211
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 64 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDASSC]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
MULAFont=Arial,8,N
MULAFont color=0 0 0
Line style=2
Pen=1 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\INNERLINK]
Line style=2
Pen=1 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDACLK]
Line style=2
Pen=2 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\GNRLLINK]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
Line style=2
Pen=1 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\RLZSLINK]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
Line style=2
Pen=3 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\RQLINK]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
Line style=2
Pen=1 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\DEPDLINK]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
Line style=2
Pen=2 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\USRDEPD]
OBJXSTRFont=Arial,8,N
OBJXSTRFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\Free Symbol]
Free TextFont=Arial,8,N
Free TextFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 64 0
Shadow color=192 192 192
Shadow=0</a:DisplayPreferences>
<a:PaperSize>(8268, 11693)</a:PaperSize>
<a:PageMargins>((315,354), (433,354))</a:PageMargins>
<a:PageOrientation>1</a:PageOrientation>
<a:PaperSource>261</a:PaperSource>
<c:Symbols>
<o:AssociationSymbol Id="o12">
<a:CreationDate>1638741664</a:CreationDate>
<a:ModificationDate>1638981731</a:ModificationDate>
<a:DestinationTextOffset>(1934, -1622)</a:DestinationTextOffset>
<a:Rect>((-65393,4493), (-54861,15863))</a:Rect>
<a:ListOfPoints>((-54898,4493),(-54898,15863),(-65393,15863))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o13"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o14"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o15"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o16">
<a:CreationDate>1638741668</a:CreationDate>
<a:ModificationDate>1638983584</a:ModificationDate>
<a:Rect>((-72852,2023), (-53316,14938))</a:Rect>
<a:ListOfPoints>((-53316,3197),(-72814,3197),(-72814,14938))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o13"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o14"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o17"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o18">
<a:CreationDate>1638741833</a:CreationDate>
<a:ModificationDate>1638975176</a:ModificationDate>
<a:Rect>((-67009,19237), (-52112,27069))</a:Rect>
<a:ListOfPoints>((-67009,25895),(-58678,25895),(-58678,20411),(-52112,20411))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o14"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o19"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o20"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o21">
<a:CreationDate>1638742137</a:CreationDate>
<a:ModificationDate>1638975176</a:ModificationDate>
<a:Rect>((-47304,20680), (-24997,30562))</a:Rect>
<a:ListOfPoints>((-24997,30562),(-35852,30562),(-35852,20680),(-47304,20680))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o22"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o19"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o23"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o24">
<a:CreationDate>1638742140</a:CreationDate>
<a:ModificationDate>1639055075</a:ModificationDate>
<a:Rect>((-51714,-3146), (-47890,18103))</a:Rect>
<a:ListOfPoints>((-49877,-3146),(-49877,18103))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o13"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o19"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o25"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o26">
<a:CreationDate>1638742143</a:CreationDate>
<a:ModificationDate>1639037055</a:ModificationDate>
<a:SourceTextOffset>(-1022, 37)</a:SourceTextOffset>
<a:Rect>((-47962,12731), (-35845,18501))</a:Rect>
<a:ListOfPoints>((-35845,12731),(-35944,17327),(-47962,17327))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o27"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o19"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o28"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o29">
<a:CreationDate>1638742322</a:CreationDate>
<a:ModificationDate>1638996937</a:ModificationDate>
<a:Rect>((-52972,-17397), (-49148,-5708))</a:Rect>
<a:ListOfPoints>((-50985,-5708),(-50985,-17397))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o13"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o30"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o31"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o32">
<a:CreationDate>1638743009</a:CreationDate>
<a:ModificationDate>1638996947</a:ModificationDate>
<a:Rect>((-70934,-18841), (-50763,-8739))</a:Rect>
<a:ListOfPoints>((-50763,-17768),(-70869,-17768),(-70934,-8739))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o30"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o33"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o34"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o35">
<a:CreationDate>1638743243</a:CreationDate>
<a:ModificationDate>1638997508</a:ModificationDate>
<a:Rect>((-45536,45), (-11972,2393))</a:Rect>
<a:ListOfPoints>((-45536,1219),(-11972,1219))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o13"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o36"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o37"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o38">
<a:CreationDate>1638743318</a:CreationDate>
<a:ModificationDate>1638982497</a:ModificationDate>
<a:DestinationTextOffset>(857, 1306)</a:DestinationTextOffset>
<a:Rect>((-35543,12359), (-22916,24634))</a:Rect>
<a:ListOfPoints>((-24919,24634),(-24892,12359),(-35543,12359))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o22"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o27"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o39"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o40">
<a:CreationDate>1638743325</a:CreationDate>
<a:ModificationDate>1638997492</a:ModificationDate>
<a:Rect>((-30936,1556), (-11599,10721))</a:Rect>
<a:ListOfPoints>((-30936,9547),(-12291,9547),(-11599,1556))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o27"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o36"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o41"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o42">
<a:CreationDate>1638808519</a:CreationDate>
<a:ModificationDate>1638983161</a:ModificationDate>
<a:Rect>((-76955,-28773), (-65988,-20345))</a:Rect>
<a:ListOfPoints>((-76955,-20345),(-76874,-27600),(-65988,-27600))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o43"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o44"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o45"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o46">
<a:CreationDate>1638825676</a:CreationDate>
<a:ModificationDate>1639389246</a:ModificationDate>
<a:Rect>((-39244,-30325), (-3535,-473))</a:Rect>
<a:ListOfPoints>((-5372,-473),(-5372,-30325),(-39244,-30325))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o36"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o47"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o48"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o49">
<a:CreationDate>1638825682</a:CreationDate>
<a:ModificationDate>1639389246</a:ModificationDate>
<a:Rect>((-47463,-29232), (-45177,-10352))</a:Rect>
<a:ListOfPoints>((-47014,-10352),(-47014,-18406),(-45476,-18406),(-45476,-29232))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o13"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o47"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o50"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o51">
<a:CreationDate>1638825703</a:CreationDate>
<a:ModificationDate>1639389246</a:ModificationDate>
<a:Rect>((-51750,-31426), (-44909,-19959))</a:Rect>
<a:ListOfPoints>((-51713,-19959),(-51713,-30253),(-44909,-30253))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o30"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o47"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o52"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o53">
<a:CreationDate>1638864109</a:CreationDate>
<a:ModificationDate>1638996861</a:ModificationDate>
<a:SourceTextOffset>(-486, 652)</a:SourceTextOffset>
<a:DestinationTextOffset>(-1125, 305)</a:DestinationTextOffset>
<a:Rect>((-34620,-12562), (-21934,-5207))</a:Rect>
<a:ListOfPoints>((-21934,-6447),(-32514,-6447),(-32432,-12562))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o54"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o55"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o56"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o57">
<a:CreationDate>1638908549</a:CreationDate>
<a:ModificationDate>1638996876</a:ModificationDate>
<a:Rect>((-12340,-12134), (-8502,609))</a:Rect>
<a:ListOfPoints>((-10297,609),(-10719,-12134))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o36"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o58"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o59"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o60">
<a:CreationDate>1638908742</a:CreationDate>
<a:ModificationDate>1638997474</a:ModificationDate>
<a:SourceTextOffset>(115, -644)</a:SourceTextOffset>
<a:DestinationTextOffset>(204, 644)</a:DestinationTextOffset>
<a:Rect>((-26058,-2593), (-7229,-131))</a:Rect>
<a:ListOfPoints>((-7229,-1363),(-26058,-1363))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o36"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o54"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o61"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o62">
<a:CreationDate>1638909822</a:CreationDate>
<a:ModificationDate>1638997486</a:ModificationDate>
<a:Rect>((-5816,54), (-1834,13540))</a:Rect>
<a:ListOfPoints>((-4828,54),(-3095,13540))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o36"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o63"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o64"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o65">
<a:CreationDate>1638910164</a:CreationDate>
<a:ModificationDate>1638997499</a:ModificationDate>
<a:Rect>((-45395,-4140), (-26890,-1792))</a:Rect>
<a:ListOfPoints>((-45395,-2967),(-26890,-2967))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o13"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o54"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o66"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o67">
<a:CreationDate>1638910565</a:CreationDate>
<a:ModificationDate>1638997246</a:ModificationDate>
<a:Rect>((-71471,28219), (-11019,35892))</a:Rect>
<a:ListOfPoints>((-69664,28219),(-69372,35892),(-11019,35892))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o14"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o68"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o69"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o70">
<a:CreationDate>1638910608</a:CreationDate>
<a:ModificationDate>1638997246</a:ModificationDate>
<a:Rect>((-10216,-1001), (-6698,30373))</a:Rect>
<a:ListOfPoints>((-8156,-1001),(-8552,30373))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o36"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o68"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o71"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o72">
<a:CreationDate>1638910899</a:CreationDate>
<a:ModificationDate>1638983161</a:ModificationDate>
<a:Rect>((-66234,-37312), (-61646,-28574))</a:Rect>
<a:ListOfPoints>((-66132,-28574),(-66234,-36139),(-61646,-36139))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o44"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o73"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o74"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o75">
<a:CreationDate>1638910945</a:CreationDate>
<a:ModificationDate>1638996504</a:ModificationDate>
<a:Rect>((-57880,-37004), (-53287,-19911))</a:Rect>
<a:ListOfPoints>((-54803,-19911),(-56501,-37004))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o30"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o73"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o76"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o77">
<a:CreationDate>1638911098</a:CreationDate>
<a:ModificationDate>1639008805</a:ModificationDate>
<a:Rect>((-68747,-10991), (-54569,-7398))</a:Rect>
<a:ListOfPoints>((-54569,-9818),(-61439,-9818),(-61439,-8573),(-68747,-8573))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o13"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o33"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o78"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o79">
<a:CreationDate>1638974971</a:CreationDate>
<a:ModificationDate>1638975176</a:ModificationDate>
<a:Rect>((-55364,21712), (-46138,25612))</a:Rect>
<a:ListOfPoints>((-53143,21787),(-54163,25612),(-46138,25612),(-47028,21712))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o19"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o19"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o80"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationClassLinkSymbol Id="o81">
<a:CreationDate>1638975025</a:CreationDate>
<a:ModificationDate>1638975176</a:ModificationDate>
<a:Rect>((-50413,25612), (-50150,30188))</a:Rect>
<a:ListOfPoints>((-50150,25612),(-50413,25612),(-50413,30188))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<c:SourceSymbol>
<o:AssociationSymbol Ref="o79"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o82"/>
</c:DestinationSymbol>
<c:Object>
<o:AssociationClassLink Ref="o83"/>
</c:Object>
</o:AssociationClassLinkSymbol>
<o:AssociationSymbol Id="o84">
<a:CreationDate>1639746604</a:CreationDate>
<a:ModificationDate>1639746666</a:ModificationDate>
<a:Rect>((-96585,18262), (-87416,26356))</a:Rect>
<a:ListOfPoints>((-87453,18262),(-87453,25182),(-96585,25182))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o85"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o86"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o87"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o88">
<a:CreationDate>1639746606</a:CreationDate>
<a:ModificationDate>1639746672</a:ModificationDate>
<a:Rect>((-85490,19000), (-72365,25239))</a:Rect>
<a:ListOfPoints>((-83653,19000),(-83653,25239),(-72365,25239))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o85"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o14"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o89"/>
</c:Object>
</o:AssociationSymbol>
<o:ClassSymbol Id="o14">
<a:CreationDate>1638740163</a:CreationDate>
<a:ModificationDate>1639055302</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-75684,13301), (-64898,29783))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>14807295</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o90"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o22">
<a:CreationDate>1638740569</a:CreationDate>
<a:ModificationDate>1638997312</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-30456,17708), (-19670,32242))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>13565902</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o91"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o13">
<a:CreationDate>1638740576</a:CreationDate>
<a:ModificationDate>1638997066</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-55332,-11250), (-44158,8154))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>16777156</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o92"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o43">
<a:CreationDate>1638740577</a:CreationDate>
<a:ModificationDate>1638978411</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-80685,-22703), (-73683,-17909))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>12632256</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o93"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o30">
<a:CreationDate>1638740578</a:CreationDate>
<a:ModificationDate>1638976872</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-56205,-20462), (-49203,-15668))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>12632256</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o94"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o19">
<a:CreationDate>1638740586</a:CreationDate>
<a:ModificationDate>1638975176</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-53957,16303), (-45797,22071))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>12632256</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o95"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o36">
<a:CreationDate>1638740587</a:CreationDate>
<a:ModificationDate>1638997304</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-13109,-2087), (-3403,3681))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>13565902</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o96"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o27">
<a:CreationDate>1638740588</a:CreationDate>
<a:ModificationDate>1638997143</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-40268,7320), (-30872,15036))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>13565902</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o97"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o33">
<a:CreationDate>1638742936</a:CreationDate>
<a:ModificationDate>1638975176</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-74550,-11008), (-67547,-6214))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>12632256</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o98"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o54">
<a:CreationDate>1638744585</a:CreationDate>
<a:ModificationDate>1638997153</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-30070,-8466), (-19978,-750))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>13565902</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o99"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o44">
<a:CreationDate>1638808232</a:CreationDate>
<a:ModificationDate>1638983161</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-69436,-30212), (-62434,-25418))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>12632256</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o100"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o47">
<a:CreationDate>1638823998</a:CreationDate>
<a:ModificationDate>1639389246</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-45662,-31819), (-38660,-27025))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>12632256</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o101"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o55">
<a:CreationDate>1638863929</a:CreationDate>
<a:ModificationDate>1638975566</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-35800,-14707), (-28798,-9913))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>12632256</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o102"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o58">
<a:CreationDate>1638908402</a:CreationDate>
<a:ModificationDate>1638982882</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-16199,-15279), (-6031,-8635))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>128</a:LineColor>
<a:FillColor>12632256</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:ManuallyResized>1</a:ManuallyResized>
<c:Object>
<o:Class Ref="o103"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o63">
<a:CreationDate>1638909731</a:CreationDate>
<a:ModificationDate>1638982943</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-4996,11010), (2006,15804))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>12632256</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o104"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o68">
<a:CreationDate>1638910288</a:CreationDate>
<a:ModificationDate>1638997246</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-13762,30323), (-4596,38039))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>12909823</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o105"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o73">
<a:CreationDate>1638910740</a:CreationDate>
<a:ModificationDate>1638978415</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-61864,-38519), (-54862,-33725))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>12632256</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o106"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o82">
<a:CreationDate>1638974985</a:CreationDate>
<a:ModificationDate>1638975176</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-54647,28278), (-46179,32099))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>12632256</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o107"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o86">
<a:CreationDate>1639054555</a:CreationDate>
<a:ModificationDate>1639054916</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-102477,22428), (-94625,29170))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>12632256</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<c:Object>
<o:Class Ref="o108"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o85">
<a:CreationDate>1639746518</a:CreationDate>
<a:ModificationDate>1639746592</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-88635,15887), (-82639,21655))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o109"/>
</c:Object>
</o:ClassSymbol>
</c:Symbols>
</o:ClassDiagram>
</c:ClassDiagrams>
<c:DefaultDiagram>
<o:ClassDiagram Ref="o11"/>
</c:DefaultDiagram>
<c:Classes>
<o:Class Id="o90">
<a:ObjectID>051DE203-E2EF-4488-9CB4-0759AE2EF285</a:ObjectID>
<a:Name>adherent</a:Name>
<a:Code>adherent</a:Code>
<a:CreationDate>1638740163</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639746660</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o110">
<a:ObjectID>32241662-60CB-4B3D-BD44-5C40311B0AF1</a:ObjectID>
<a:Name>id</a:Name>
<a:Code>id</a:Code>
<a:CreationDate>1638740240</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639324490</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o111">
<a:ObjectID>C0C87F93-9F8A-418D-89C8-9CB5E0E5FAF8</a:ObjectID>
<a:Name>firstname</a:Name>
<a:Code>firstname</a:Code>
<a:CreationDate>1638740240</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639324908</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o112">
<a:ObjectID>2B264B88-32DD-411C-8644-12B5CCBE8CF5</a:ObjectID>
<a:Name>lastname</a:Name>
<a:Code>lastname</a:Code>
<a:CreationDate>1638740240</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639324908</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o113">
<a:ObjectID>16569E37-39AE-453A-A645-90530EC07D2D</a:ObjectID>
<a:Name>dob</a:Name>
<a:Code>dob</a:Code>
<a:CreationDate>1638740240</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639324490</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o114">
<a:ObjectID>63D3584E-11D7-4D96-AFF8-654A563DB025</a:ObjectID>
<a:Name>email</a:Name>
<a:Code>email</a:Code>
<a:CreationDate>1638740240</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639324490</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o115">
<a:ObjectID>786F9CB8-4455-40E0-A368-9A3B5C6432F2</a:ObjectID>
<a:Name>pw</a:Name>
<a:Code>pw</a:Code>
<a:CreationDate>1638740427</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639324490</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o116">
<a:ObjectID>C69F77E3-7C46-4F5C-A3AD-877A144F44E1</a:ObjectID>
<a:Name>street_nb</a:Name>
<a:Code>street_nb</a:Code>
<a:CreationDate>1638740427</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639324490</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o117">
<a:ObjectID>C8A1A350-01B1-4649-A0EB-BD4960E35A39</a:ObjectID>
<a:Name>street_name</a:Name>
<a:Code>street_name</a:Code>
<a:CreationDate>1638740427</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639324490</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o118">
<a:ObjectID>B0E769D6-D04A-4E35-BFFF-270476AB4D0B</a:ObjectID>
<a:Name>comp_adr</a:Name>
<a:Code>comp_adr</a:Code>
<a:CreationDate>1638740427</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325016</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o119">
<a:ObjectID>F4E0321A-806B-4788-B624-469B1DC6B052</a:ObjectID>
<a:Name>tel</a:Name>
<a:Code>tel</a:Code>
<a:CreationDate>1638740427</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639324490</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o120">
<a:ObjectID>04F38831-0083-463A-A82B-9EEC25F5A226</a:ObjectID>
<a:Name>create_date</a:Name>
<a:Code>create_date</a:Code>
<a:CreationDate>1638740427</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325670</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o121">
<a:ObjectID>A3D6D639-902B-4417-AC98-56BC4F80FF98</a:ObjectID>
<a:Name>confirm_date</a:Name>
<a:Code>confirm_date</a:Code>
<a:CreationDate>1638740427</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325670</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o122">
<a:ObjectID>1D9237E7-FDA0-420A-BC96-495EBFB3A54A</a:ObjectID>
<a:Name>deactivate_date</a:Name>
<a:Code>deactivate_date</a:Code>
<a:CreationDate>1638740427</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325670</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o123">
<a:ObjectID>5CB7CC92-8709-40C9-96CA-C077B24922F9</a:ObjectID>
<a:Name>activation_code</a:Name>
<a:Code>activation_code</a:Code>
<a:CreationDate>1638976342</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1639324926</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o124">
<a:ObjectID>6EAB09BA-B4F1-4DBB-9E43-DD2D9F7DAAD5</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638740395</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740398</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o110"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o124"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o91">
<a:ObjectID>BFCA5EE9-BEF6-45EE-B569-7A3D2FE7A83B</a:ObjectID>
<a:Name>partenaire</a:Name>
<a:Code>partenaire</a:Code>
<a:CreationDate>1638740569</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639324601</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o125">
<a:ObjectID>8087639E-5293-4430-A2A1-19F45EF1FF65</a:ObjectID>
<a:Name>id</a:Name>
<a:Code>id</a:Code>
<a:CreationDate>1638740709</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639324666</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o126">
<a:ObjectID>B4BBE71F-736D-43E5-8222-DB1556F7ACD5</a:ObjectID>
<a:Name>name</a:Name>
<a:Code>name</a:Code>
<a:CreationDate>1638740709</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639324666</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o127">
<a:ObjectID>31AD6E96-A3EF-460B-B6D6-FB14A0DE7E43</a:ObjectID>
<a:Name>email</a:Name>
<a:Code>email</a:Code>
<a:CreationDate>1638740709</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639324666</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o128">
<a:ObjectID>CB70A8B0-2DBF-4EBD-AF6F-5356323E08A0</a:ObjectID>
<a:Name>pw</a:Name>
<a:Code>pw</a:Code>
<a:CreationDate>1638740709</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639324666</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o129">
<a:ObjectID>173478AE-0A97-4B42-8C80-A1E0279A9CCD</a:ObjectID>
<a:Name>tel</a:Name>
<a:Code>tel</a:Code>
<a:CreationDate>1638740709</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639324666</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o130">
<a:ObjectID>203B3FA5-50FA-4B5A-8F16-CC67E893EC7F</a:ObjectID>
<a:Name>street_nb</a:Name>
<a:Code>street_nb</a:Code>
<a:CreationDate>1638740709</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639324666</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o131">
<a:ObjectID>C02A2B1C-6B5F-42A4-8A1A-92809852BF15</a:ObjectID>
<a:Name>street_name</a:Name>
<a:Code>street_name</a:Code>
<a:CreationDate>1638740709</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639324666</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o132">
<a:ObjectID>2DF6BDCA-B6A0-411C-941C-D5A5FEFBD4D8</a:ObjectID>
<a:Name>comp_adr</a:Name>
<a:Code>comp_adr</a:Code>
<a:CreationDate>1638740709</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639324666</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o133">
<a:ObjectID>A3E62F21-8B20-453A-9BB1-42024DF8F331</a:ObjectID>
<a:Name>register_date</a:Name>
<a:Code>register_date</a:Code>
<a:CreationDate>1638740709</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325715</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o134">
<a:ObjectID>431D2A9E-0F9B-4846-8A4D-6B394F9A9450</a:ObjectID>
<a:Name>validate_date</a:Name>
<a:Code>validate_date</a:Code>
<a:CreationDate>1638740709</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325715</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o135">
<a:ObjectID>F1CE01F2-715F-4DA0-8BDB-CA0EBAB63088</a:ObjectID>
<a:Name>deactivate_date</a:Name>
<a:Code>deactivate_date</a:Code>
<a:CreationDate>1638740709</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325715</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o136">
<a:ObjectID>099EDFF5-B3B1-4D4D-AB6F-2A0186BA9116</a:ObjectID>
<a:Name>activation_code</a:Name>
<a:Code>activation_code</a:Code>
<a:CreationDate>1638976328</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1639324666</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o137">
<a:ObjectID>38CCCB57-1D5E-4502-BCE2-8C9C70F12955</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638740767</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740778</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o125"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o137"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o92">
<a:ObjectID>EA45CA83-8DF3-4C3D-948E-A7F99BF73464</a:ObjectID>
<a:Name>don</a:Name>
<a:Code>don</a:Code>
<a:CreationDate>1638740576</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639499486</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o138">
<a:ObjectID>A232F769-24B4-4CE9-9EB6-3C8FE7846C93</a:ObjectID>
<a:Name>id</a:Name>
<a:Code>id</a:Code>
<a:CreationDate>1638740868</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325285</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o139">
<a:ObjectID>FB8C3BFC-D398-4150-8664-F603FB6875B7</a:ObjectID>
<a:Name>qty</a:Name>
<a:Code>qty</a:Code>
<a:CreationDate>1638740868</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325285</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>float</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o140">
<a:ObjectID>8878778A-FC99-4708-9B42-7ACD64B4455F</a:ObjectID>
<a:Name>dlc</a:Name>
<a:Code>dlc</a:Code>
<a:CreationDate>1638740868</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325285</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o141">
<a:ObjectID>7748BDC2-889C-4786-A710-93BBE7AA5DBB</a:ObjectID>
<a:Name>ddm</a:Name>
<a:Code>ddm</a:Code>
<a:CreationDate>1638740868</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325285</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o142">
<a:ObjectID>7FABA44C-1A27-4FEB-B3FF-0E2D8171CADA</a:ObjectID>
<a:Name>create_date</a:Name>
<a:Code>create_date</a:Code>
<a:CreationDate>1638740868</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325285</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o143">
<a:ObjectID>666013B2-24C7-407F-B5FC-DB1917F221F7</a:ObjectID>
<a:Name>reserve_date</a:Name>
<a:Code>reserve_date</a:Code>
<a:CreationDate>1638740868</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325285</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o144">
<a:ObjectID>3016CD27-BD0B-42CD-9A8F-64502A61FBFC</a:ObjectID>
<a:Name>cancel_date</a:Name>
<a:Code>cancel_date</a:Code>
<a:CreationDate>1638976791</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1639325285</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o145">
<a:ObjectID>9C295FFB-FC86-4775-BE07-42CA81332725</a:ObjectID>
<a:Name>picture</a:Name>
<a:Code>picture</a:Code>
<a:CreationDate>1638740868</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325285</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o146">
<a:ObjectID>EE295C6A-7E0C-4DED-B927-C35056FD7743</a:ObjectID>
<a:Name>alt_street_nb</a:Name>
<a:Code>alt_street_nb</a:Code>
<a:CreationDate>1639035855</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1639325285</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o147">
<a:ObjectID>DE2D0AB3-FDDD-453B-BEC4-65F434B331F5</a:ObjectID>
<a:Name>alt_street_name</a:Name>
<a:Code>alt_street_name</a:Code>
<a:CreationDate>1639008736</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325285</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o148">
<a:ObjectID>077E41FD-A5F4-4118-B9E4-ED447353A817</a:ObjectID>
<a:Name>alt_comp_adr</a:Name>
<a:Code>alt_comp_adr</a:Code>
<a:CreationDate>1639054394</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639325285</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o149">
<a:ObjectID>B9E04C13-5C5A-4DD8-AA2F-B1312D766DFD</a:ObjectID>
<a:Name>available_collect</a:Name>
<a:Code>available_collect</a:Code>
<a:CreationDate>1638997337</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325285</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o150">
<a:ObjectID>A4EAAFD0-9C70-4139-85AD-0A1D11742CC4</a:ObjectID>
<a:Name>receive</a:Name>
<a:Code>receive</a:Code>
<a:CreationDate>1639391904</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639392440</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o151">
<a:ObjectID>37DF3BC5-64F6-45BC-8656-C975BE8B792F</a:ObjectID>
<a:Name>no_receive</a:Name>
<a:Code>no_receive</a:Code>
<a:CreationDate>1639391904</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639392440</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o152">
<a:ObjectID>FD3D5D41-B4AF-4567-9727-63A87EE9E6AF</a:ObjectID>
<a:Name>handover</a:Name>
<a:Code>handover</a:Code>
<a:CreationDate>1639391904</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639392440</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o153">
<a:ObjectID>FB3A575C-544C-430F-9073-0CDFF8691FA5</a:ObjectID>
<a:Name>no_handover</a:Name>
<a:Code>no_handover</a:Code>
<a:CreationDate>1639391904</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639392440</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o154">
<a:ObjectID>AD83AE7B-4732-43E9-BB34-C029ED0454A3</a:ObjectID>
<a:Name>final_date</a:Name>
<a:Code>final_date</a:Code>
<a:CreationDate>1638740868</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639392440</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o155">
<a:ObjectID>6FD9C532-EFE8-42F1-AA8A-EDDDCDC6936F</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638740926</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638740929</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o138"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o155"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o93">
<a:ObjectID>BA4A362B-2275-4BF7-99F9-06B691D9B8BE</a:ObjectID>
<a:Name>cat</a:Name>
<a:Code>cat</a:Code>
<a:CreationDate>1638740577</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325517</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o156">
<a:ObjectID>4926F696-2CFB-4EAE-8287-D7937274B59E</a:ObjectID>
<a:Name>id</a:Name>
<a:Code>id</a:Code>
<a:CreationDate>1638741134</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325523</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o157">
<a:ObjectID>942C12EE-27AF-4C94-B3D8-E091C428A2BD</a:ObjectID>
<a:Name>name</a:Name>
<a:Code>name</a:Code>
<a:CreationDate>1638795491</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639325523</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o158">
<a:ObjectID>E9FCF145-6BCC-449E-9B76-ABA713FD8C77</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638741192</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638741197</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o156"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o158"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o94">
<a:ObjectID>F9EF8A6B-ADDB-4736-BAFE-E1ABF9B6FDCE</a:ObjectID>
<a:Name>produit</a:Name>
<a:Code>produit</a:Code>
<a:CreationDate>1638740578</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639747705</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o159">
<a:ObjectID>2FF43FA2-929C-4CC8-963D-AC63C356EA53</a:ObjectID>
<a:Name>id</a:Name>
<a:Code>id</a:Code>
<a:CreationDate>1638741028</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325489</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o160">
<a:ObjectID>53B35A33-6889-44B7-8317-35E389302EC8</a:ObjectID>
<a:Name>name</a:Name>
<a:Code>name</a:Code>
<a:CreationDate>1638741028</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325489</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o161">
<a:ObjectID>F8512356-8CEF-44C6-8283-3F4BCE9EF66C</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638741049</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638741053</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o159"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o161"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o95">
<a:ObjectID>F80C9ACE-3B69-4F28-91A4-C5CC74FB4D1F</a:ObjectID>
<a:Name>localisation</a:Name>
<a:Code>localisation</a:Code>
<a:CreationDate>1638740586</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639329585</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o162">
<a:ObjectID>45DF0AB8-A478-4050-A87C-88919B3A5A23</a:ObjectID>
<a:Name>id</a:Name>
<a:Code>id</a:Code>
<a:CreationDate>1638974886</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1639324573</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o163">
<a:ObjectID>13129F51-731F-4617-8EC4-8427ADF02A7B</a:ObjectID>
<a:Name>city</a:Name>
<a:Code>city</a:Code>
<a:CreationDate>1638740621</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639324573</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o164">
<a:ObjectID>3E413A67-BF0D-4966-A4C3-C670C2B8F6AF</a:ObjectID>
<a:Name>postcode</a:Name>
<a:Code>postcode</a:Code>
<a:CreationDate>1638740621</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639324573</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o165">
<a:ObjectID>A2D1872D-64F1-42EE-AEF4-ECE54B0C9C82</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638742015</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638974941</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o162"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o165"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o96">
<a:ObjectID>DAC77B44-AF51-4CD6-87D5-CBCFD1E6AD45</a:ObjectID>
<a:Name>stockage</a:Name>
<a:Code>stockage</a:Code>
<a:CreationDate>1638740587</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639575423</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o166">
<a:ObjectID>E413D056-8E25-4EF8-ABE7-1EF1233E5E40</a:ObjectID>
<a:Name>id</a:Name>
<a:Code>id</a:Code>
<a:CreationDate>1638741368</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325315</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o167">
<a:ObjectID>84A11848-1432-4F42-845D-76C62146C11B</a:ObjectID>
<a:Name>create_space</a:Name>
<a:Code>create_space</a:Code>
<a:CreationDate>1638976415</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1639325315</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o168">
<a:ObjectID>375900DB-CF2A-4D78-898B-80DC924E4029</a:ObjectID>
<a:Name>remove_space</a:Name>
<a:Code>remove_space</a:Code>
<a:CreationDate>1638976415</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1639325315</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o169">
<a:ObjectID>263F68D9-DC7D-40A3-BFD3-16A3C73EA921</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638741432</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638741435</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o166"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o169"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o97">
<a:ObjectID>9B337BBA-49B7-4290-BBE7-4FDC8FFB3870</a:ObjectID>
<a:Name>relais</a:Name>
<a:Code>relais</a:Code>
<a:CreationDate>1638740588</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325747</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o170">
<a:ObjectID>2ED8C839-D27B-4CC7-A70D-0FF3830BD390</a:ObjectID>
<a:Name>id</a:Name>
<a:Code>id</a:Code>
<a:CreationDate>1638741314</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325098</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o171">
<a:ObjectID>AC36B39C-2121-4D0B-965F-EEFEB58A902B</a:ObjectID>
<a:Name>name</a:Name>
<a:Code>name</a:Code>
<a:CreationDate>1638741314</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325098</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o172">
<a:ObjectID>31894487-74AC-440E-8D99-4BE00CACF27C</a:ObjectID>
<a:Name>street_nb</a:Name>
<a:Code>street_nb</a:Code>
<a:CreationDate>1638741314</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325098</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o173">
<a:ObjectID>D30CEDE3-1308-40AB-B591-945AF4DA0784</a:ObjectID>
<a:Name>street_name</a:Name>
<a:Code>street_name</a:Code>
<a:CreationDate>1638741314</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325098</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o174">
<a:ObjectID>1E8AD026-5F63-48AF-8867-F1F150EC0AFD</a:ObjectID>
<a:Name>comp_adr</a:Name>
<a:Code>comp_adr</a:Code>
<a:CreationDate>1638741314</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325098</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o175">
<a:ObjectID>53AAF8E6-0B79-4571-A52C-4997C822B2D2</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638741345</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638741349</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o170"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o175"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o98">
<a:ObjectID>23CB78FD-586F-4BE2-8EB0-89B345F91FC7</a:ObjectID>
<a:Name>unite</a:Name>
<a:Code>unite</a:Code>
<a:CreationDate>1638742936</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639747705</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o176">
<a:ObjectID>A6137118-0707-43C4-9C55-519137641503</a:ObjectID>
<a:Name>id</a:Name>
<a:Code>id</a:Code>
<a:CreationDate>1638909340</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325479</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o177">
<a:ObjectID>EDECF33B-6377-4C2C-9496-7B45651279D9</a:ObjectID>
<a:Name>name</a:Name>
<a:Code>name</a:Code>
<a:CreationDate>1638742960</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325479</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o178">
<a:ObjectID>D1594FEC-E1ED-4D2E-949D-8D781669B0F3</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638742980</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638909369</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o176"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o178"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o99">
<a:ObjectID>37BCD573-070B-4708-A657-4D165BF7E1C3</a:ObjectID>
<a:Name>horaire</a:Name>
<a:Code>horaire</a:Code>
<a:CreationDate>1638744585</a:CreationDate>
<a:Creator>savsv</a:Creator>
<a:ModificationDate>1639575423</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o179">
<a:ObjectID>5842A7B9-0A9F-46BF-9A42-1BAD1B1E6F80</a:ObjectID>
<a:Name>id</a:Name>
<a:Code>id</a:Code>
<a:CreationDate>1638864031</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639325376</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o180">
<a:ObjectID>2AED5220-C3FA-4CA9-B6A9-4675915848E7</a:ObjectID>
<a:Name>morning_start</a:Name>
<a:Code>morning_start</a:Code>
<a:CreationDate>1638744628</a:CreationDate>
<a:Creator>savsv</a:Creator>
<a:ModificationDate>1639575170</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:DataType>Time</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o181">
<a:ObjectID>8FA3578F-4C40-4261-BCB0-8C77F0170F0A</a:ObjectID>
<a:Name>morning_end</a:Name>
<a:Code>morning_end</a:Code>
<a:CreationDate>1638744628</a:CreationDate>
<a:Creator>savsv</a:Creator>
<a:ModificationDate>1639575200</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:DataType>Time</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o182">
<a:ObjectID>83DCAA04-D1E4-444A-95B8-B2A40A64733C</a:ObjectID>
<a:Name>afternoon_start</a:Name>
<a:Code>afternoon_start</a:Code>
<a:CreationDate>1638744628</a:CreationDate>
<a:Creator>savsv</a:Creator>
<a:ModificationDate>1639575200</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:DataType>Time</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o183">
<a:ObjectID>44F20C18-7AE8-45D1-9444-76B2C766DEC4</a:ObjectID>
<a:Name>afternoon_end</a:Name>
<a:Code>afternoon_end</a:Code>
<a:CreationDate>1638744628</a:CreationDate>
<a:Creator>savsv</a:Creator>
<a:ModificationDate>1639575200</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:DataType>Time</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o184">
<a:ObjectID>585D98CE-7D5C-466B-AB44-E0FBE2BEE23A</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638744659</a:CreationDate>
<a:Creator>savsv</a:Creator>
<a:ModificationDate>1638864094</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o179"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o184"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o100">
<a:ObjectID>C8E8307B-7DCE-47FF-A890-65A0F0095D3C</a:ObjectID>
<a:Name>cat2</a:Name>
<a:Code>cat2</a:Code>
<a:CreationDate>1638808232</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639325536</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o185">
<a:ObjectID>734D97F9-154F-435C-85F2-7B0243DDEC4C</a:ObjectID>
<a:Name>id</a:Name>
<a:Code>id</a:Code>
<a:CreationDate>1638808237</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639325547</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o186">
<a:ObjectID>6E675793-7006-4B96-9FA5-480A673D9B72</a:ObjectID>
<a:Name>name</a:Name>
<a:Code>name</a:Code>
<a:CreationDate>1638808237</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639325554</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o187">
<a:ObjectID>3AD4AE8A-E8A0-4FA8-9FAC-6C1E7722360F</a:ObjectID>
<a:Name>Identifiant_2</a:Name>
<a:Code>Identifiant_2</a:Code>
<a:CreationDate>1638808408</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1638808413</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o185"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o187"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o101">
<a:ObjectID>326E88CB-227F-4763-AA29-79C761D04EE6</a:ObjectID>
<a:Name>conservation</a:Name>
<a:Code>conservation</a:Code>
<a:CreationDate>1638823998</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325581</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o188">
<a:ObjectID>26C35706-4ABC-43CA-9882-308EE0505EAD</a:ObjectID>
<a:Name>id</a:Name>
<a:Code>id</a:Code>
<a:CreationDate>1638824190</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325581</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o189">
<a:ObjectID>B99CAF16-50BF-41E3-B660-50FB1615F0B8</a:ObjectID>
<a:Name>name</a:Name>
<a:Code>name</a:Code>
<a:CreationDate>1638824361</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325581</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o190">
<a:ObjectID>8F9A6E21-6536-4293-997F-F2143230A0C0</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638824392</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638824395</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o188"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o190"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o102">
<a:ObjectID>D8886003-9606-44B9-89CB-27556D0BECA0</a:ObjectID>
<a:Name>jour</a:Name>
<a:Code>jour</a:Code>
<a:CreationDate>1638863929</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639325448</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o191">
<a:ObjectID>3FC8F0F4-D970-436B-8038-FCFF59241E47</a:ObjectID>
<a:Name>id</a:Name>
<a:Code>id</a:Code>
<a:CreationDate>1638908985</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325448</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o192">
<a:ObjectID>62EAB544-B4B1-464D-A401-BFE197B74E6E</a:ObjectID>
<a:Name>name</a:Name>
<a:Code>name</a:Code>
<a:CreationDate>1638908628</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325448</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o193">
<a:ObjectID>33F31BF8-0122-4AD6-A420-E1CAC4CD695D</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638909151</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638909154</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o191"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o193"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o103">
<a:ObjectID>771DEE7D-906F-41E9-A99D-0154DA94D9C8</a:ObjectID>
<a:Name>dimension</a:Name>
<a:Code>dimension</a:Code>
<a:CreationDate>1638908402</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325385</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o194">
<a:ObjectID>A142D9E7-0416-401A-8CCA-799EE11B183A</a:ObjectID>
<a:Name>id</a:Name>
<a:Code>id</a:Code>
<a:CreationDate>1638908411</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325401</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o195">
<a:ObjectID>1BE2D152-1E9A-417F-A129-6BED426F8EFD</a:ObjectID>
<a:Name>name</a:Name>
<a:Code>name</a:Code>
<a:CreationDate>1638908411</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325401</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o196">
<a:ObjectID>BF41AECE-A00E-4730-9FA5-F37D333CEFAC</a:ObjectID>
<a:Name>height</a:Name>
<a:Code>height</a:Code>
<a:CreationDate>1638908411</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325401</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o197">
<a:ObjectID>08D24349-EE23-4BCD-9B26-EE20F578FD01</a:ObjectID>
<a:Name>width</a:Name>
<a:Code>width</a:Code>
<a:CreationDate>1638908411</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325401</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o198">
<a:ObjectID>D027A082-1E25-41EE-9435-ECB67CA46E3A</a:ObjectID>
<a:Name>depth</a:Name>
<a:Code>depth</a:Code>
<a:CreationDate>1638975631</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1639325401</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o199">
<a:ObjectID>FEB47E9D-EE88-442F-8272-9F2190A805E3</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638908540</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638908543</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o194"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o199"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o104">
<a:ObjectID>AC425B14-24B2-46AF-8F41-49A02DFEB3D3</a:ObjectID>
<a:Name>accessibilite</a:Name>
<a:Code>accessibilite</a:Code>
<a:CreationDate>1638909731</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325060</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o200">
<a:ObjectID>BB97BED1-8D40-4804-B654-675A6429101B</a:ObjectID>
<a:Name>id</a:Name>
<a:Code>id</a:Code>
<a:CreationDate>1638909743</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325066</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o201">
<a:ObjectID>FA8BB504-674E-45CE-BEDD-77298918E978</a:ObjectID>
<a:Name>name</a:Name>
<a:Code>name</a:Code>
<a:CreationDate>1638909743</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325066</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o202">
<a:ObjectID>871BCECF-EBA7-4775-854D-E3273AA8E820</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638909766</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638909769</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o200"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o202"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o105">
<a:ObjectID>1D371398-9F85-4C59-AE7F-BA3EF8C9B7B9</a:ObjectID>
<a:Name>indisponibilite</a:Name>
<a:Code>indisponibilite</a:Code>
<a:CreationDate>1638910288</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639324689</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o203">
<a:ObjectID>4D1B719D-1A83-40AA-A753-5173D6527F99</a:ObjectID>
<a:Name>id</a:Name>
<a:Code>id</a:Code>
<a:CreationDate>1638910319</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639324766</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o204">
<a:ObjectID>B6E707BF-88F0-41D4-930E-8F5C8D8899C1</a:ObjectID>
<a:Name>start</a:Name>
<a:Code>start</a:Code>
<a:CreationDate>1638910319</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639324766</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o205">
<a:ObjectID>366F293F-190D-45B1-B43D-CE8DC44F9E63</a:ObjectID>
<a:Name>end</a:Name>
<a:Code>end</a:Code>
<a:CreationDate>1638910319</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639324766</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o206">
<a:ObjectID>1E66DDFC-7343-4109-97E7-8F5FEE384C46</a:ObjectID>
<a:Name>register_date</a:Name>
<a:Code>register_date</a:Code>
<a:CreationDate>1638977065</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1639325692</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o207">
<a:ObjectID>FBEC7582-073A-44E8-9E94-0AECD0852E6E</a:ObjectID>
<a:Name>cancel_date</a:Name>
<a:Code>cancel_date</a:Code>
<a:CreationDate>1638977065</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1639325692</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o208">
<a:ObjectID>B0E52339-8973-4DD0-9993-3B0FE4B7E897</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638910546</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638910549</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o203"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o208"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o106">
<a:ObjectID>4EE291AB-D150-4587-8938-85DCB574E533</a:ObjectID>
<a:Name>cat3</a:Name>
<a:Code>cat3</a:Code>
<a:CreationDate>1638910740</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325560</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o209">
<a:ObjectID>3E7BE4CF-7D2E-4724-90AA-B1BECE5B076A</a:ObjectID>
<a:Name>id</a:Name>
<a:Code>id</a:Code>
<a:CreationDate>1638910759</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325565</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o210">
<a:ObjectID>E27BC27C-417D-4428-8B19-499495CC230A</a:ObjectID>
<a:Name>name</a:Name>
<a:Code>name</a:Code>
<a:CreationDate>1638910777</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639325732</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o211">
<a:ObjectID>7901A955-5433-43D2-B2F9-4FEA167858B5</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1638910796</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638910799</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o209"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o211"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o107">
<a:ObjectID>7DCDE51D-5DC7-450D-B149-1AC39C6E3CDF</a:ObjectID>
<a:Name>distance</a:Name>
<a:Code>distance</a:Code>
<a:CreationDate>1638974985</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1639324590</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o212">
<a:ObjectID>32FC5D18-163D-43A8-9AAE-00B78523AB55</a:ObjectID>
<a:Name>avg_distance</a:Name>
<a:Code>avg_distance</a:Code>
<a:CreationDate>1638974986</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1639325026</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
</o:Class>
<o:Class Id="o108">
<a:ObjectID>33430FD0-447A-424E-808B-C6B469D93393</a:ObjectID>
<a:Name>carnet</a:Name>
<a:Code>carnet</a:Code>
<a:CreationDate>1639054555</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639746635</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o213">
<a:ObjectID>614AC06C-3D46-4F7E-A877-1A9DE7A51888</a:ObjectID>
<a:Name>id</a:Name>
<a:Code>id</a:Code>
<a:CreationDate>1639054569</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639324090</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o214">
<a:ObjectID>A587C9E0-E0FD-4BE8-BB1F-5CCE0819CB6D</a:ObjectID>
<a:Name>name</a:Name>
<a:Code>name</a:Code>
<a:CreationDate>1639054728</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639324090</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o215">
<a:ObjectID>156FFF3D-52E9-474E-93F4-73CDB1EC3AF9</a:ObjectID>
<a:Name>nb_cred</a:Name>
<a:Code>nb_cred</a:Code>
<a:CreationDate>1639054728</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639324090</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o216">
<a:ObjectID>DA8DD187-6393-4411-BD7D-0235C0BA6B05</a:ObjectID>
<a:Name>price</a:Name>
<a:Code>price</a:Code>
<a:CreationDate>1639054728</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639324090</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:DataType>float</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o217">
<a:ObjectID>1D804748-DC75-444A-B1E6-0481432A713A</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1639054807</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639054809</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o213"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o217"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o109">
<a:ObjectID>252089F1-B635-4B0A-BE0C-E338358E757D</a:ObjectID>
<a:Name>achat</a:Name>
<a:Code>achat</a:Code>
<a:CreationDate>1639746518</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639746660</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o218">
<a:ObjectID>6A92825C-6F5A-4C97-8726-724E0E538A97</a:ObjectID>
<a:Name>id</a:Name>
<a:Code>id</a:Code>
<a:CreationDate>1639746539</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639746586</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o219">
<a:ObjectID>E331804D-DE6E-472F-B649-EE5DB5BE7764</a:ObjectID>
<a:Name>date</a:Name>
<a:Code>date</a:Code>
<a:CreationDate>1639746539</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639746571</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:DataType>date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o220">
<a:ObjectID>FE7E9466-CF6B-4947-9EAF-169A3A383F37</a:ObjectID>
<a:Name>qty</a:Name>
<a:Code>qty</a:Code>
<a:CreationDate>1639746539</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639746558</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o221">
<a:ObjectID>F6FAAB60-57EF-419D-87D4-096DA5776355</a:ObjectID>
<a:Name>Identifiant_1</a:Name>
<a:Code>Identifiant_1</a:Code>
<a:CreationDate>1639746583</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639746586</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<c:Identifier.Attributes>
<o:Attribute Ref="o218"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o221"/>
</c:PrimaryIdentifier>
</o:Class>
</c:Classes>
<c:Associations>
<o:Association Id="o15">
<a:ObjectID>4C083C20-D856-451A-AF19-A03D6A4C5128</a:ObjectID>
<a:Name>beneficiary</a:Name>
<a:Code>beneficiary</a:Code>
<a:CreationDate>1638741664</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639499486</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:RoleBName>beneficiary</a:RoleBName>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o90"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o92"/>
</c:Object2>
</o:Association>
<o:Association Id="o17">
<a:ObjectID>8A919B82-21D5-4CC5-AFCA-111CD53F7DC9</a:ObjectID>
<a:Name>donor</a:Name>
<a:Code>donor</a:Code>
<a:CreationDate>1638741668</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639392497</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:RoleBName>donor</a:RoleBName>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o90"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o92"/>
</c:Object2>
</o:Association>
<o:Association Id="o20">
<a:ObjectID>7A90E571-A744-4990-A9BD-7C05257FEE43</a:ObjectID>
<a:Name>Association_3</a:Name>
<a:Code>Association_3</a:Code>
<a:CreationDate>1638741833</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638741962</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o95"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o90"/>
</c:Object2>
</o:Association>
<o:Association Id="o23">
<a:ObjectID>F4ED954D-9699-481D-BF84-6E5BB224EC68</a:ObjectID>
<a:Name>Association_4</a:Name>
<a:Code>Association_4</a:Code>
<a:CreationDate>1638742137</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638742284</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o95"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o91"/>
</c:Object2>
</o:Association>
<o:Association Id="o25">
<a:ObjectID>0490B2CC-29CC-4D8E-B849-C8056EE3D452</a:ObjectID>
<a:Name>Association_5</a:Name>
<a:Code>Association_5</a:Code>
<a:CreationDate>1638742140</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638742196</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o95"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o92"/>
</c:Object2>
</o:Association>
<o:Association Id="o28">
<a:ObjectID>C5208E43-F8E3-48C0-9436-21F444FE07E3</a:ObjectID>
<a:Name>Association_6</a:Name>
<a:Code>Association_6</a:Code>
<a:CreationDate>1638742143</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638975140</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o95"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o97"/>
</c:Object2>
</o:Association>
<o:Association Id="o31">
<a:ObjectID>BECFC468-0EAB-45B3-A057-37CC5F24DBB9</a:ObjectID>
<a:Name>Association_7</a:Name>
<a:Code>Association_7</a:Code>
<a:CreationDate>1638742322</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638742339</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o94"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o92"/>
</c:Object2>
</o:Association>
<o:Association Id="o34">
<a:ObjectID>DAC334C9-1A10-43EE-B323-89A8925D9F4D</a:ObjectID>
<a:Name>mesurer</a:Name>
<a:Code>mesurer</a:Code>
<a:CreationDate>1638743009</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639747705</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:RoleAName>product</a:RoleAName>
<a:RoleBName>unit</a:RoleBName>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..*</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o98"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o94"/>
</c:Object2>
</o:Association>
<o:Association Id="o37">
<a:ObjectID>37D0D6AB-C010-45F6-BBC1-1E39047A3EAD</a:ObjectID>
<a:Name>Association_11</a:Name>
<a:Code>Association_11</a:Code>
<a:CreationDate>1638743243</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638975582</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o96"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o92"/>
</c:Object2>
</o:Association>
<o:Association Id="o39">
<a:ObjectID>595662E2-11DE-46DE-9308-D774392CB34F</a:ObjectID>
<a:Name>Association_12</a:Name>
<a:Code>Association_12</a:Code>
<a:CreationDate>1638743318</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638743398</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>1..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o97"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o91"/>
</c:Object2>
</o:Association>
<o:Association Id="o41">
<a:ObjectID>D61D57F9-BA5A-41E6-AAFE-88CA55D925B4</a:ObjectID>
<a:Name>Association_13</a:Name>
<a:Code>Association_13</a:Code>
<a:CreationDate>1638743325</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638743449</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>1..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o96"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o97"/>
</c:Object2>
</o:Association>
<o:Association Id="o45">
<a:ObjectID>843620A9-7AFD-42A9-A3EC-56D2459CE40B</a:ObjectID>
<a:Name>Association_17</a:Name>
<a:Code>Association_17</a:Code>
<a:CreationDate>1638808519</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1638910870</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>1..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..*</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o100"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o93"/>
</c:Object2>
</o:Association>
<o:Association Id="o48">
<a:ObjectID>0367B4C8-1042-401A-8B7A-500E26B178C2</a:ObjectID>
<a:Name>Association_18</a:Name>
<a:Code>Association_18</a:Code>
<a:CreationDate>1638825676</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638825782</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o101"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o96"/>
</c:Object2>
</o:Association>
<o:Association Id="o50">
<a:ObjectID>882F15CC-A79E-4269-BD09-9F8B823649F4</a:ObjectID>
<a:Name>Association_19</a:Name>
<a:Code>Association_19</a:Code>
<a:CreationDate>1638825682</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638825792</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o101"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o92"/>
</c:Object2>
</o:Association>
<o:Association Id="o52">
<a:ObjectID>5A37824C-77C7-433D-8D5D-34798F708CDD</a:ObjectID>
<a:Name>Association_20</a:Name>
<a:Code>Association_20</a:Code>
<a:CreationDate>1638825703</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638825801</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o101"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o94"/>
</c:Object2>
</o:Association>
<o:Association Id="o56">
<a:ObjectID>737D2748-AF80-4FE9-A8B7-988804ADEE07</a:ObjectID>
<a:Name>Association_21</a:Name>
<a:Code>Association_21</a:Code>
<a:CreationDate>1638864109</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1638975446</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o102"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o99"/>
</c:Object2>
</o:Association>
<o:Association Id="o59">
<a:ObjectID>B2F9FD4F-B4FC-409C-BAA7-E41AB86EB796</a:ObjectID>
<a:Name>Association_23</a:Name>
<a:Code>Association_23</a:Code>
<a:CreationDate>1638908549</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638908583</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>1..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o103"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o96"/>
</c:Object2>
</o:Association>
<o:Association Id="o61">
<a:ObjectID>948271EA-689D-48C8-BD6A-D002B67F1102</a:ObjectID>
<a:Name>Association_24</a:Name>
<a:Code>Association_24</a:Code>
<a:CreationDate>1638908742</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639575423</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:RoleAMultiplicity>0..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o99"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o96"/>
</c:Object2>
</o:Association>
<o:Association Id="o64">
<a:ObjectID>32C2EE09-63FB-40DF-8FCF-EF9B345A65AD</a:ObjectID>
<a:Name>Association_25</a:Name>
<a:Code>Association_25</a:Code>
<a:CreationDate>1638909822</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638909888</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o104"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o96"/>
</c:Object2>
</o:Association>
<o:Association Id="o66">
<a:ObjectID>6F863AC6-7AB5-49AA-ABA1-0034E69A2FAC</a:ObjectID>
<a:Name>Association_26</a:Name>
<a:Code>Association_26</a:Code>
<a:CreationDate>1638910164</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638975096</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:RoleAMultiplicity>1..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o99"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o92"/>
</c:Object2>
</o:Association>
<o:Association Id="o69">
<a:ObjectID>529D007C-3DD0-4397-9169-ED1071B5C7FA</a:ObjectID>
<a:Name>Association_27</a:Name>
<a:Code>Association_27</a:Code>
<a:CreationDate>1638910565</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638911745</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>1..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o105"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o90"/>
</c:Object2>
</o:Association>
<o:Association Id="o71">
<a:ObjectID>1F498080-7411-498F-B81B-EDE7E1354814</a:ObjectID>
<a:Name>Association_28</a:Name>
<a:Code>Association_28</a:Code>
<a:CreationDate>1638910608</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638911752</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>1..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o105"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o96"/>
</c:Object2>
</o:Association>
<o:Association Id="o74">
<a:ObjectID>3F11A031-87C7-4B35-B3A0-666BBDB726D9</a:ObjectID>
<a:Name>Association_29</a:Name>
<a:Code>Association_29</a:Code>
<a:CreationDate>1638910899</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638983221</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:RoleAMultiplicity>1..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..*</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o106"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o100"/>
</c:Object2>
</o:Association>
<o:Association Id="o76">
<a:ObjectID>1463CD02-67E8-41E9-AB9E-627CABE49920</a:ObjectID>
<a:Name>Association_30</a:Name>
<a:Code>Association_30</a:Code>
<a:CreationDate>1638910945</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638910981</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o106"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o94"/>
</c:Object2>
</o:Association>
<o:Association Id="o78">
<a:ObjectID>4ACCDFB1-3431-4CEE-BA87-CB81F65F2332</a:ObjectID>
<a:Name>Association_31</a:Name>
<a:Code>Association_31</a:Code>
<a:CreationDate>1638911098</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638911137</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o98"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o92"/>
</c:Object2>
</o:Association>
<o:Association Id="o80">
<a:ObjectID>4B91835C-4530-4B53-9A50-D9FD44881EA0</a:ObjectID>
<a:Name>Association_32</a:Name>
<a:Code>Association_32</a:Code>
<a:CreationDate>1638974971</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1639747657</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o95"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o95"/>
</c:Object2>
</o:Association>
<o:Association Id="o87">
<a:ObjectID>50628794-238A-444E-BAF1-4347AA7B46A6</a:ObjectID>
<a:Name>Association_33</a:Name>
<a:Code>Association_33</a:Code>
<a:CreationDate>1639746604</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639746635</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o108"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o109"/>
</c:Object2>
</o:Association>
<o:Association Id="o89">
<a:ObjectID>D14ECAE6-1B58-4F0B-B965-102CD74C6449</a:ObjectID>
<a:Name>Association_34</a:Name>
<a:Code>Association_34</a:Code>
<a:CreationDate>1639746606</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639746660</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o90"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o109"/>
</c:Object2>
</o:Association>
</c:Associations>
<c:AssociationClassLinks>
<o:AssociationClassLink Id="o83">
<a:ObjectID>11010681-3962-47A6-AF7C-35A5F35D8055</a:ObjectID>
<a:CreationDate>1638975025</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1638975025</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<c:Object1>
<o:Class Ref="o107"/>
</c:Object1>
<c:Object2>
<o:Association Ref="o80"/>
</c:Object2>
</o:AssociationClassLink>
</c:AssociationClassLinks>
<c:TargetModels>
<o:TargetModel Id="o222">
<a:ObjectID>72F00AA5-6F18-4A87-B064-B8F16AD79109</a:ObjectID>
<a:Name>Analyse</a:Name>
<a:Code>Analysis</a:Code>
<a:CreationDate>1638740123</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639035814</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:TargetModelURL>file:///%_OBJLANG%/analysis.xol</a:TargetModelURL>
<a:TargetModelID>87317290-AF03-469F-BC1E-99593F18A4AB</a:TargetModelID>
<a:TargetModelClassID>1811206C-1A4B-11D1-83D9-444553540000</a:TargetModelClassID>
<c:SessionShortcuts>
<o:Shortcut Ref="o10"/>
</c:SessionShortcuts>
</o:TargetModel>
<o:TargetModel Id="o223">
<a:ObjectID>AE7D4FAD-633D-4CF0-903C-AC1B3D438B46</a:ObjectID>
<a:Name>Diagramme de classes UML_1</a:Name>
<a:Code>DIAGRAMME_DE_CLASSES_UML_1</a:Code>
<a:ExtractionID>7340147</a:ExtractionID>
<a:CreationDate>1638911573</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638911634</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:TargetModelURL>file:///C|/Users/Thibault/Desktop/Diagramme de classes UML_1.mpd</a:TargetModelURL>
<a:TargetModelID>F1BC6D57-D86F-46E6-97A4-CFFCF8DB61EF</a:TargetModelID>
<a:TargetModelClassID>CDE44E21-9669-11D1-9914-006097355D9B</a:TargetModelClassID>
<c:SessionShortcuts>
<o:Shortcut Ref="o3"/>
</c:SessionShortcuts>
</o:TargetModel>
<o:TargetModel Id="o224">
<a:ObjectID>C67BAA6E-3373-4714-A34B-21ED23456D9A</a:ObjectID>
<a:Name>Diagramme de classes UML_1</a:Name>
<a:Code>DIAGRAMME_DE_CLASSES_UML_1</a:Code>
<a:ExtractionID>79200</a:ExtractionID>
<a:CreationDate>1638911799</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638911877</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:TargetModelURL>file:///C|/Users/Thibault/Desktop/Diagramme de classes UML_1.mcd</a:TargetModelURL>
<a:TargetModelID>7E520AB3-ACCC-476D-A518-D2CF71A4D4B8</a:TargetModelID>
<a:TargetModelClassID>1E597170-9350-11D1-AB3C-0020AF71E433</a:TargetModelClassID>
<c:SessionShortcuts>
<o:Shortcut Ref="o4"/>
</c:SessionShortcuts>
</o:TargetModel>
<o:TargetModel Id="o225">
<a:ObjectID>B0F624A9-FF95-4087-BD0D-E4D7E7EDDDFD</a:ObjectID>
<a:Name>Diagramme de classes UML_1</a:Name>
<a:Code>DIAGRAMME_DE_CLASSES_UML_1</a:Code>
<a:ExtractionID>17</a:ExtractionID>
<a:CreationDate>1638911919</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638911957</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:TargetModelURL>file:///C|/Users/Thibault/Desktop/MPD.mpd</a:TargetModelURL>
<a:TargetModelID>81342C3A-A408-47C1-9BAD-E4671554446A</a:TargetModelID>
<a:TargetModelClassID>CDE44E21-9669-11D1-9914-006097355D9B</a:TargetModelClassID>
<c:SessionShortcuts>
<o:Shortcut Ref="o5"/>
</c:SessionShortcuts>
</o:TargetModel>
<o:TargetModel Id="o226">
<a:ObjectID>FF8C8569-DEFB-4526-A353-C4A4E6551155</a:ObjectID>
<a:Name>MOO P2 G3</a:Name>
<a:Code>MOO_P2_G3</a:Code>
<a:ExtractionID>3538976</a:ExtractionID>
<a:CreationDate>1638997563</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1638997848</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:TargetModelURL>file:///C|/Users/Thibault/Desktop/MOO P2 G3.mpd</a:TargetModelURL>
<a:TargetModelID>269A5707-9D0F-4F80-B73A-9922C5BB909B</a:TargetModelID>
<a:TargetModelClassID>CDE44E21-9669-11D1-9914-006097355D9B</a:TargetModelClassID>
<c:SessionShortcuts>
<o:Shortcut Ref="o6"/>
</c:SessionShortcuts>
</o:TargetModel>
<o:TargetModel Id="o227">
<a:ObjectID>000FE581-13CA-4A7D-BD30-F41D1608860D</a:ObjectID>
<a:Name>MOO P2 G3</a:Name>
<a:Code>MOO_P2_G3</a:Code>
<a:ExtractionID>7340111</a:ExtractionID>
<a:CreationDate>1639008868</a:CreationDate>
<a:Creator>Thibault</a:Creator>
<a:ModificationDate>1639008886</a:ModificationDate>
<a:Modifier>Thibault</a:Modifier>
<a:TargetModelURL>file:///C|/Users/Thibault/Desktop/MOO P2 G3.mpd</a:TargetModelURL>
<a:TargetModelID>54029FDD-A46C-4D46-BDA9-221F52CB7B38</a:TargetModelID>
<a:TargetModelClassID>CDE44E21-9669-11D1-9914-006097355D9B</a:TargetModelClassID>
<c:SessionShortcuts>
<o:Shortcut Ref="o7"/>
</c:SessionShortcuts>
</o:TargetModel>
<o:TargetModel Id="o228">
<a:ObjectID>E53CD295-DE8E-49A7-B99F-A23648A05E43</a:ObjectID>
<a:Name>MOO AI110 P2 G3</a:Name>
<a:Code>MOO_AI110_P2_G3</a:Code>
<a:CreationDate>1639036706</a:CreationDate>
<a:Creator>christian</a:Creator>
<a:ModificationDate>1639037443</a:ModificationDate>
<a:Modifier>christian</a:Modifier>
<a:TargetModelURL>file:///D|/Documents/00-afcepf-EQL/AI 110/G3/MOO AI110 P2 G3.mpd</a:TargetModelURL>
<a:TargetModelID>5125C573-749F-4020-AC42-DB75BCF46951</a:TargetModelID>
<a:TargetModelClassID>CDE44E21-9669-11D1-9914-006097355D9B</a:TargetModelClassID>
<c:SessionShortcuts>
<o:Shortcut Ref="o8"/>
</c:SessionShortcuts>
</o:TargetModel>
<o:TargetModel Id="o229">
<a:ObjectID>04047B8B-A10A-4853-A1CB-E55236FB1354</a:ObjectID>
<a:Name>MOO P2 G3</a:Name>
<a:Code>MOO_P2_G3</a:Code>
<a:CreationDate>1639128684</a:CreationDate>
<a:Creator>formation</a:Creator>
<a:ModificationDate>1639129180</a:ModificationDate>
<a:Modifier>formation</a:Modifier>
<a:TargetModelURL>file:///C|/dld-projet2/Modeles/MOO P2 G3.mpd</a:TargetModelURL>
<a:TargetModelID>025B6B68-C69E-45EC-8AD0-FC73F33DB3D5</a:TargetModelID>
<a:TargetModelClassID>CDE44E21-9669-11D1-9914-006097355D9B</a:TargetModelClassID>
<c:SessionShortcuts>
<o:Shortcut Ref="o9"/>
</c:SessionShortcuts>
</o:TargetModel>
</c:TargetModels>
</o:Model>
</c:Children>
</o:RootObject>

</Model>