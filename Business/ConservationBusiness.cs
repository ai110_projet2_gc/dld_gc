﻿using Fr.EQL.AI110.DLD_GC.DataAccess;
using Fr.EQL.AI110.DLD_GC.Entities;

namespace Fr.EQL.AI110.DLD_GC.Business
{
    public class ConservationBusiness
    {
        public List<Conservation> GetConservations()
        {

            ConservationDao dao = new ConservationDao();
            return dao.GetAllConservations();

        }
    }
}