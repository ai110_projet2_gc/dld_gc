﻿using Fr.EQL.AI110.DLD_GC.DataAccess;
using Fr.EQL.AI110.DLD_GC.Entities;

namespace Fr.EQL.AI110.DLD_GC.Business
{
    public class UniteBusiness
    {
        public List<Unite> GetUnites()
        {
            UniteDao dao = new UniteDao();
            return dao.GetAllUnites();
        }
    }
}